import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions, fonts} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Rating, AirbnbRating } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import PackageComponent from '../components/PackageComponent';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")



 class CountryDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
           data:'',
           images:[],
           temperature:'0.00'
          
          
         

      };

    }



     getWeather(country) {
      //const openweather_api = "https://api.openweathermap.org/data/2.5/weather?appid=f6c9f143d6581381662978157abcb2aa&lat=" + lat +"&lon=" + long;
      const openweather_api = "http://api.openweathermap.org/data/2.5/weather?q="+country+"&appid=f6c9f143d6581381662978157abcb2aa&units=imperial"
      fetch(openweather_api)
      .then(res => res.json())
      .then((data) => {
        console.log('data', JSON.stringify(data))
        if(data.cod == 200){
          if(data.main){
            var temperature = data.main.temp // in farenheight
            this.setState({temperature:temperature})
            
            // var message = fTemp+'\xB0F is ' + fToCel + '\xB0C.';
            //   console.log(message)
            
          }
        }
       
      })
    
  }

    _fetchCountry =  (countryId) =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'general/country_details/'+ countryId
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",JSON.stringify(responseJson))
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              this.setState({data:responseJson.data,
                                images:responseJson.data.images})
                                this.getWeather(responseJson.data.name)

                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }


   


    componentWillMount(){
      var result = this.props.navigation.getParam('result')
      console.log('country_id-----',result['country_id'])
       this._fetchCountry(result['country_id'])
 }

 onCityClick = (item) =>{
  console.log('onCityClick--------',JSON.stringify(item))
  var id = item.id
  let obj ={'city_id':id}
  this.props.navigation.navigate("CityDetails",{result:obj})

}



 _renderCities = ({item, index}) =>{
  return (
    <TouchableOpacity onPress={()=> this.onCityClick(item)}
    style={styles.listContainer}>
    <FastImage 
    source={{uri:item.image}}
    style={styles.countryImage} 
    resizeMode={FastImage.resizeMode.cover}/>

     <Text style={styles.countryText}>{item.name.toUpperCase()}</Text>
    </TouchableOpacity>
  )
}

_attractionClick (item){
  console.log('.......',item)
  var aid = item.id
  let obj ={'attraction_id':aid}
  this.props.navigation.navigate("AttractionDetails",{result:obj})
}



_renderAttractions= ({item, index}) => {
 
  return (
      <TouchableOpacity  onPress={()=>this._attractionClick(item)}
      style={styles.attractionsContainer}>
         <FastImage source={{uri:item.images}}
         resizeMode={FastImage.resizeMode.cover}
         style={styles.attractionsImage}/>
         {/* <View style={styles.attractionsText}>
          <Text style={{color:'white'}}>{item.name}</Text>
          <Text style={{color:'white',fontSize:12}}>{item.address}</Text>
         
         </View> */}
      </TouchableOpacity>
  );
}
renderPage = ({item, index}) =>{
  return (
    <FastImage 
    source={{uri:item}}
    style={{height:'100%',width:'100%',backgroundColor:'grey'}}
    resizeMode={FastImage.resizeMode.cover}/>
  );
}
onHotelClick = (item) =>{
  console.log('onHotelClick--------',JSON.stringify(item))
  var id = item.id
  let obj ={'hotel_id':id}
  this.props.navigation.navigate("HotelDetails",{result:obj})

}

_renderHotel = ({item, index}) =>{
  return (
     <HotelComponent
      object = {item} 
      index ={index}
      baseUrl ={this.state.baseUrlBanner}
      clickHandler ={this.onHotelClick}/>
  )
}

onPackageClick = (item) =>{
  console.log('onPackageClick--------',JSON.stringify(item))
  var id = item.id
  let obj ={'package_id':id}
  this.props.navigation.navigate("ExperienceDetails",{result:obj})

}

_renderExperiences = ({item, index}) =>{
  return (
     <PackageComponent
      object = {item} 
      index ={index}
      clickHandler ={this.onPackageClick}/>
  )
}

   
    render() {

      const {data} = this.state
      console.log(":::",data.main_attractions)

        return (
      <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
        {/** header */}
        <View style={styles.header}>
        <TouchableOpacity style={{flexDirection:'row',alignItems:'center',padding:10}}
        onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/back.png')}
          style={{height:30,width:30,marginLeft:4,marginRight:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
          <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>{data.name} </Text> 
         </TouchableOpacity>

         <View/>
        </View>

          {/** header end */}
        <KeyboardAwareScrollView 
        contentContainerStyle={{
          justifyContent:'center',
         paddingBottom:20
        }}>



           <View style={{height:dimensions.SCREEN_HEIGHT * 0.4,width:dimensions.SCREEN_WIDTH}} >
           <Carousel
           ref={(c) => { this._carousel = c; }}
           data={this.state.images}
           renderItem={this.renderPage}
           autoplay={false}
           //loop
           sliderWidth={dimensions.SCREEN_WIDTH}
           itemWidth={dimensions.SCREEN_WIDTH }
         />
         </View>

           <View style={{borderRadius:15,padding:10,backgroundColor:colors.LIGHT_BLACK,marginTop:-20,paddingBottom:15}}>

            <View style={{borderColor:colors.COLOR_PRIMARY,borderWidth:0.5,
            borderRadius:15,marginHorizontal:30,padding:8,marginTop:-40,backgroundColor:'black'}}>
           <Text style={{fontSize:18,fontWeight:'500',marginVertical:10,fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR}}>About</Text>
           <Text style={{color:colors.WHITE}}>{data.facts ? data.facts.about : ''}</Text>
           </View>

            <View style={styles.hotelContainer}>
           <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,fontWeight:'400',margin:10}}> Main Attractions</Text>
           {/* <FlatList
           
           style={{marginVertical:10}}
           horizontal={true}
           numColumns={1}
           data={data.main_attractions}
           showsHorizontalScrollIndicator={false}
           renderItem={(item,index) => this._renderAttractions(item,index)}
           keyExtractor={item => item.id}
          /> */}

           {/** attraction */}
           {
              
                <Carousel
                style={{marginVertical:10}}
                data={this.state.data.main_attractions}
                renderItem={this._renderAttractions}
                autoplay={true}
                loop
                sliderWidth={dimensions.SCREEN_WIDTH * 0.98}
                itemWidth={dimensions.SCREEN_WIDTH * 0.6}
              />
             
             
              }

             {/** attraction  end*/}

          </View>


          <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,fontSize:18,fontWeight:'400',marginVertical:10}}>Facts</Text>

          <View style={{flexDirection:'row',margin:0}}>
          <TouchableOpacity style={styles.factContainer}>
            <View style={[styles.rowContainer,{justifyContent:'flex-start'}]}>
            <FastImage 
            source={require('../assets/currency_yellow.png')}
            style={{height:15,width:15}} 
            resizeMode={FastImage.resizeMode.cover}/>
            <Text style={{color:colors.COLOR_PRIMARY,marginLeft:10,fontWeight:'700'}}>Currency</Text>
            </View>
            <Text style={styles.factData}>{(data.facts && data.facts.currency) ? data.facts.currency : ''}</Text>
            <View/>
          </TouchableOpacity>


          <TouchableOpacity style={styles.factContainer}>
            <View style={[styles.rowContainer,{justifyContent:'flex-start'}]}>
            <FastImage 
            source={require('../assets/weather_yellow.png')}
            style={{height:15,width:15}} 
            resizeMode={FastImage.resizeMode.cover}/>
            <Text style={{color:colors.COLOR_PRIMARY,marginLeft:10,fontWeight:'700'}}>Weather</Text>
            </View>
            <Text style={styles.factData}>{this.state.temperature} C</Text>
            <View/>
          </TouchableOpacity>

          </View>

          <View style={{flexDirection:'row',margin:0}}>
          <TouchableOpacity style={styles.factContainer}>
          <View style={[styles.rowContainer,{justifyContent:'flex-start'}]}>
          <FastImage 
          source={require('../assets/plug_yellow.png')}
          style={{height:15,width:15}} 
          resizeMode={FastImage.resizeMode.cover}/>
          <Text style={{color:colors.COLOR_PRIMARY,marginLeft:10,fontWeight:'700'}}>Electric Plug</Text>
          </View>
          <Text style={styles.factData}>{(data.facts && data.facts.electic_plug) ? data.facts.electic_plug : ''}</Text>
            <View/>
          </TouchableOpacity>

          <TouchableOpacity style={styles.factContainer}>
          <View style={[styles.rowContainer,{justifyContent:'flex-start'}]}>
          <FastImage 
          source={require('../assets/language.png')}
          style={{height:15,width:15}} 
          resizeMode={FastImage.resizeMode.cover}/>
          <Text style={{color:colors.COLOR_PRIMARY,marginLeft:10,fontWeight:'700'}}>Languages</Text>
          </View>
          <Text style={styles.factData}>{(data.facts && data.facts.languages) ? data.facts.languages : ''}</Text>
            <View/>
          </TouchableOpacity>
          </View>
         
         

          
           

          

           <View style={{height:2,width:'100%',backgroundColor:'black',marginVertical:10}}/>
     {/** hotels */}
      <View style={styles.hotelContainer}>
           <View style={styles.rowContainer}>
               <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,fontSize:16}}>  Stays</Text>
               <TouchableOpacity style={{marginHorizontal:15}}
                    onPress={()=>{this.props.navigation.navigate("Stays",{stays:data.stays})} }>
                    <FastImage 
                    source={require('../assets/arrow_forward_yellow.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.cover}/>
                       
                     </TouchableOpacity>
           </View>


           <FlatList
               style={{marginVertical:10}}
               horizontal={true}
               data={data.stays}
               showsHorizontalScrollIndicator={false}
               renderItem={(item,index) => this._renderHotel(item,index)}
               keyExtractor={item => item.id}
           />

          </View>

         {/** hotels end */}




          {/** experiences */}
      <View style={styles.hotelContainer}>
           <View style={styles.rowContainer}>
               <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,fontSize:16}}>  Experiences</Text>
               <TouchableOpacity style={{marginHorizontal:15}}
                    onPress={()=>{this.props.navigation.navigate("Experiences",{experiences:data.experience})} }>
                    <FastImage 
                    source={require('../assets/arrow_forward_yellow.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.cover}/>
                       
                     </TouchableOpacity>
           </View>


           <FlatList
               style={{marginVertical:10}}
               horizontal={true}
               data={data.experience}
               showsHorizontalScrollIndicator={false}
               renderItem={(item,index) => this._renderExperiences(item,index)}
               keyExtractor={item => item.id}
           />

          </View>

         {/** hotels end */}

    {/**
           <Text style={{color:colors.WHITE,fontSize:18,fontWeight:'200',marginVertical:10}}>Cities</Text>

           <FlatList
           
           style={{marginVertical:10}}
           horizontal={false}
           numColumns={1}
           data={data.cities}
           showsHorizontalScrollIndicator={false}
           renderItem={(item,index) => this._renderCities(item,index)}
           keyExtractor={item => item.id}
          />

     */}

          <View style={{height:2,width:'100%',backgroundColor:'black',marginVertical:10}}/>


           </View>


         
        
        </KeyboardAwareScrollView>

       
         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(CountryDetails);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.BLACK,
    marginTop:15,
   
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  ratingContainer:{
    padding:6,borderRadius:20,
    justifyContent:'center',alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY
  },
  descriptionContainer:{
    padding:6,borderRadius:10,
    backgroundColor:colors.BLACK,
    borderWidth:0.4,
    borderColor:colors.COLOR_PRIMARY,
    width:'95%',alignSelf:'center',
    marginVertical:10
  }, 
  listContainer:{
    width:'98%',
    borderRadius:10,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    height:70,
    marginVertical:5,
    alignSelf:'center',
    overflow:'hidden',justifyContent:'center'
  },
  countryImage:{
      position:'absolute',
      top:0,
      left:0,
      right:0,
      bottom:0,
  },
  countryText:{
      color:'white',
      fontWeight:'bold',
      fontSize:17,
      marginLeft:20
  },
  rowContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
},
container: {
  backgroundColor:colors.LIGHT_BLACK,
  margin:10,
  height:dimensions.SCREEN_HEIGHT * 0.24,
  width:dimensions.SCREEN_WIDTH * 0.6,
  overflow:'hidden',
  borderRadius:10,
  borderWidth:0.6,
  borderColor:colors.BLACK,

 },
 imageContainer:{
   margin:0,
   position:'absolute',
   top:0,
   bottom:0,left:0,right:0,
  
 },
 factContainer:{
   height:85,
   flex:1,
   margin:10,
   backgroundColor:colors.BLACK,
   borderRadius:10,
  //  flexDirection:'row',
   padding:7,
   justifyContent:'space-between'
 },
 factData:{color:colors.WHITE,marginLeft:10,fontSize:12},
 attractionsContainer:{
  width:'100%',
  height:190,
  borderRadius:10,
  backgroundColor:'grey',overflow:'hidden'
},
attractionsImage:{
  top:0,
  bottom:0,left:0,right:0,
  position:'absolute',overflow:'hidden'
},


}
)
