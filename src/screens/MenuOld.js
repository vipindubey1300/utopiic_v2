import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ButtonComponent from '../components/ButtonComponent';


import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")



 class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
    

      };

    }

    _onLogout =async ()=>{
      try {
       
        this.props.remove({});
        await AsyncStorage.removeItem("user_id");
        await AsyncStorage.removeItem("name");
        await AsyncStorage.removeItem("image");
        await AsyncStorage.removeItem("email");
        await AsyncStorage.removeItem("phone");
        await AsyncStorage.removeItem("user_type");
        await AsyncStorage.removeItem("token");
        
       this.props.navigation.navigate("Passcode")
      
        return true;
      }
      catch(exception) {
        
        return false;
      }
    }


_terms =()=>{
  this.props.navigation.navigate("Terms")
}

_privacy =()=>{
  this.props.navigation.navigate("Privacy")
}


   
    render() {

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <ScrollView style={{backgroundColor:'black'}}>


          {/** header */}
          <View style={styles.header}>
          <TouchableOpacity
          onPress={()=> {this.props.navigation.pop()} }>
          <FastImage 
          source={require('../assets/round_back_black.png')}
          style={{height:40,width:40}} 
          resizeMode={FastImage.resizeMode.contain}/>
           </TouchableOpacity>

          <View style={{margin:12}}>
            <Text style={{fontWeight:'bold',fontSize:18,marginVertical:5}}>{this.props.user.name}</Text>
            <Text style={{fontWeight:'100',fontSize:12,marginVertical:5}}>{this.props.user.phone}</Text>
            <Text style={{fontWeight:'100',fontSize:12,marginVertical:5}}>{this.props.user.email}</Text>
            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
                
                    <Text style={{color:'grey'}}>Profile Incomplete!</Text>
                    <TouchableOpacity
                    onPress={()=> {this.props.navigation.navigate("EditProfile")} }>
                    <FastImage 
                    source={require('../assets/edit-round-shadow.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.contain}/>
                     </TouchableOpacity>
            </View>
          </View>
          </View>

            {/** header end */}


            {/** request  */}

            <View style={styles.requestContainer}>
            <View style={styles.rowContainer}>
                <TouchableOpacity style={styles.container}
                onPress={()=> {this.props.navigation.navigate("Requests")} }>
                <FastImage 
                source={require('../assets/requests-icon.png')}
                style={{height:75,width:75}} 
                resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>

                <View>
                <Text style={{color:colors.COLOR_PRIMARY,marginBottom:10}}>Requests</Text>
                <Text style={{color:'white',width:'80%'}}>Requests asd sdhfskdf sdfhksdf sdfhsdf sdflh</Text>
                </View>

                <TouchableOpacity style={{alignSelf:'flex-end',marginLeft:-30,marginBottom:-10}}
                onPress={()=> {this.props.navigation.navigate("Requests")} }>
                <FastImage 
                source={require('../assets/edit-round-shadow.png')}
                style={{height:25,width:25}} 
                resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>

            
            </View>
            
            </View>
            {/** request end */}


             {/** payment  */}

             <View style={styles.paymentContainer}>
             <View style={styles.rowContainer}>
                 <TouchableOpacity style={styles.container}
                   onPress={()=> {this.props.navigation.navigate("Payments")} }>
                 <FastImage 
                 source={require('../assets/currency-icon.png')}
                 style={{height:75,width:75}} 
                 resizeMode={FastImage.resizeMode.contain}/>
                 </TouchableOpacity>
 
                 <View>
                 <Text style={{color:colors.COLOR_PRIMARY,marginBottom:10}}>Payments</Text>
                 <Text style={{color:'white',width:'80%'}}>Requests asd sdhfskdf sdfhksdf sdfhsdf sdflh</Text>
                 </View>
 
                 <TouchableOpacity style={{alignSelf:'flex-end',marginLeft:-30,marginBottom:-10}}
                 onPress={()=> {this.props.navigation.navigate("Payments")} }>
                 <FastImage 
                 source={require('../assets/edit-round-shadow.png')}
                 style={{height:25,width:25}} 
                 resizeMode={FastImage.resizeMode.contain}/>
                 </TouchableOpacity>
 
             
             </View>
             
             </View>
             {/** request end */}



             {/** bottom section */}
             <View style={styles.bottomContainer}>
              <TouchableOpacity  onPress={()=> this._privacy()} style={styles.buttonContainer}>
                <Text style={{color:colors.COLOR_PRIMARY}}>Privacy Policy</Text>
              </TouchableOpacity>


              <TouchableOpacity onPress={()=> this._terms()}
              style={styles.buttonContainer}>
              <Text style={{color:colors.COLOR_PRIMARY}}>Terms and Conditions</Text>
              </TouchableOpacity>
             
             </View>


              {/** bottom section end */}



              <ButtonComponent 
              style={{marginTop:25,width:dimensions.SCREEN_WIDTH * 0.4,alignSelf:'center'}}
              handler={this._onLogout}
              label ={'Logout'}/>


             




            
              
              </ScrollView>
         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
    remove : (userinfo) => dispatch(removeUser(userinfo)),   
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Menu);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.COLOR_PRIMARY,
       borderBottomRightRadius:20
  },
  requestContainer:{
      paddingVertical:25,
      paddingHorizontal:10,
      borderBottomRightRadius:16,
      width:dimensions.SCREEN_WIDTH,
      backgroundColor:colors.BLACK,
      marginTop:0,
      marginBottom:-15,
      elevation:2
      
  },
  paymentContainer:{
    paddingTop:30,
    paddingHorizontal:10,
    paddingBottom:20,
    borderBottomRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.LIGHT_BLACK,
    marginTop:0
},
  rowContainer:{
      flexDirection:'row',
     // justifyContent:'space-between',
      alignItems:'center',
     
  },
  container:{
    borderColor:colors.COLOR_PRIMARY,
    borderRadius:6,
    borderWidth:0.7,
    marginRight:10,
    padding:10
  },
  bottomContainer:{
    padding:10,
    flexDirection:'row',
    alignItems:'center',
    marginTop:100
  },
  buttonContainer:{
      borderRadius:15,
      backgroundColor:colors.LIGHT_BLACK,
      padding:10,
      justifyContent:'center',
      alignItems: 'center',
      flex:1,
      marginHorizontal:10
  }

}
)
