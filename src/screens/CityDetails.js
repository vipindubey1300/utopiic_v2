import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Rating, AirbnbRating } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")



 class CityDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
           data:'',
           images:[]
          
          
         

      };

    }





    _fetchCity =  (cityId) =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'general/city_details/'+ cityId

                 console.log("*********",JSON.stringify(url))
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            // console.log("*********",JSON.stringify(responseJson))
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              this.setState({data:responseJson.data,images:responseJson.data.images})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }


   


    componentWillMount(){
      var result = this.props.navigation.getParam('result')
      console.log('city_id-----',result['city_id'])
       this._fetchCity(result['city_id'])
 }


 _renderCities = ({item, index}) =>{
  return (
    <TouchableOpacity onPress={()=> this.onCountryClick(item)}
    style={styles.listContainer}>
    <FastImage 
    source={{uri:item.image}}
    style={styles.countryImage} 
    resizeMode={FastImage.resizeMode.cover}/>

     <Text style={styles.countryText}>{item.country.toUpperCase()}</Text>
    </TouchableOpacity>
  )
}

_attractionClick (item){
  console.log('.......',item)
  var aid = item.id
  let obj ={'attraction_id':aid}
  this.props.navigation.navigate("AttractionDetails",{result:obj})
}


_renderAttractions = ({item, index}) =>{
  return (
    <TouchableOpacity onPress={()=> this._attractionClick(item)}
    style={styles.container}>
    <FastImage 
    source={{uri:item.cover}}
    style={styles.imageContainer} 
    resizeMode={FastImage.resizeMode.cover}/>

    </TouchableOpacity>
  )
}

_renderHotel = ({item, index}) =>{
    return (
       <HotelComponent
        object = {item} 
        index ={index}
        baseUrl ={this.state.baseUrlBanner}
        clickHandler ={this.onHotelClick}/>
    )
  }


_renderArticles = ({item, index}) =>{
    return (
        <TouchableOpacity style={styles.articleContainer}>
            <View style={{flex:8}}>
                <Text style={{color:'white'}}>{item.name}</Text>
                <Text style={{color:colors.COLOR_PRIMARY,fontSize:12}}>{item.description.substring(0,150)}</Text>
            </View>

            <FastImage 
            source={{uri:item.image}}
            style={{flex:2,height:70,borderRadius:15,overflow: 'hidden',}} 
            resizeMode={FastImage.resizeMode.cover}/>
        </TouchableOpacity>
    )
  }
  renderPage = ({item, index}) =>{
    return (
      <FastImage 
      source={{uri:item}}
      style={{height:'100%',width:'100%',backgroundColor:'grey'}}
      resizeMode={FastImage.resizeMode.cover}/>
    );
  }
  
   
    render() {

      const {data} = this.state

        return (
      <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
        {/** header */}
        <View style={styles.header}>
        <TouchableOpacity style={{flexDirection:'row',alignItems:'center',padding:10}}
        onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/back.png')}
          style={{height:30,width:30,marginLeft:4,marginRight:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
          <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>{data.name} </Text> 
         </TouchableOpacity>

         <View/>
        </View>

          {/** header end */}
        <KeyboardAwareScrollView contentContainerStyle={{
          justifyContent:'center',
         paddingBottom:20
        }}>

        <View style={{height:dimensions.SCREEN_HEIGHT * 0.4,width:dimensions.SCREEN_WIDTH}} >
        <Carousel
        ref={(c) => { this._carousel = c; }}
        data={this.state.images}
        renderItem={this.renderPage}
        autoplay={true}
       // loop
        sliderWidth={dimensions.SCREEN_WIDTH}
        itemWidth={dimensions.SCREEN_WIDTH }
      />
      </View>

           <View style={{borderRadius:15,padding:10,backgroundColor:'black',marginTop:-20,paddingBottom:15}}>

           <Text style={{color:colors.WHITE,fontSize:18,fontWeight:'200',marginVertical:10}}>About</Text>
           <Text style={{color:colors.WHITE}}>{data.facts ? data.facts.about : ''}</Text>

           <Text style={{color:colors.WHITE,fontSize:18,fontWeight:'200',marginVertical:10}}>Main Attractions</Text>
           <FlatList
           
           style={{marginVertical:10}}
           horizontal={true}
           numColumns={1}
           data={data.main_attractions}
           showsHorizontalScrollIndicator={false}
           renderItem={(item,index) => this._renderAttractions(item,index)}
           keyExtractor={item => item.id}
          />

           {/** hotels */}
           <View style={styles.hotelContainer}>
           <View style={styles.rowContainer}>
               <Text style={{color:colors.COLOR_PRIMARY,fontSize:16}}>  Stays</Text>
               <TouchableOpacity
               onPress={()=>{this.props.navigation.navigate("Stays")} }>
               <FastImage 
               source={require('../assets/arrow_forward_yellow.png')}
               style={{height:25,width:25,marginRight:20}} 
               resizeMode={FastImage.resizeMode.cover}/>
                  
                </TouchableOpacity>
           </View>


           <FlatList
               style={{marginVertical:10}}
               horizontal={true}
               data={data.stays}
               showsHorizontalScrollIndicator={false}
               renderItem={(item,index) => this._renderHotel(item,index)}
               keyExtractor={item => item.id}
           />

          </View>

         {/** hotels end */}

          {/** experince */}
          <View style={styles.hotelContainer}>
          <View style={styles.rowContainer}>
              <Text style={{color:colors.COLOR_PRIMARY,fontSize:16}}>  Experiences</Text>
              <TouchableOpacity
              onPress={()=>{this.props.navigation.navigate("Stays")} }>
              <FastImage 
              source={require('../assets/arrow_forward_yellow.png')}
              style={{height:25,width:25,marginRight:20}} 
              resizeMode={FastImage.resizeMode.cover}/>
                 
               </TouchableOpacity>
          </View>


          <FlatList
              style={{marginVertical:10}}
              horizontal={true}
              data={data.stays}
              showsHorizontalScrollIndicator={false}
              renderItem={(item,index) => this._renderHotel(item,index)}
              keyExtractor={item => item.id}
          />

         </View>

        {/** experince end */}

        <Text style={{color:colors.WHITE,fontSize:18,fontWeight:'200',marginVertical:10,alignSelf:'center'}}>Articles</Text>
          <FlatList
                style={{marginVertical:10}}
                data={data.articles}
                showsHorizontalScrollIndicator={false}
                renderItem={(item,index) => this._renderArticles(item,index)}
                keyExtractor={item => item.id}
            />




          

          
           

          

           

           </View>


         
        
        </KeyboardAwareScrollView>

       
         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(CityDetails);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    borderRadius:16,
    width:dimensions.SCREEN_WIDTH*0.96,
    backgroundColor:colors.LIGHT_BLACK,
    marginTop:15,
   
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  ratingContainer:{
    padding:6,borderRadius:20,
    justifyContent:'center',alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY
  },
  descriptionContainer:{
    padding:6,borderRadius:10,
    backgroundColor:colors.BLACK,
    borderWidth:0.4,
    borderColor:colors.COLOR_PRIMARY,
    width:'95%',alignSelf:'center',
    marginVertical:10
  }, 
  listContainer:{
    width:'98%',
    borderRadius:10,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    height:60,
    marginVertical:5,
    alignSelf:'center',
    overflow:'hidden',justifyContent:'center'
  },
  countryImage:{
      position:'absolute',
      top:0,
      left:0,
      right:0,
      bottom:0,
  },
  countryText:{
      color:'white',
      fontWeight:'bold',
      fontSize:17,
      marginLeft:20
  },
  rowContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
},
container: {
  backgroundColor:colors.GREY,
  margin:10,
  height:dimensions.SCREEN_HEIGHT * 0.24,
  width:dimensions.SCREEN_WIDTH * 0.6,
  overflow:'hidden',
  borderRadius:10,
  borderWidth:0.6,
  borderColor:colors.BLACK,

 },
 imageContainer:{
   margin:0,
   position:'absolute',
   top:0,
   bottom:0,left:0,right:0,
  
 },
 factContainer:{
   height:80,
   flex:1,
   margin:10,
   backgroundColor:colors.LIGHT_BLACK,
   borderRadius:10,
   flexDirection:'row',
   padding:7,
 },

  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  articleContainer:{
      borderRadius:18,
      borderWidth:0.1,
      borderColor:colors.COLOR_PRIMARY,
      padding:5,
      width:'95%',
      margin:5,
      flexDirection:'row'
  },
 


}
)
