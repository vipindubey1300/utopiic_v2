import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import BookingComponent from '../components/BookingComponent';
import ProductComponent from '../components/ProductComponent';

 const d = Dimensions.get("window")



 class HomeDataSupplier extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          bookings:[],
          loading_status:false,
          selectedTab:'',
          baseUrl:'',
          baseUrlBanner:'',
          products: []
         

      };

    }




    _fetchProducts = async () =>{
        console.log("*********",this.props.user.token)
      this.setState({loading_status:true})
               let url = urls.base_url +'suppliers/native/products'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                         
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data.list
                            //console.log('lists,',list)
                           
                           
                           this.setState({products:list})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }

    _fetchBookings = async () =>{
      console.log(' _fetchBookings _fetchBookings _fetchBookings-----------,')
      this.setState({loading_status:true})
    
               let url = urls.base_url +'suppliers/native/bookings'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                         
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data.list
                            console.log(' _fetchBookings lists-----------,',list)
                            
                           
                           this.setState({bookings:list})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }

 







    componentWillMount(){
      console.log(JSON.stringify(this.props.user))
     this._fetchProducts()
    // this._fetchPackages()
    this._fetchBookings()
    }


    onProductClick = (item) =>{
        console.log('onHotelClick--------',JSON.stringify(item))
        // var id = item.id
        // let obj ={'hotel_id':id}
        // this.props.navigation.navigate("HotelDetails",{result:obj})
      
      }

    _renderProduct = ({item, index}) =>{
       return (
          <ProductComponent
           object = {item} 
           index ={index}
           clickHandler ={this.onProductClick}/>
       )
     }



     _renderBooking = ({item, index}) =>{
        return (
           <BookingComponent
            object = {item} 
            index ={index}
            clickHandler ={this.onHotelClick}/>
        )
      }

    render() {

        return (
          <ScrollView style={{flex:1}}>
          {/** header */}
          <View style={styles.header}>
          <TouchableOpacity style={styles.container}
          onPress={()=> {} }>
          <FastImage 
          source={require('../assets/utopiic-logo-text.png')}
          style={{height:40,width:100,marginLeft:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
             
           </TouchableOpacity>

           <TouchableOpacity style={styles.container}
           onPress={()=>{this.props.navigation.navigate("Menu")} }>
           <FastImage 
           source={require('../assets/menu-gray.png')}
           style={{height:50,width:50}} 
           resizeMode={FastImage.resizeMode.cover}/>
              
            </TouchableOpacity>
          </View>

            {/** header end */}


            {/** heading */}
                <Text style={{color:'white',fontSize:18,margin:10}}>Hi {this.props.user.name},</Text>
                <Text style={{color:'white',fontSize:12,margin:10}}>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</Text>

             {/** heading end */}



               {/** exclusive */}
               <View style={styles.exclusiveContainer}>
                <View style={styles.rowContainer}>
                    <Text style={{color:'white',fontSize:16}}>  Products</Text>
                    <TouchableOpacity style={styles.container}
                    onPress={()=>{} }>
                    <FastImage 
                    source={require('../assets/arrow_forward_yellow.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.cover}/>
                       
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10}}
                    horizontal={true}
                    data={this.state.products}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderProduct(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** exclusive end */}




               {/** hotels */}
               <View style={styles.hotelContainer}>
                <View style={styles.rowContainer}>
                    <Text style={{color:colors.COLOR_PRIMARY,fontSize:16}}>  Bookings</Text>
                    <TouchableOpacity style={styles.container}
                    onPress={()=>{this.props.navigation.navigate("Stays")} }>
                    <FastImage 
                    source={require('../assets/arrow_forward_yellow.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.cover}/>
                       
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10}}
                    horizontal={true}
                    data={this.state.bookings}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderBooking(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** hotels end */}


              
         
         </ScrollView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(HomeDataSupplier);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
  exclusiveContainer:{
      paddingVertical:10,
      paddingHorizontal:2,
      borderTopRightRadius:16,
      width:dimensions.SCREEN_WIDTH,
      backgroundColor:colors.LIGHT_BLACK,
      marginTop:10,
      paddingBottom:30
  },
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.BLACK,
    marginTop:-15
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  }

}
)
