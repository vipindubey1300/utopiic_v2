import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions,fonts} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Rating, AirbnbRating } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';
import RNPickerSelect from 'react-native-picker-select';
import {Carousel as CarouselB}   from 'react-native-banner-carousel';
import Icon from 'react-native-vector-icons/FontAwesome';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import LinearGradient from 'react-native-linear-gradient';

import SwiperFlatList from 'react-native-swiper-flatlist';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';
import StarRating from 'react-native-star-rating';

 const d = Dimensions.get("window")



 class ExperienceDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          details:'',
          name:'',
          days:'',  
          city:'',
          nights:'',  
          country:'',
          currency:'',  
          cover:'',
          basic_price:'',      
          tax:'',
          description:'',  
          hashtag:'',
          summary:'',  
          hotels:[],
          images:[],  
          includes:[],
          attractions:[],
          itinerary:[],  
        //   addressRegion:{
           addressRegion:{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
           },
           rating:0,
           daysItenary:[],
           selectedItenary:null,
           selectedDay:null,
           guides:[]
          
          
         

      };

    }





    _fetchDetails =  (packageId) =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'package/details/'+ packageId
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",JSON.stringify(responseJson))
                           
                            this.setState({loading_status:false})

                         
  
                            if(responseJson.status){

                              var array =[]
                              const temp =  responseJson.data.details.itinerary
                              temp.forEach(function (item, index) {
                                  array.push({'key':temp[index].id,'value':temp[index].id,'label':temp[index].day + " day"} )
                                 // console.log('=============',item, index);
                                });

                              this.setState({
                                details:responseJson.data.details,
                                name:responseJson.data.details.name,
                                days:responseJson.data.details.days,  
                                city:responseJson.data.details.city_name,
                                nights:responseJson.data.details.nights,  
                                country:responseJson.data.details.country_name,
                                currency:responseJson.data.details.currency,  
                                cover:responseJson.data.details.cover,
                                basic_price:responseJson.data.details.basic_price,                                hotel_facilities:responseJson.data.hotel_facilities,
                                tax:responseJson.data.details.tax,
                                description:responseJson.data.details.description,  
                                hashtag:responseJson.data.details.hashtag,
                                summary:responseJson.data.details.summary,  
                                rating:responseJson.data.details.rating,  
                                images:responseJson.data.details.images,  
                                includes:responseJson.data.details.includes,
                                attractions:responseJson.data.details.attractions,
                                itinerary:responseJson.data.details.itinerary, 
                                
                                daysItenary:array,
                                selectedItenary:responseJson.data.details.itinerary.length > 0
                                ? responseJson.data.details.itinerary[0] : null,
                                selectedDay:responseJson.data.details.itinerary.length > 0
                                ? responseJson.data.details.itinerary[0].id : null
                    
                              })
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }


      _fetchHotels =  (packageId) =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'package/package_hotels/'+ packageId
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("_fetchHotels*********",JSON.stringify(responseJson))
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              this.setState({
                                hotels:responseJson.data.List,
                               
                              })
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }


      _fetchGuides =  () =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'tours/list_guides'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("guidddeeeessssss*********",JSON.stringify(responseJson))
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              this.setState({
                                guides:responseJson.data,
                               
                              })
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }


    // componentWillMount(){
    //  // console.log(JSON.stringify(this.props.user))
   
    //  this._fetchHotels()
    //  // this._fetchPackages()
    // }


    componentWillMount(){
      var result = this.props.navigation.getParam('result')
      console.log('_fetchDetails-----',result['package_id'])
       this._fetchDetails(result['package_id'])
       this._fetchHotels(result['package_id'])
       this._fetchGuides()
  }

  _onDone =()=>{
    if(this.state.details != ''){
      let obj ={
        'experience':this.state.details,
        'hotels':this.state.hotels
      }
      this.props.navigation.navigate("ReviewExperience",{result:obj})
    }
  }
  _terms =()=>{
    this.props.navigation.navigate("Terms")
  }


 _renderFacilities = ({item, index}) =>{
  return (
     <TouchableOpacity
      style={styles.listContainer}>
     <FastImage 
     source={{uri:item.image}}
     style={styles.facilitiesImage} 
     resizeMode={FastImage.resizeMode.contain}/>

      <Text style={styles.facilitiesText}>{item.name}</Text>
     </TouchableOpacity>
  )
}


_attractionClick (item){
  console.log('.......',item)
  var aid = item.aid
  let obj ={'attraction_id':aid}
  this.props.navigation.navigate("AttractionDetails",{result:obj})
}



_renderAttractions= ({item, index}) => {
 
  return (
      <TouchableOpacity  onPress={()=>this._attractionClick(item)}
      style={styles.attractionsContainer}>
         <FastImage source={{uri:item.images}}
         resizeMode={FastImage.resizeMode.cover}
         style={styles.attractionsImage}/>
         {/* <View style={styles.attractionsText}>
          <Text style={{color:'white'}}>{item.name}</Text>
          <Text style={{color:'white',fontSize:12}}>{item.address}</Text>
         
         </View> */}
      </TouchableOpacity>
  );
}

onHotelClick = (item) =>{
  console.log('onHotelClick--------',JSON.stringify(item))
  var id = item.id
  let obj ={'hotel_id':id}
  this.props.navigation.navigate("HotelDetails",{result:obj})

}


_renderHotel = ({item, index}) =>{
  return (
    <TouchableOpacity  onPress={()=> this.onHotelClick(item)}
    style={styles.hotelContainer}>
    <FastImage source={{uri:item.image_path + item.image}}
    resizeMode={FastImage.resizeMode.cover}
    style={styles.hotelImage}/>


    <View  style={styles.hotelBottom}>
              <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',padding:10}}>
              <View style={{flex:9}}>
                <Text style={{color:colors.COLOR_PRIMARY,textTransform:'capitalize'}}>  {item.name}</Text>
                <View style={{
                  flexDirection:'row',
                  alignItems:'center',
                  marginVertical:4
                }}>
                <Image
                source={require('../assets/pin.png')}
                resizeMode='stretch'
                style={{width:15, height: 15,marginRight:5}}/>
                <Text style={{ fontSize:12,color:'white',width:'90%',textTransform:'capitalize'}}>{item.address}</Text>
                </View>

                <View  style={{width:'40%'}}>
                <StarRating
                    disabled={true}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={5}
                    starSize={17}
                    rating={item.star_rating}
                    fullStarColor={colors.COLOR_PRIMARY}
                 />
                 </View>
    
             

              </View>



              <View style={{flex:3}}>
                <Text style={{color:'white'}}>Rs 3000</Text>
                <Text style={{color:'white'}}>/per night</Text>
              </View>


              
          </View>
          <View style={[styles.ratingContainer,{position:'absolute',bottom:180,right:10}]}>
          <Text style={{color:colors.BLACK}}>{parseFloat(2).toFixed(1)}</Text>
          </View>

    </View>
    </TouchableOpacity>

  )
}


_renderGuides = ({item, index}) =>{
  return(
    <View style={styles.guideContainer}>
      <View style={{flexDirection:'row',alignItems:'center'}}>
      <View style={{flex:7}}>
        <Text style={{color:'white',fontSize:14,fontWeight:'600'}}>{item.first_name + ' '+ item.last_name}</Text>
        <Text style={{color:'white',fontSize:12}}>{item.contact}</Text>
        <Text style={{color:'white',fontSize:12}}>{item.email}</Text>
       
        <Text style={{color:colors.COLOR_PRIMARY,fontSize:12}}>{item.company_name}</Text>
  
      </View>
        <View style={{flex:3}}>
        <FastImage source={{uri:item.image}}
        resizeMode={FastImage.resizeMode.cover}
        style={{height:60,width:60,borderRadius:10,overflow:'hidden'}}/>
    
        </View>

      </View>

      <View style={{height:16}}/>

      <Text style={{color:'grey',fontSize:12}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</Text>

    </View>
  )
}


_renderItenary = ({item, index}) =>{
  return (
    <View></View>

  )
}

_renderIncludes = ({item, index}) =>{
  return (
     <View
      style={styles.listContainer}>
     <FastImage 
     source={{uri:item.image}}
     style={styles.facilitiesImage} 
     resizeMode={FastImage.resizeMode.contain}/>

      <Text style={styles.facilitiesText}>{item.name}</Text>
     </View>
  )
}
   
renderPage =({item, index}) =>{
  console.log('image--',item)
  return (
    <FastImage 
    source={{uri:item}}
    style={{height:'100%',width:'100%'}}
    resizeMode={FastImage.resizeMode.cover}/>
  );
}

    render() {

   
      const {name,
        days,
        city,
        nights,
        country,
        currency,
        cover,
        basic_price,
        tax,
        description,
        hashtag,
        summary,
        images,  
        includes,
        attractions,
        itinerary,
        rating
      } = this.state

     
      

        return (
      <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
        {/** header */}
        <View style={styles.header}>
        <TouchableOpacity style={{flexDirection:'row',alignItems:'center',padding:10}}
        onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/back.png')}
          style={{height:30,width:30,marginLeft:4,marginRight:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
          <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>{name} </Text> 
         </TouchableOpacity>

         <View/>
        </View>

          {/** header end */}
        <KeyboardAwareScrollView contentContainerStyle={{
          justifyContent:'center',
         paddingBottom:50
        }}>
        <View style={{height:dimensions.SCREEN_HEIGHT * 0.4,width:dimensions.SCREEN_WIDTH}} >
       
        <Carousel
             ref={(c) => { this._carousel = c; }}
             data={images}
             renderItem={this.renderPage}
             //autoplay={true}
             //loop
             sliderWidth={dimensions.SCREEN_WIDTH}
             itemWidth={dimensions.SCREEN_WIDTH }
           />
        </View>


      
           <View style={{borderRadius:0, paddingLeft:6,borderRadius:10,paddingBottom:6,paddingRight:6,backgroundColor:colors.LIGHT_BLACK,marginTop:-10,paddingBottom:15}}>

            <LinearGradient colors={['rgba(0, 0, 0, 1)', 'rgba(0, 0, 0, 1)', 'rgba(0, 0, 0, 1)']} style={styles.descriptionContainer}>
            <View style={{flexDirection:'row',justifyContent:'space-between',padding:10,alignItems:'center'}}>
               <Text style={{color:colors.COLOR_PRIMARY,flex:8}}>{name.substring(0,100)}</Text>


              <View style={[styles.ratingContainer,{flex:1}]}>
              <Text style={{color:colors.BLACK}}>{parseFloat(rating).toFixed(1)}</Text>
              </View>

            </View>
            <Text style={{color:colors.GREY,fontSize:12}}>{summary}</Text>
            <View style={{height:5}}/>

              <ScrollView showsVerticalScrollIndicator={false}
               contentContainerStyle={{flexGrow:1,marginVertical:10}}>
              <Text style={{color:colors.WHITE}}>{description}</Text>
              </ScrollView>
              <View style={{height:10}}/>
             </LinearGradient>

           

             {/** attraction */}
              {
                attractions.length > 0 ?
                <View>
                   <View style={styles.divider}/>
                <View style={styles.attractionContainer}> 
                <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,marginVertical:10,fontWeight:'bold'}}>Attractions</Text>
                <Carousel
                ref={(c) => { this._carousel = c; }}
                data={attractions}
                renderItem={this._renderAttractions}
                autoplay={true}
                loop
                sliderWidth={dimensions.SCREEN_WIDTH * 0.9}
                itemWidth={dimensions.SCREEN_WIDTH * 0.6}
              />
              </View>
              </View>
              : null
              }

             {/** attraction  end*/}



                {/** hotel */}
                {
                  this.state.hotels.length > 0 ?
                  <View>
                    <View style={styles.divider}/>
                    <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,marginVertical:10,fontWeight:'bold'}}>Hotels</Text>
                          <FlatList
                          horizontal={true}
                          style={{marginVertical:10}}
                          data={this.state.hotels}
                          showsHorizontalScrollIndicator={false}
                          renderItem={(item,index) => this._renderHotel(item,index)}
                          keyExtractor={item => item.id}
                      />

                  </View>
                  :  null
                }
                
                
                 {/** hotel ends */}

        {/** Includes */}
                {
                  includes.length > 0 ?
                  <View>
                    <View style={styles.divider}/>
                <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,marginVertical:10,fontWeight:'bold'}}>Includes</Text>
                    <FlatList
                    contentContainerStyle={{alignSelf:'center'}}
                    style={{marginVertical:10}}
                  
                    numColumns={2}
                    data={includes}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderIncludes(item,index)}
                    keyExtractor={item => item.id}
                    />
                    </View>
                    :null
                }
        {/** Includes  end*/}


                    {/** Itneary */}
                      {
                        this.state.itinerary.length > 0 ?
                        <View>
                          <View style={styles.divider}/>
                      <View style={{flexDirection:'row',alignItems:'center',marginVertical:10, justifyContent:'space-between'}}>
                       <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,fontWeight:'bold'}}>Itinerary</Text>
                       <View style={styles.pickerContainer}>
                       <RNPickerSelect
                       style={{
                           ...pickerSelectStyles,

                         }}
                          Icon ={ () => <Icon style={{marginBottom:10,marginRight:10,marginTop:Platform.OS == 'android' ? 15 :0}}
                             name="caret-down" size={20} color={colors.COLOR_PRIMARY}/>}
                          placeholder={{
                              label: 'DAYS...',
                              value: null,
                              key:0
                          }}
                          placeholderTextColor={colors.COLOR_PRIMARY}
                          value={this.state.selectedDay}
                          items={this.state.daysItenary}
                          onValueChange={(value) => {
                            var arr = this.state.itinerary.filter(item =>{
                              return item.id == value
                            })
                          
                              this.setState({
                                selectedDay: value,
                                selectedItenary:arr[0]
                              });
                          }}
                         
                        
                         
                      />
                      </View>

                     

                      </View>
                      {
                        this.state.selectedItenary
                        ?
                        <View style={styles.itenaryContainer}>
                      
                        <Text style={{color:'grey',fontSize:15}}>{this.state.selectedItenary.title}</Text>
                        <Text style={{color:colors.WHITE,fontSize:12}}>{this.state.selectedItenary.description}</Text>
                       

                        </View>

                        :null
                      }
                        </View>
                        : null
                      }
                 {/** Itneary ends */}





                   {/** Guides */}
                   {
                     this.state.guides.length > 0
                     ? 
                     <View>
                       <View style={styles.divider}/>
                   <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,marginVertical:10,fontWeight:'bold'}}>Guides</Text>
                   <FlatList
                   horizontal={true}
                   style={{marginVertical:10}}
                   data={this.state.guides}
                   showsHorizontalScrollIndicator={false}
                   renderItem={(item,index) => this._renderGuides(item,index)}
                   keyExtractor={item => item.id}
               />
                   

                       </View>

                       : null
                   }
                  

                  
              {/** Guides ends */}


                 <TouchableOpacity onPress={() => this._terms()}
                 style={styles.buttonContainer}>
                 <Text style={{color:colors.COLOR_PRIMARY}}>Terms & Conditions</Text>
               </TouchableOpacity>

           </View>


         
        
        </KeyboardAwareScrollView>

        <View style={styles.bottomContainer}>
        <View style={{flexDirection:'row',justifyContent:'space-between',padding:5,alignItems:'center'}}>
            <View>  
              <Text style={{color:colors.COLOR_PRIMARY}}>{basic_price} {currency}</Text>
              <Text style={{color:'grey'}}>* Inclusive all taxes</Text>

            </View>
            <ButtonComponent 
            style={{width:dimensions.SCREEN_WIDTH * 0.4,alignSelf:'center'}}
              handler={this._onDone}
            label ={'Request'}/>
         </View>
         
        </View>
         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(ExperienceDetails);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderRadius:16,
    width:dimensions.SCREEN_WIDTH * 0.9,
    backgroundColor:colors.GREY,
    margin:7,
    height:230,
    borderWidth:0.8,borderColor:'black',overflow:'hidden'
   
},
hotelImage:{
  top:0,
  bottom:0,left:0,right:0,
  position:'absolute',overflow:'hidden'
},
hotelBottom:{
  position:'absolute',
  left:0,right:0,bottom:0,
  //height:80,
  padding:0,
  backgroundColor:'rgba(30,30,30,0.7)'
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  ratingContainer:{
    padding:6,borderRadius:20,
    justifyContent:'center',alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY,height:40,width:40
  },
  descriptionContainer:{
    padding:6,borderRadius:15,
   // backgroundColor:colors.BLACK,
    borderWidth:0.4,
    borderColor:colors.COLOR_PRIMARY,
    width:'85%',alignSelf:'center',
    marginBottom:10,
    marginTop:-50,
    //height:170
  }, 
  listContainer:{
    width:'45%',
    borderRadius:30,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    height:50,
    marginVertical:5,
    alignSelf:'center',
    overflow:'hidden',flexDirection:'row',alignItems:'center',marginHorizontal:10
  },
  facilitiesImage:{
    height:20,width:20,marginHorizontal:5
  },
  facilitiesText:{
      color:'white',
      fontWeight:'100',
      fontSize:13,
      marginLeft:10
  },
  rowContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
},
bottomContainer:{
  position:'absolute',
  bottom:0,left:0,right:0,
  height:65,
   backgroundColor:'rgba(30,30,30,0.99)',
  //backgroundColor:'rgba(0,0,0,0.91)',
  //ios    
shadowOpacity: 0.3,
shadowRadius: 3,
shadowOffset: {
    height: 0,
    width: 0
},
//android
elevation: 1,
borderTopColor:'black',
borderTopWidth:0.6

},
attractionsContainer:{
  width:'100%',
  height:190,
  borderRadius:10,
  backgroundColor:'grey',overflow:'hidden'
},
attractionsImage:{
  top:0,
  bottom:0,left:0,right:0,
  position:'absolute',overflow:'hidden'
},
attractionsText:{
  position:'absolute',
  left:10,
  bottom:10
},
attractionContainer:{
  padding:10,borderRadius:20,
  backgroundColor:colors.BLACK,
  borderWidth:0.4,
  borderColor:colors.BLACK,
  width:'99%',alignSelf:'center',
  marginBottom:10,
 
}, 
divider:{
  width:'100%',
  height:1,
  backgroundColor:'black',marginVertical:10
},
buttonContainer:{
  borderRadius:15,
  backgroundColor:colors.BLACK,
  padding:10,
  justifyContent:'center',
  alignItems: 'center',
  flex:1,
  marginHorizontal:10,
  alignSelf:'center',
  borderWidth:0.7
  ,borderColor:colors.COLOR_PRIMARY,
  marginVertical:13
},
pickerContainer:{
  height:40,
  width: 150,
  color: 'white',
  alignItems:"center",
  textAlign:"center",
  justifyContent:"center",
  fontSize:14,
  backgroundColor:colors.BLACK,
  borderRadius:15,
  overflow:'hidden'
},
itenaryContainer:{
  width:'90%',
  alignSelf:'center',
  borderColor:colors.COLOR_PRIMARY,
  borderWidth:0.6,
  borderRadius:15,
  padding:10,
  backgroundColor:'black'
 // height:100
},
guideContainer:{
  borderRadius:14,
  borderWidth:0.6,
  borderColor:colors.COLOR_PRIMARY,
  marginHorizontal:10,
  marginVertical:3,
  padding:8,
  width:dimensions.SCREEN_WIDTH * 0.7,
  backgroundColor:'black'
}

}
)
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    color: 'white',
    paddingLeft:50
  },
  inputAndroid: {
 
    color: 'white',
    marginLeft:43,
    alignItems:'center'
   
 
   
   
  },
});