// reducer taking the initial state as the first parameter.
//  As a second parameter we’ll provide action.




const initialState = {
  user: {  }
};
function rootReducer(state = initialState, action) {
  switch (action.type) {
      case "ADD_USER":
          return {
              ...state,
              user: action.payload
          };
      case "REMOVE_USER":
          return {
              ...state,
              user: {}
          };

      case "INVESTOR_USER":
            return {
                ...state,
                user: {
                    ...state.user,
                    'investor':1
                    }
            };

     case "REMOVE_INVESTOR_USER":

         let delEnt = {...state.user};

                    
         delete delEnt['investor'];

         return {
            ...state,user:{...state.user}

        };



      case "UPDATE_USER":
          return {
              ...state,
              user: {
              ...state.user,
              ...action.payload
              }
          };

      default: return state;
  }
}
export default rootReducer;

