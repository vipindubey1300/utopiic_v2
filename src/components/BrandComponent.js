import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'

export default class BrandComponent extends React.Component {
  state = {
   
  };

	

  render() {
     const {object} = this.props
    return (
        <TouchableOpacity  style={[styles.container]} onPress={()=> this.props.clickHandler(this.props.object)}>
        <View onPress={()=> this.props.clickHandler(this.props.object)}
         style={[styles.imageContainer]} >
            <FastImage 
            source={{uri: urls.base_url_media + object.brand_logo}}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.cover}/>
        </View>

        <View/>
        <View/>

        <View style={[styles.textContainer,{
          backgroundColor:this.props.textBackground ?
          this.props.textBackground : colors.BLACK
        }]}>
        <Text style={{color:'white'}}>{object.name}</Text>
        <Text style={{textAlign:'left',color:'white',fontSize:10}}> This is the description</Text>
        </View>


        {/* <FastImage 
        source={require('../assets/category1_icon.png')}
        style={styles.icon} 
        resizeMode={FastImage.resizeMode.cover}/> */}

       
        </TouchableOpacity>
     
    );

  }
}



const styles = StyleSheet.create({
  container: {
   
    margin:10,
   height:dimensions.SCREEN_HEIGHT * 0.28,
   width:dimensions.SCREEN_WIDTH * 0.43,
   overflow:'hidden',
   borderRadius:0,
   borderWidth:0,
   borderColor:colors.GREY,

  },
  imageContainer:{
    margin:0,
   flex:8,
   overflow:'hidden',
   borderRadius:0,
   borderWidth:0,
   borderColor:colors.GREY,
   backgroundColor:'#808080',
  },

 
  imageStyle:{
  
   height:'100%',width:'100%',
  },
  icon:{
      position: 'absolute',right: 10,
      top:0,height:25,width:25
  },
  textContainer:{paddingVertical:5,
    paddingHorizontal:3,backgroundColor:colors.BLACK,flex:3,justifyContent:'center'}
});