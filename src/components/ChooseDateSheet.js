import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'
import RBSheet from "react-native-raw-bottom-sheet";
 import DateTimePickerModal from 'react-native-modal-datetime-picker';
//import DateTimePicker from '@react-native-community/datetimepicker';
import ButtonComponent from './ButtonComponent';
import DatePicker from 'react-native-datepicker'

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];



export default class ChooseDateSheet extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            fromDate:'',
            toDate:'',
            isDateTimePickerVisibleFrom:false,
            isDateTimePickerVisibleTo:false

      };

    }

    convertDateFull(date) {
      var yyyy = date.getFullYear().toString();
      var mm = (date.getMonth()).toString();
      var dd  = date.getDate().toString();
  
      var mmChars = mm.split('');
      var ddChars = dd.split('');
      
      console.log("The current month is " + monthNames[date.getMonth()])
      console.log("The current day is " + date.getDate())
      console.log("The current year is " +date.getFullYear())

      var d = date.getDate() + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear()


      return d
    }


    componentWillMount=()=>{

      var date = new Date();
      date.setDate(date.getDate() + 1); //1 day after

        this.setState({
                fromDate:this.convertDateFull(new Date()),
                toDate:this.convertDateFull(date)
       })
    }

    convertDate(date) {
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth()+1).toString();
        var dd  = date.getDate().toString();
    
        var mmChars = mm.split('');
        var ddChars = dd.split('');
    
        return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
      }
    

     
    
      _showDateTimePickerFrom = () => this.setState({ isDateTimePickerVisibleFrom: true });
      _hideDateTimePickerFrom = () => {
        this.setState({ isDateTimePickerVisibleFrom: false });
      }
      handleDatePickedFrom = (date) => {
        console.log('A date has been picked: ',date);
        this.setState({fromDate:this.convertDateFull(date)})
       // this.setState({date_obj:this.convertDate(date),dob:date.toString().substring(4,15)})
        this._hideDateTimePickerFrom();
      };

      _showDateTimePickerTo = () => this.setState({ isDateTimePickerVisibleTo: true });
      _hideDateTimePickerTo = () => {
        this.setState({ isDateTimePickerVisibleTo: false });
      }
    
      handleDatePickedTo = (date) => {
        console.log('A date has been picked------: ',this.convertDate(date));
        this.setState({toDate:this.convertDateFull(date)})
       // this.setState({date_obj:this.convertDate(date),dob:date.toString().substring(4,15)})
        this._hideDateTimePickerTo();
      };

      _onDone =()=>{
            this.props.success(this.state.fromDate,this.state.toDate)
      }
   


  
    render() {

       
        return(
            <RBSheet
            ref={this.props.inputRef}
            height={dimensions.SCREEN_HEIGHT * 0.5}
            duration={240}
            customStyles={{
              container: {
                alignItems: "center",
                justifyContent:'center',
                backgroundColor:colors.LIGHT_BLACK,
                padding:10,
                borderTopLeftRadius:0,
                 borderTopRightRadius:0,
             
              }
            }}
            animationType='fade'
            minClosingHeight={10}
          >
            

          <DateTimePickerModal
                mode="date"
             isVisible={this.state.isDateTimePickerVisibleFrom}
             onConfirm={this.handleDatePickedFrom}
             onCancel={this._hideDateTimePickerFrom}
             //maximumDate={new Date()}
              >
          </DateTimePickerModal>


          <DateTimePickerModal
          mode="date"
       isVisible={this.state.isDateTimePickerVisibleTo}
       onConfirm={this.handleDatePickedTo}
       onCancel={this._hideDateTimePickerTo}
       //maximumDate={new Date()}
        >
    </DateTimePickerModal>


                 {/** date section */}
                 <View style={{marginVertical:10}}/>


                 <View style={{alignSelf:'center',width:'80%'}}>
                 <Text style={styles.dateHeading}>From</Text>
                 <TouchableOpacity onPress={this._showDateTimePickerFrom} style={styles.dateContainer}>
                    <Text style={{color:'white'}}>{this.state.fromDate}</Text>
                    <FastImage 
                    source={require('../assets/calendar.png')}
                    style={{height:30,width:30}} 
                    resizeMode={FastImage.resizeMode.contain}/>
                 </TouchableOpacity>

                <View style={{marginVertical:10}}/>

                 <Text style={styles.dateHeading}>To</Text>
                 <TouchableOpacity onPress={this._showDateTimePickerTo} style={styles.dateContainer}>
                 <Text style={{color:'white'}}>{this.state.toDate}</Text>
                 <FastImage 
                    source={require('../assets/calendar.png')}
                    style={{height:30,width:30}} 
                    resizeMode={FastImage.resizeMode.contain}/>
                 </TouchableOpacity>


                 </View>

                 <ButtonComponent 
                 style={{marginTop:25,width:dimensions.SCREEN_WIDTH * 0.4,alignSelf:'center'}}
                 handler={this._onDone}
                 label ={'Select Date'}/>


              {/** date section end */}
            
          </RBSheet>
          )
     
  
  
    }
}





let styles = StyleSheet.create({
    header:{
      
         backgroundColor:colors.BLACK,
         borderBottomRightRadius:20,
         height:190
    },
   
    
    rowContainer:{
        flexDirection:'row',
       // justifyContent:'space-between',
        alignItems:'center',
       
    },
    dateContainer:{
      borderColor:colors.COLOR_PRIMARY,
      borderRadius:16,
      borderWidth:0.3,
      marginRight:10,
      padding:10,
      backgroundColor:'black',
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
    },
    dateHeading:{
        color:colors.COLOR_PRIMARY,
        marginVertical:7
    },
    bottomContainer:{
      position:'absolute',
      bottom:0,left:0,right:0,
      height:45,
       backgroundColor:'rgba(30,30,30,0.7)',
     // backgroundColor:'rgba(0,0,0,0.91)',
     borderTopColor:'black',
     borderTopWidth:0.7
  
    
    }
  
  }
  )