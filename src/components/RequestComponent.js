import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'

export default class RequestComponent extends React.Component {
    constructor(props) {
        super(props);
        this.delayValue = 500;
        this.state = {
          animatedValue: new Animated.Value(0),
          data:[]
        }
      }

      componentDidMount = () => {
        Animated.spring(this.state.animatedValue, {
          toValue: 1,
          tension: 20,
          useNativeDriver: true
        }).start();
      }


    
	

  render() {
         //admin_status  0 - pending from admin side 1 admin approved the booking
        //status 0 -pending 1 Approved 2 Paid 3 Completed
        //payment_status - pending or paid
        //booking_type_name - this will be the type of booking like package, hotel, transport, native, tour to transfer


      const {object,index} = this.props


      this.delayValue = this.delayValue + 500;
      const translateX = this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [this.delayValue, 1]
      });

 
       return (
          <Animated.View  style={{ transform: [{ translateX }]}}>

           <View style={[styles.requestContainer,{
               borderColor:object.status == 3 ? colors.COLOR_PRIMARY :'grey'
           }]}>
            {
                object.status > 1
                ?
                <View>
                <Text style={{color:'white',fontSize:16,lineHeight:27}}>{object.name},{object.booking_type_name}</Text>
                <View style={{height:10}}/>
                <Text style={styles.greyText}>Amount </Text>
                    <Text  style={{color:colors.COLOR_PRIMARY,fontSize:13,lineHeight:27}}>{parseFloat(object.basic_price) +parseFloat(object.discount) + parseFloat(object.convenience_fees)  + parseFloat(object.publish_price)} {object.currency}</Text>
                <View style={{height:10}}/>
                <Text style={styles.greyText}>Booking Code </Text>
                     <Text  style={{color:colors.WHITE,fontSize:13,lineHeight:27}}>{object.booking_code}</Text>

                <View style={[styles.rowContainer,{justifyContent:'space-between'}]}>
                    <Text style={{color:colors.DARK_GREY,fontSize:13,lineHeight:27}}>{object.date}</Text>
                    <View style={styles.rowContainer}>
                   
                    <Text  style={{color:colors.COLOR_PRIMARY,fontSize:11}}>Completed   </Text>
                    <FastImage 
                    source={require('../assets/checkcircle-filled-yellowpng.png')}
                    style={{height:25,width:25,marginHorizontal:3,flexWrap:'nowrap'}} 
                    resizeMode={FastImage.resizeMode.contain}/>

                    </View>
                </View>
                </View>
                :

               <View>
                     <View style={[styles.rowContainer,{justifyContent:'space-between'}]}>
                        <Text style={styles.greyText}>Request For </Text>
                        <View style={styles.rowContainer}>
                        
                        <Text  style={styles.greyText}>Pending... </Text>
                        <FastImage 
                        source={require('../assets/imgp.png')}
                        style={{height:15,width:15,marginHorizontal:3,flexWrap:'nowrap'}} 
                        resizeMode={FastImage.resizeMode.contain}/>

                        </View>
                    </View>

            <Text style={{color:'white',fontSize:16,lineHeight:27,textTransform:'capitalize'}}>{object.name},{object.booking_type_name}</Text>

                    <View style={{height:12}}/>

                    <Text style={styles.greyText}>Amount </Text>
                    <Text  style={{color:colors.COLOR_PRIMARY,fontSize:13,lineHeight:27}}>{parseFloat(object.basic_price) +parseFloat(object.discount) + parseFloat(object.convenience_fees)  + parseFloat(object.publish_price)} {object.currency}</Text>


                    <View style={{height:12}}/>

                    <Text style={styles.greyText}>Request status </Text>
                    <Text  style={{color:colors.WHITE,fontSize:13,lineHeight:27}}>Pending</Text>

                    <View style={{height:12}}/>

                    <Text style={styles.greyText}>Booking Code </Text>
                     <Text  style={{color:colors.WHITE,fontSize:13,lineHeight:27}}>{object.booking_code}</Text>



                    <View style={{height:12}}/>

                    <Text style={styles.greyText}>Request Date </Text>
                    <View style={[styles.rowContainer,{justifyContent:'space-between'}]}>
                    <Text style={{color:colors.WHITE,fontSize:13,lineHeight:27}}>{object.date} </Text>
                    {
                      object.admin_status == 1
                       // object.admin_status 
                        ?  <TouchableOpacity onPress={()=> this.props.onPay(object)}>
                        <FastImage 
                        source={require('../assets/circle-next.png')}
                        style={{height:25,width:25,marginHorizontal:3,flexWrap:'nowrap'}} 
                        resizeMode={FastImage.resizeMode.contain}/>
                        </TouchableOpacity>
                        :
                        null
                    }
                </View>

               </View>


            }
       
         </View>
         </Animated.View>
       )

  }
}



const styles = StyleSheet.create({
    requestContainer:{
        width:'95%',
        paddingHorizontal:5,
        paddingVertical:10,
        backgroundColor:colors.BLACK,
        borderRadius:20,
        marginVertical:5,
        alignSelf:'center',
        borderWidth:1,
        borderColor:'black'
       
      },
      yellowText:{
        color:colors.COLOR_PRIMARY
      },
      whiteText:{
        color:colors.WHITE
      },
      rowContainer:{
        flexDirection:'row',
       // justifyContent:'space-between',
        alignItems:'center',
       
    },
    greyText:{
        color:'grey',
        fontSize:12
      },
});