import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,
  ActivityIndicator,TextInput, SafeAreaView,Keyboard,} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
import Video from 'react-native-video';
import { FlatList } from 'react-native-gesture-handler';
import { Button } from 'native-base';
import FastImage from 'react-native-fast-image'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


import { colors ,urls,fonts,dimensions} from '../app_constants';
import {showMessage,showToastMessage} from '../utils/showMessage';
import ProgressBar from '../components/ProgressBar';
import TextInputComponent from '../components/TextInputComponent';
import RoundButton from '../components/RoundButton';
import VerifyOtpSheet from '../components/VerifyOtpSheet';

import { connect } from 'react-redux';
import { addUser } from '../actions/actions';

class Register extends React.Component {

	constructor(props) {
    super(props);
    this.state={
      loading_status:false,
      deviceToken:'324323243cfffgfdgfd'
    }
  }


       
  componentDidMount =()=>{
    let name = this.props.user.name
    let email = this.props.user.email
    let mobile = this.props.user.phone

    this.nameInput.setState({text:name})
    this.emailInput.setState({text:email})
    this.mobileInput.setState({text:mobile})
}

  loginHandler =() =>{
   this.props.navigation.navigate("Home")
  }


  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  _isValid(){
      console.log("0000000")
   const passcode = this.passcodeInput.getInputValue();
   if(passcode.trim().length == 0){
      showMessage('Enter PassCode')
      return false
    }
    else{
      return true;
    }
  }

  _onPasscode = () =>{
   
    if(this._isValid()){
     
        this.setState({loading_status:true})
        var formData = new FormData();
        const passcode = this.passcodeInput.getInputValue();
      
       formData.append('passcode', passcode);
       formData.append('device_id', this.state.deviceToken);
       Platform.OS =='android'
       ?  formData.append('type',1)
       : formData.append('type',2)

       Keyboard.dismiss()
    
     let url = urls.base_url +'auth/passcode'
             
     fetch(url, {
               method: 'POST',
               body: formData
              }).then((response) => response.json())
                   .then(async (responseJson) => {
                       this.setState({loading_status:false})
    
                    console.log("FFF",JSON.stringify(responseJson))
                    
                    if (responseJson.status){

          
    
                      var  user_id = responseJson.data.customer_id.toString()
                      var name = responseJson.data.name
                      var email = responseJson.data.email
                      var image = responseJson.data.customer_image
                      var token = responseJson.data.token
                      var phone = responseJson.data.phone
    
                      AsyncStorage.multiSet([
                        ["user_id", user_id.toString()],
                        ["email", email.toString()],
                        ["name", name.toString()],
                        ["image", image.toString()],
                        ["token", token.toString()],
                        ["phone", phone.toString()],
                        ]);
  
                       await this.props.add({ 
                          user_id: user_id, 
                          name : name,
                          image : image,
                          email:  email ,
                          phone : phone,
                          token:  token 
                        })

                        showMessage('Correct Passcode')
                        
                       // this.setState({passcode:''})
                        //this.props.navigation.navigate('Register');
                      
                       }else{
                        showMessage(responseJson.message)
                        
                        
                      }
                   }).catch((error) => {
                             this.setState({loading_status:false})
                          
                             showToastMessage(error.message)
                   });
     }
   
  }

  _next =()=>{
      // this.props.navigation.navigate("Preferences")
      this.verifySheet.open()
  }
  
  
    _onLogin = () =>{
      if(this._isValid()){
      Keyboard.dismiss()
      const email = this.emailInput.getInputValue();
      const password = this.passwordInput.getInputValue();
  
    
        this.setState({loading_status:true})
        var formData = new FormData();
      
       formData.append('email',email);
       formData.append('password', password);
       formData.append('device_token', 'jaBBD87dg7D');
       Platform.OS =='android'
       ?  formData.append('device_type',1)
       : formData.append('device_type',2)
    
    
         console.log("FFF",JSON.stringify(formData))
    
    
               let url = urls.BASE_URL +'api_login'
              console.log("FFF",JSON.stringify(url))
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData
              }).then((response) => response.json())
                   .then(async (responseJson) => {
                       this.setState({loading_status:false})
    
                       console.log("FFF",JSON.stringify(responseJson))

    
                    if (!responseJson.error){
    
                      var id = responseJson.result.id
                      var name = responseJson.result.name
                      var email = responseJson.result.email
                      var phone = responseJson.result.mobile
                      var image = responseJson.result.user_image
                      var status = responseJson.result.otp_verified   
                     
                      showMessage(responseJson.message,false)

                      if(status == 1){
                         //verified user
                         AsyncStorage.multiSet([
                          ["id", id.toString()],
                          ["email", email.toString()],
                          ["name", name.toString()],
                          ["phone", phone.toString()],
                          ["image", image.toString()],
                         
                          ]);
  
                          console.log("Saving----",id)
    
                         await  this.props.add({ 
                            id: id, 
                            phone : phone,
                            image : image,
                            email:  email ,
                            name:name,
                            
                          })
                          this.props.navigation.navigate("Home")
                      }
                      else if(status == 0){
                        let tempObject = {
                          'image':image,
                          'id' : id,
                          'email':email,
                          'phone':phone,
                          'name':name,
                        
                          'path':'login'//path will decide route from otp screen
                         }
                        this.props.navigation.navigate("Otp",{result:tempObject})

                      }
  
                 
                              
                       }else{
  
                           showMessage(responseJson.message)
                        
                         }
                   }).catch((error) => {
                     console.log(error.message)
                             this.setState({loading_status:false})
                             showMessage('Try Again')
    
                   });
      }
    
     }


    _success =()=>{
      this.verifySheet.close()
      this.props.navigation.navigate("Preferences")
      
  }


	render() {
    return (
      <>
      <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
      <VerifyOtpSheet
      inputRef={ref => this.verifySheet = ref} //for close/open sheet
      ref={ref => this.verifyOtpSheet = ref}
      success ={this._success}
    />
        <SafeAreaView style={styles.container}>
                {/* position absolute make the image to go in safe are so wrapping in view solve this issue */}
                {/* image background */}
                <View>
                  <Image source={require('../assets/image-bg.png')} 
                    resizeMode="stretch"
                    style={styles.imageContainer}/>
                 </View>
                {/* image background  end*/}


                    <KeyboardAwareScrollView
                    contentContainerStyle={styles.scrollContainer}>

                    

                    <FastImage 
                      source={require('../assets/logo.png')}
                      style={styles.headerLogo} 
                      resizeMode={FastImage.resizeMode.contain}/>

                      <Text style={styles.headingText}>Fill Your Basic Information</Text>
                      <Text style={styles.headingSmallText}>It will help us personalize your experience.</Text>


                   
                      <TextInputComponent
                      placeholder={'Enter Name'}
                    //  onSubmitEditing={()=> this._onPasscode()}
                      //onFinish={this._onPasscode}
                      inputRef={ref => this.name = ref}
                      ref={ref => this.nameInput = ref}
                      style={{width:'90%'}}
                      returnKeyType="go"
                  />

                      <TextInputComponent
                        placeholder={'Enter Email'}
                        //onSubmitEditing={()=> this._onPasscode()}
                       // onFinish={this._onPasscode}
                        inputRef={ref => this.passcode = ref}
                        ref={ref => this.emailInput = ref}
                        style={{width:'90%'}}
                        returnKeyType="go"
                    />

                    <TextInputComponent
                    placeholder={'Enter Phone Number'}
                    //onSubmitEditing={()=> this._onPasscode()}
                    //onFinish={this._onPasscode}
                    keyboardType="numeric"
                    textContentType='telephoneNumber'
                    inputRef={ref => this.passcode = ref}
                    ref={ref => this.mobileInput = ref}
                    style={{width:'90%'}}
                    returnKeyType="go"
                />
                <RoundButton
                handler={this._next}
                />

                    


                
                  
                  </KeyboardAwareScrollView>

                  { this.state.loading_status && <ProgressBar/> }
             
        </SafeAreaView>
      </>
    );
	}
}

const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (user_info) => dispatch(removeUser(user_info)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Register);

let styles = StyleSheet.create({
  container:{
  //  height:'100%',
    flex:1,
    backgroundColor:colors.BLACK
  },
  imageContainer:{
      //  position:'absolute',
      //  top:0,left:0,right:0,bottom:0


      //height and width will not go in safe area means notch me nhi 
      //  height:'100%',
      // width:'100%'

      position: 'absolute',
      flex: 1,
      backgroundColor:'rgba(0,0,0,0.45)',
      width:dimensions.SCREEN_WIDTH,
      height: dimensions.SCREEN_HEIGHT
  },

  scrollContainer:{
    padding:10,
    alignItems:'center'
  },
  headerLogo:{
      height: dimensions.SCREEN_HEIGHT * 0.15, 
      width:dimensions.SCREEN_WIDTH * 0.4, 
      margin:10

  },
  headingText:{
    color:colors.WHITE,
    fontSize:18,
    marginVertical:25,
    fontWeight:'bold'
  },
  headingSmallText:{
    color:colors.DARK_GREY,
    fontSize:13,
    marginVertical:15,
    fontWeight:'100'
  },
 

})

