import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,
  ActivityIndicator,TextInput, SafeAreaView,Keyboard} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
import Video from 'react-native-video';
import { FlatList } from 'react-native-gesture-handler';
import { Button } from 'native-base';
import FastImage from 'react-native-fast-image'


import { colors ,urls,fonts,dimensions} from '../app_constants';
import {showSnackMessage,showToastMessage} from '../utils/showMessage';
//import ProgressBar from '../components/ProgressBar';

import { connect } from 'react-redux';
import { addUser } from '../actions/actions';

class Splash extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
         
        }
    }

    home = async () =>{
     
      await AsyncStorage.multiGet(["user_id", "name","image","email",'phone','user_type','token','passcode']).then(response => {
        console.log(response[0][0]) // Key1
        console.log(response[0][1]) // Value1
        console.log(response[1][0]) // Key2
        console.log(response[1][1])// Value2
        console.log(response)
  
        let id = response[0][1]
        let name = response[1][1]
        let image = response[2][1]
        let email = response[3][1]
        let phone = response[4][1]
        let user_type = response[5][1]
        let token = response[6][1]
        let passcode = response[7][1]

       
  
        this.props.add({ 
          user_id: id, 
          name : name,
          image : image,
          email:  email ,
          phone:phone,
          user_type:user_type,
          token:token,
          passcode:passcode
        
        })

        if(user_type =='6'){
          console.log("Supplier--------")
          //supplier
          this.props.navigation.navigate('HomeSupplier') 
          return
          
        }
      
        this.props.navigation.navigate('Home') 
         
    })

   
     
    
    
    }
  
  




  checkLoginStatus = async () =>{
    await AsyncStorage.getItem('user_id')
              .then((value) => {
                if(value){
                    this.home()
                }
  
                else{
                this.props.navigation.navigate('Passcode')
  
                }
              });
  
  }

    
  check = async () =>{
    setTimeout(() => {
            
       this.checkLoginStatus()
    }, 2000)
 
 
   }


   componentDidMount() {
       this.check()
   }





    render() {
      

      return (
        <>
          <StatusBar barStyle="light-content"  //dark-content//light-content
            backgroundColor={colors.STATUS_BAR_COLOR}/>
             <SafeAreaView style={styles.container}>

             <FastImage 
                 source={require('../assets/logo.png')}
                 style={styles.splash_logo} 
                 resizeMode={FastImage.resizeMode.contain}/>


              <View style={{alignItems:'center',marginTop:-40}}>
              <FastImage 
                  source={require('../assets/logo-info.png')}
                  style={styles.centerLogo} 
                  resizeMode={FastImage.resizeMode.contain}/>
              </View>


              <View/>
               
             </SafeAreaView>


                 {/* <FastImage 
                  source={require('../assets/logo-text.png')}
                  style={styles.splash_logo} 
                  resizeMode={FastImage.resizeMode.contain}/>
           */}
        </>
      );
    }
}

const mapDispatchToProps = dispatch => {
  return {
    // dispatching plain actions
    add: (userinfo) => dispatch(addUser(userinfo)),
  
    
  }
}

export default connect(null, mapDispatchToProps)(Splash);


let styles = StyleSheet.create({
    container:{
      flex: 1,
      justifyContent:'space-between',
      alignItems:'center',
      backgroundColor:'black',
      padding:20
    },
    splash_image:{
      height: dimensions.SCREEN_HEIGHT * 1, 
      width:dimensions.SCREEN_WIDTH * 1, 
    },
    splash_logo:{
      height: 70, 
      width:70, 
     alignSelf:'flex-start',
     marginLeft:20
     
    },
    centerLogo:{
      height: 180, 
      width:180, 
      marginTop:-50
    }
   

})
