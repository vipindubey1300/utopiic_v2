import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,
  ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors, dimensions } from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';

import SwiperFlatList from 'react-native-swiper-flatlist';


import FastImage from 'react-native-fast-image'
import RoundButton from '../components/RoundButton';

import PreferencesComponent from '../components/PreferencesComponent';
import {showMessage,showToastMessage} from '../utils/showMessage';
import { connect } from 'react-redux';
import ProgressBar from '../components/ProgressBar';
import Swiper from '@starodubenko/react-native-deck-swiper'

import {  urls } from '../app_constants';

 const d = Dimensions.get("window")



class Preferences extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          preferences:[],
          baseUrl:'',
          selectedIndex:0,
          swipedAllCards: false,
          swipeDirection: '',
          cardIndex: 0,
          selectedPreferences:0,

      };

    }



    _savePreferences = () =>{
     
      var formData = new FormData();
      formData.append('preference_id',this.state.preferences[this.state.selectedIndex].id);
      //console.log('responseJson',formData)
      this.setState({loading_status:true})
    
               let url = urls.base_url +'package/add_preference'
          
                    fetch(url, {
                    method: 'POST',
                    body:formData,
                    headers: {
                      
                      //'Content-Type': 'application/x-www-form-urlencoded',
                      'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                        //  console.log("))))))))))",responseJson)
                          this.setState({loading_status:false})

                          if(responseJson.status){
                           showMessage(responseJson.message,false)
                           this.swiper.swipeLeft()
                          }
                          else{
                            showMessage(responseJson.message)
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         //console.log(error)
                        });
    
    
    
    }



    _fetchPreferences = async () =>{

      this.setState({loading_status:true})
    
               let url = urls.base_url +'package/preference'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                      'Content-Type': 'application/json',
                       'token': this.props.user.token,
                      // 'token':"vFBd+r5qNmIDOVJGgF3XHOvqWjnVK8fM9b4/ml/10e6LEXsd+oUN1/RT1x1v06QJOvpGGN7s+nV8Z4U6DQD4BGqtsvY4itiQgEHykswYEokgDk1N3svPC5ChKJyIxnpzOWVQaPaj7ugs3noyHCkNK86b75rRRBKxOoZfkJRAW4Q="
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                                console.log(JSON.stringify(responseJson))
                         
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data[0].list
                            var baseUrl = responseJson.data[0].image_path
                           
                           this.setState({preferences:list,baseUrl:baseUrl})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }


    componentWillMount(){
        this._fetchPreferences()
    }


    renderItem = ({item, index}) => {
      return(
        <PreferencesComponent 
        base_url={this.state.baseUrl}
         preferences={this.state.preferences} index={index}

        />
      )
     }


   _emptyList = () => {
      return (
       <Text style={{color:'yellow'}}>No Preferences Found</Text>
      );
    }

  

    renderCard = (card, index) => {
      // console.log('renderCard',card)
       //below condition check is necessary as setstate was not workign after fetching data
       if(card){
         return (
           <View style={{height:220,width:250,marginLeft:Dimensions.get('window').width * 0.04}}>
          <PreferencesComponent ref={ref => this.preferencePage = ref} 
          index={index}
          base_url={this.state.baseUrl}
          preference={card}/>
          </View>
          
         )
       }
     };
   
     onSwiped = (type,index) => {
       //console.log(`on swiped ${index}`)
       // this.setState({
       //   selectedModel : index
       // })
       this.setState(prevState => ({ selectedPreferences: index + 1 }));
     }
   
     onSwipedAllCards = () => {
       this.setState({
         swipedAllCards: true
       })
       this.props.navigation.navigate("Home")
     };
    
   
     swipeLeft = () => {
       this.swiper.swipeLeft()
     };

    
_next =()=>{
  this.props.navigation.navigate("Home")
}

    render() {

      //let { children, ...props } = this.props;
   

  
        var children =this.state.preferences.map((preference,index) => {
         
          return (
  
            <PreferencesComponent ref={ref => this.preferencePage = ref} 
            index={index}
            base_url={this.state.baseUrl}
            preference={preference}/>
          )
        })
      

        return (
            <SafeAreaView style={{backgroundColor:'black'}}>
          <StatusBar
              backgroundColor="black"
              barStyle="light-content"
          />
          <ImageBackground
          source={require('../assets/image-bg-preferences.png')}
          resizeMode='stretch'
          style={{width: '100%', height: '100%',backgroundColor:'black'}}>

              <KeyboardAwareScrollView
              alwaysBounceVertical={false}
              contentContainerStyle={{justifyContent:'center',
              alignItems:'center',flexGrow: 1}}
              style={styles.container}>

                    <View>

                      {/* heading */}
                      <View style={{flexDirection:'row',alignItems:'center',alignSelf:'flex-start',marginLeft:30}}>
                      <FastImage 
                      source={require('../assets/preferences_filter.png')}
                      style={{height:25,width:25}} 
                      resizeMode={FastImage.resizeMode.contain}/>
                      <Text style={styles.heading}>   Preferences</Text>
                      </View>
                      
                      <Text style={styles.title}>Selecting your preferences will help us to know what you want.This enables us to server you best.</Text>
                        {/* heading end */}


                       <View
                         style={{margin:40,backgroundColor:'rgba(0,0,0,0.001)',
                         height:d.height * 0.36,width:d.width * 0.9,alignSelf:'center',alignItems:'center'}}>
                         

                         <Swiper
                         ref={swiper => {
                           this.swiper = swiper
                           }}
                          backgroundColor={'rgba(0,0,0,0.0000000001)'}
                          // childrenOnTop={true}
                           showSecondCard={true}
                           verticalSwipe={false}
                           horizontalSwipe={true}
                           onSwiped={(index) => this.onSwiped('general',index)}
                           onSwipedLeft={(index) => this.onSwiped('left',index)}
                           onSwipedRight={(index) => this.onSwiped('right',index)}
                           onSwipedTop={(index) => this.onSwiped('top',index)}
                           onSwipedBottom={(index) => this.onSwiped('bottom',index)}
                           //onTapCard={this.swipeLeft}
                           //cards={pager}
                           cards={this.state.preferences}
                           cardIndex={this.state.cardIndex}
                           cardVerticalMargin={0}
                            renderCard={this.renderCard}
                           onSwipedAll={this.onSwipedAllCards}
                           stackSize={3}
                           stackSeparation={15}
                           animateOverlayLabelsOpacity
                           animateCardOpacity
                           swipeBackCard
                 >
                   {/* <Button onPress={() => this.swiper.swipeBack()} title='Swipe Back' /> */}
                 </Swiper>
      
                       </View>

                       <Text style={{color:'white',alignSelf:'center',fontSize:16}}>Interested ?</Text>


                       <View style={{
                         flexDirection:'row',
                         alignItems:'center',
                         marginBottom:20,
                         width:'100%',
                         marginTop:20,
                         marginLeft:20,
                         justifyContent:'center'
                       }}>

                            <TouchableOpacity onPress={()=>   this.swiper.swipeRight()}
                            style={{width:70,height:40,marginHorizontal:20,borderRadius:20,backgroundColor:colors.LIGHT_BLACK, padding:0,borderColor:colors.LIGHT_BLACK,borderWidth:0.6,justifyContent:'center',alignItems:'center'}}>
                              <Text style={{color:colors.WHITE}}>No</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={()=>{this._savePreferences()}}
                             style={{width:70,height:40,marginHorizontal:20,borderRadius:20,backgroundColor:colors.BLACK, padding:0,borderColor:colors.COLOR_PRIMARY,borderWidth:0.6,justifyContent:'center',alignItems:'center'}}>
                            <Text style={{color:colors.COLOR_PRIMARY}}>Yes</Text>
                            </TouchableOpacity>

                       </View>

                       <RoundButton style={{alignSelf:'center'}}
                        handler={this._next}
                         />

                



                    </View>


              </KeyboardAwareScrollView>

          </ImageBackground>
          { this.state.loading_status && <ProgressBar/> }

            </SafeAreaView>




        )
    }
}


const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (user_info) => dispatch(removeUser(user_info)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Preferences);

let styles = StyleSheet.create({
  container:{
        flex: 1,


  },
  imageBackground:{

    position: 'absolute',
    flex: 1,
    backgroundColor:'rgba(0,0,0,0.45)',
    width: d.width,

    zIndex:-2,
    height: d.height * 0.97,

  },
  heading:{
    fontSize:19,
    color:'black',
    fontWeight:'bold',
      textAlign:'center',
  },
  title:{
    fontSize:14,
    color:'black',
    fontWeight:'400',
    marginBottom:20,
    alignSelf:'flex-start',
    width:dimensions.SCREEN_WIDTH * 0.6,
    marginLeft:30
  },
    input: {
    width: d.width * 0.64,
		marginHorizontal: 40,
		paddingTop: 0,
		marginTop: 0,
  },
  buttonContainer:{
    marginTop:10,
    padding:10,
    width:null,
    height:null,
    alignSelf:'center',
    flexDirection:'row',
    backgroundColor:'#BE863A',
    alignItems:'center'

}

}
)
