import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'

import { withNavigation } from 'react-navigation';

import {colors,urls,dimensions} from '../app_constants';

export default class BottomBarSupplier extends React.Component {
    constructor(props) {
        super(props);
        this. state = {
            selectedTab:'home'
        };
    }

    // componentDidUpdate( prevProps,prevState) {
    //   // if (nextProps.selectedTab !== this.props.selectedTab) {
    //   //   this.setState({ text: nextProps.defaultValue.toString() });
    //   // }
    //   console.log('@@@@@@@@@@',prevState)
    // }

    

    getSelectedTab = () => this.state.selectedTab;
    
    _onPressHome =()=>{
          this.setState({ selectedTab:'home'},()=>{
               this.props.handler('home')
          })
    }

    _onPressSuppliers =()=>{
      this.setState({ selectedTab:'suppliers'},()=>{
       this.props.handler('suppliers')
     })
      
    }

    _onPressRequest =()=>{
        this.setState({ selectedTab:'request'},()=>{
          this.props.handler('request')
       })
        
      }
   
  
    render(){
        const {selectedTab } = this.state
        return (
            <View style={styles.container}>


            {
               selectedTab == 'home'
               ?
               <TouchableOpacity style={styles.imageContainer}>
               <FastImage 
               source={require('../assets/home_white.png')}
               style={styles.image} 
               resizeMode={FastImage.resizeMode.cover}/> 
                </TouchableOpacity>
                :
                <TouchableOpacity style={styles.imageContainer}
                onPress={()=> this._onPressHome() }>
                <FastImage 
                source={require('../assets/home_yellow.png')}
                style={styles.image} 
                resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>

            }
            {
                selectedTab == 'suppliers'
                ?
                <TouchableOpacity style={styles.imageContainer}>
                <FastImage 
                source={require('../assets/supplier-white.png')}
                style={styles.image} 
                resizeMode={FastImage.resizeMode.cover}/> 
                 </TouchableOpacity>
                 :
                 <TouchableOpacity style={styles.imageContainer}
                 onPress={()=> this._onPressSuppliers() }>
                 <FastImage 
                 source={require('../assets/supplier-yellow.png')}
                 style={styles.image} 
                 resizeMode={FastImage.resizeMode.cover}/>
                 </TouchableOpacity>
 
             }

             {
                selectedTab == 'request'
                ?
                <TouchableOpacity style={styles.imageContainer}>
                <FastImage 
                source={require('../assets/chat_white.png')}
                style={styles.image} 
                resizeMode={FastImage.resizeMode.cover}/> 
                 </TouchableOpacity>
                 :
                 <TouchableOpacity style={styles.imageContainer}
                 onPress={()=> this._onPressRequest() }>
                 <FastImage 
                 source={require('../assets/chat_yellow.png')}
                 style={styles.image} 
                 resizeMode={FastImage.resizeMode.cover}/>
                 </TouchableOpacity>
 
             }


           
               
            </View>
           );
        }
};

BottomBarSupplier.defaultProps = {
    handler: () => {},
    style: {},
   
  };



const styles = StyleSheet.create({

  container:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',  
    backgroundColor:'rgba(30,30,30,0.7)',
    width:dimensions.SCREEN_WIDTH,
    height:60,
    paddingHorizontal:10
    // shadowColor: '#000000',
    // shadowOffset: {
    //   width: 0,
    //   height: 3
    // },
    // shadowRadius: 5,
    // shadowOpacity: 1.0,
    // elevation:1,

  },
  image:{
      height:22,
      width:22
  },
  imageContainer:{
      flex:1,
      alignItems:'center'
  }
  

})
