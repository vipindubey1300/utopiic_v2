import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';



import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import SearchInput from '../components/SearchInput';
import { showMessage } from '../utils/showMessage';
import ZendeskChat from "react-native-zendesk-chat";
ZendeskChat.init("jGCc8CzMayUufCUYqYqGcewgZt5u1ILi");
//ynJT8gRVfS7uN8RUiAA8IvzD73flrhaz
 const d = Dimensions.get("window")



 class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         
          loading_status:false,
      
      };

    }




_initialize =()=>{
    ZendeskChat.startChat({
        name: 'abc',
        email: 'abc@gmail.com',
        phone: '47564385',
        tags: ["tag1", "tag2"],
        department: "Your department",
        // The behaviorFlags are optional, and each default to 'true' if omitted
        behaviorFlags: {
            showAgentAvailability: true,
            showChatTranscriptPrompt: true,
            showPreChatForm: true,
            showOfflineForm: true,
        },
        // The preChatFormOptions are optional & each defaults to "optional" if omitted
        preChatFormOptions: {
            //name: !user.full_name ? "required" : "optional",
            name:  "required",
            email: "optional",
            phone: "optional",
            department: "required",
        },
        localizedDismissButtonTitle: "Dismiss",
    });
}

    componentWillMount(){
        this._initialize()
    }



    render() {

        return (
          <ScrollView style={{backgroundColor:'black',flex:1}}>
          {/** header */}
          <View style={styles.header}>
          <View/>

           <TouchableOpacity 
           onPress={()=>{this.props.navigation.navigate("Menu")} }>
           <FastImage 
           source={require('../assets/menu-gray.png')}
           style={{height:50,width:50}} 
           resizeMode={FastImage.resizeMode.cover}/>
              
            </TouchableOpacity>
          </View>

            {/** header end */}


           
         
         </ScrollView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Chat);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
  countriesContainer:{
      paddingVertical:10,
      paddingHorizontal:2,
      width:dimensions.SCREEN_WIDTH,
      backgroundColor:colors.LIGHT_BLACK,
      marginTop:10
  },
  listContainer:{
  width:'95%',
  borderRadius:20,
  borderWidth:1,
  borderColor:colors.COLOR_PRIMARY,
  height:80,
  marginVertical:5,
  alignSelf:'center',
  overflow:'hidden',justifyContent:'center'
},
countryImage:{
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
},
countryText:{
    color:'white',
    fontWeight:'bold',
    fontSize:17,
    marginLeft:20
}


}
)
