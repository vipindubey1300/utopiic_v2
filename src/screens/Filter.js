import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import { RadioButton } from 'react-native-paper';
import RangeSlider from 'rn-range-slider';
import { CheckBox } from 'react-native-elements'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ButtonComponent from '../components/ButtonComponent';


import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")



 class Filter extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            selectedFilter:'sort' ,//sort,location,price,
            alphabetChecked:false,
            priceChecked:false,
            countries:[],
            isCountryChecked : [],
            minPrice:0,
            maxPrice:10000

      };

    }

    _fetchCountries = async () =>{

             this.setState({loading_status:true})
             let url = urls.base_url +'general/counties'
                  fetch(url, {
                  method: 'POST',
                  }).then((response) => response.json())
                      .then((responseJson) => {
                        this.setState({loading_status:false})
                        if(responseJson.status){
                          var list = responseJson.data.list
                          let initialCheck = list.map(() => false);
                          // console.log('countries,',list)
                         this.setState({countries:list,isCountryChecked:initialCheck})
                        }  
                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})
                      });
  }
 

  componentDidMount (){
    this._fetchCountries()
  }

_cancel =()=>{
  this.props.navigation.pop()
}

_apply =()=>{
  var filterCountries = []
  var sortAlphabet = ''
  var sortPrice = ''
  
  

  //filter paramteres now
  if(this.state.alphabetChecked) sortAlphabet = 1

  if(this.state.priceChecked) sortPrice = 1

  this.state.isCountryChecked.map((element,index) => {
    if (element != false)  filterCountries.push(this.state.countries[index].id)
  })

  var minPrice = this.state.minPrice
  var maxPrice = this.state.maxPrice
  var price = minPrice + '-' + maxPrice

  console.log('filterCountries--',filterCountries)
  console.log('sortAlphabet--',sortAlphabet)
  console.log('sortPrice--',sortPrice)
  console.log('price--',price)
  this.props.navigation.state.params.onApply({filterCountries,sortAlphabet,sortPrice,price})
  this.props.navigation.pop()
}

handleChange = (index) => {
  let checked = [...this.state.isCountryChecked];
  checked[index] = !checked[index];
  this.setState({isCountryChecked: checked });
}



_renderCountries = ({item, index}) =>{
  return (
     <View style={{margin:5,flexDirection:'row',alignItems:'center'}}>
       <CheckBox
          checkedColor={colors.COLOR_PRIMARY}
          onPress={() => this.handleChange(index)}
          checked={this.state.isCountryChecked[index]} />

      <Text style={{color:'white'}}>{item.country}</Text>
     </View>
  )
}

getView =()=>{
  const selected =  this.state.selectedFilter
  if(selected =='sort'){
    return(
      <View style={{width:'100%'}}>
      <View style={styles.sortContainer}>
          <RadioButton.Android
          value="alphabet"
          color={colors.COLOR_PRIMARY}
          status={ this.state.alphabetChecked  ? 'checked' : 'unchecked' }
          onPress={() => this.setState({alphabetChecked:!this.state.alphabetChecked})}
          />
          <Text style={{color:'white'}}>Alphabetical Order</Text>

      </View>
      <View style={styles.sortContainer}>
          <RadioButton.Android
          value="price"
          color={colors.COLOR_PRIMARY}
          status={ this.state.priceChecked === 'price' ? 'checked' : 'unchecked' }
          onPress={() => this.setState({priceChecked:'price'})}
          />
          <Text style={{color:'white'}}>Price</Text>

      </View>

      </View>

      
    )
  }


  else if(selected =='location'){
    return(
      <View  style={{height:dimensions.SCREEN_HEIGHT * 0.5}}>
        <FlatList
                   
                    horizontal={false}
                    data={this.state.countries}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderCountries(item,index)}
                    keyExtractor={item => item.id}
                />
      </View>
    )
  }


  else if(selected =='price'){
    return(
      <View style={{alignSelf:'center'}}>
        <RangeSlider
          style={{width: dimensions.SCREEN_WIDTH * 0.5,
             height: 100,
           //  transform: [{ rotate: '90deg' }],
             marginTop:10}}
          gravity={'center'}
          min={0}
          max={10000}
          step={20}
          selectionColor="#BB9A5C"
          blankColor="#eee"
          onValueChanged={(low, high, fromUser) => {
              this.setState({minPrice: low, maxPrice: high})
          }}/>

      </View>
    )
  }


}



   
    render() {

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <View style={{backgroundColor:'black',flex:1}}>


          {/** header */}
          <View style={styles.header}>
              <View style={{flexDirection:'row',alignItems:'center'}}>

              <TouchableOpacity
                onPress={()=> {this.props.navigation.goBack()} }>
                <FastImage 
                source={require('../assets/round_back_black.png')}
                style={{height:40,width:40}} 
                resizeMode={FastImage.resizeMode.contain}/>
            </TouchableOpacity>

             <Text style={{fontSize:18,fontWeight:'bold'}}>     Filters</Text>



              </View>
                <View style={{height:40}}/>
              <Text style={{marginHorizontal:20}}>Here you can apply sorting and filters options to make it easier for you to find what you are looking for.</Text>

          
          </View>

            {/** header end */}


            

             {/** filterContainer  */}

             <View style={styles.filterContainer}>

             <View style={styles.leftContainer}>

                 <TouchableOpacity onPress={()=> this.setState({selectedFilter:'sort'})}
                 style={{height:60,width:'100%',justifyContent:'center',alignItems:'center',backgroundColor: this.state.selectedFilter == 'sort' ? colors.COLOR_PRIMARY :colors.LIGHT_BLACK}}>
                     <Text style={{color:this.state.selectedFilter == 'sort' ? 'black' :'white'}}> Sort</Text>
                 </TouchableOpacity>

                 <TouchableOpacity onPress={()=> this.setState({selectedFilter:'location'})}
                 style={{height:60,width:'100%',justifyContent:'center',alignItems:'center',backgroundColor: this.state.selectedFilter == 'location' ? colors.COLOR_PRIMARY :colors.LIGHT_BLACK}}>
                     <Text style={{color:this.state.selectedFilter == 'location' ? 'black' :'white'}}> Location</Text>
                 </TouchableOpacity>

                 <TouchableOpacity onPress={()=> this.setState({selectedFilter:'price'})}
                 style={{height:60,width:'100%',justifyContent:'center',alignItems:'center',backgroundColor: this.state.selectedFilter == 'price' ? colors.COLOR_PRIMARY :colors.LIGHT_BLACK}}>
                     <Text style={{color:this.state.selectedFilter == 'price' ? 'black' :'white'}}> Price</Text>
                 </TouchableOpacity>


             </View>

             <View style={styles.rightContainer}>
                 <View style={{paddingVertical:20,paddingHorizontal:10,alignItems:'center'}}>
                   {this.getView()}
                 </View>

                  {/** bottom section */}
             <View style={styles.bottomContainer}>
              <TouchableOpacity  onPress={()=> this._cancel()} style={[styles.buttonContainer,{backgroundColor:colors.LIGHT_BLACK}]}>
                <Text style={{color:colors.WHITE}}>Cancel</Text>
              </TouchableOpacity>


              <TouchableOpacity onPress={()=> this._apply()}
              style={[styles.buttonContainer,{backgroundColor:colors.COLOR_PRIMARY}]}>
              <Text style={{color:colors.BLACK}}>Apply</Text>
              </TouchableOpacity>
             
             </View>


              {/** bottom section end */}
                 
             </View>
           
             
             </View>
             {/** filterContainer end */}

              </View>
              <SafeAreaView/>
         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
    remove : (userinfo) => dispatch(removeUser(userinfo)),   
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Filter);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.COLOR_PRIMARY,
       borderBottomRightRadius:20,
      flex:3
      
  },
  requestContainer:{
      paddingVertical:25,
      paddingHorizontal:10,
      borderBottomRightRadius:16,
      width:dimensions.SCREEN_WIDTH,
      backgroundColor:colors.BLACK,
      marginTop:0,
      marginBottom:-15,
      elevation:2
      
  },
  filterContainer:{
    paddingTop:0,
    paddingHorizontal:0,
    paddingBottom:0,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.BLACK,
    marginTop:-29,
   flex:8,
    flexDirection:'row'
},
leftContainer:{
    flex:4,
    backgroundColor:colors.LIGHT_BLACK,
    borderTopColor:'grey',
    borderTopWidth:1
},
rightContainer:{
    flex:8,
    justifyContent:'space-between'
},
  rowContainer:{
      flexDirection:'row',
     // justifyContent:'space-between',
      alignItems:'center',
     
  },
  container:{
    borderColor:colors.COLOR_PRIMARY,
    borderRadius:6,
    borderWidth:0.7,
    marginRight:10,
    padding:10
  },
  bottomContainer:{
    padding:10,
    flexDirection:'row',
    alignItems:'center',
   
  },
  buttonContainer:{
      borderRadius:15,
      padding:10,
      justifyContent:'center',
      alignItems: 'center',
      flex:1,
      marginHorizontal:10
  },
  sortContainer:{
    width:'100%',
    margin:5,
    paddingVertical:4,
    paddingHorizontal:6,
    backgroundColor:colors.LIGHT_BLACK,
    borderRadius:10,
    flexDirection:'row',
    alignItems:'center'
  }

}
)
