import firebase from 'react-native-firebase';
import { ToastAndroid } from 'react-native';
// const axios = require('axios');
// import Config from '../Constants/Config';





export const getFCMToken = async () => {
    try {
      //ToastAndroid.show("ASd",ToastAndroid.SHORT)
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
          console.log("permission enabled")
          return await retrieveToken();
        } else {
          console.log("permission not enabled")
          return await requestPermission();
        }
    } catch (error) {
        // User has rejected permissions
        console.log('checkPermission error occured.' + error);
    }
    //return null;
};

const retrieveToken = async () => {
    var fcmToken = null;
   
    try {
      fcmToken = await firebase.messaging().getToken();
      console.log("fcmToken = " + fcmToken)
      return fcmToken
      
    } catch (error) {
      // User has rejected permissions
      console.log('getToken error occured.' + error);
      return fcmToken;
    }
   
}


const requestPermission = async () => {
    try {
      console.log("requesting permission")
      await firebase.messaging().requestPermission();
      // User has authorised
      return await retrieveToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
}