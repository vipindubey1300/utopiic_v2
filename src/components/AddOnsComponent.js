import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'
import { Item } from 'native-base';

export default class AddOnsComponent extends React.Component {
    constructor(props) {
        super(props);
        this.delayValue = 500;
        this.state = {
          animatedValue: new Animated.Value(0),
          data:[]
        }
      }

      componentDidMount = () => {
        Animated.spring(this.state.animatedValue, {
          toValue: 1,
          tension: 20,
          useNativeDriver: true
        }).start();
      }


      guestView  = (count) =>{
      
        var a =   Array(parseInt(count)).fill().map(Math.random)
        
           return a.map((object) => {
             return (
               <FastImage 
               source={require('../assets/round_back_black.png')}
               style={{height:20,width:20,margin:3,flexWrap:'nowrap'}} 
               resizeMode={FastImage.resizeMode.contain}/>
             );
         });
   
        
     }
   
     _facilities = (facilities) =>{
           
          return facilities.map((object) => {
            return (
             <View style={[styles.rowContainer,{marginHorizontal:3}]}>
               <FastImage 
               source={{uri:object.image}}
               style={{height:15,width:15,margin:3,flexWrap:'nowrap'}} 
               resizeMode={FastImage.resizeMode.contain}/>
               <Text style={[styles.whiteText,{fontSize:10}]}>{object.name}</Text>
             </View>
              
            );
        });
   
       
    }
   
   
   
	

  render() {
      const {object,onSelectRoom,selected,index} = this.props


      this.delayValue = this.delayValue + 500;
      const translateX = this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [this.delayValue, 1]
      });

      const isEven = (this.props.index % 2 == 0)

    //   const isSelectedRoom =
    //   this.state.selectedRoom == null 
    //   ? false
    //    : (this.state.selectedRoom.id === object.id)
 
       return (
        
        <Animated.View style={{transform: [{ translateX }],backgroundColor:isEven ? colors.BLACK : colors.LIGHT_BLACK,}}>
         <View style={{
            backgroundColor:isEven ? colors.LIGHT_BLACK : colors.BLACK,
            borderBottomRightRadius:20, 
            paddingVertical:10,
            paddingHorizontal:5
         }}>
 
         <View  style={{flexDirection:'row'}}>
         <FastImage 
               source={{uri:object.image}}
               style={{height:90,width:dimensions.SCREEN_WIDTH * 0.1,margin:3,borderRadius:15,flex:3}} 
               resizeMode={FastImage.resizeMode.cover}/>

           <View style={{overflow:'hidden',margin:7,flex:8}}>
           <Text style={[styles.yellowText]}>{object.name}</Text>
           <Text style={styles.whiteText}>{object.description}</Text>
           </View>
 
         
         </View>


         <View  style={[styles.rowContainer,{justifyContent:'space-between',marginHorizontal:6,
        marginVertical:5}]}>
             <View  style={[styles.rowContainer]}>
             <Text style={[styles.whiteText,{fontWeight:'500'}]}>Price : </Text>
             <Text style={{color:'grey',fontSize:13}}>{object.currency} {object.price} </Text>
             </View>


             {
                 selected 
                 ?
                 <TouchableOpacity onPress={() => this.props.onSelectAddOns(object)}>
                 <FastImage 
                 source={require('../assets/checkcircle-filled-yellowpng.png')}
                 style={{height:40,width:40}} 
                 resizeMode={FastImage.resizeMode.contain}/>
                 </TouchableOpacity>
                 :
                 <TouchableOpacity  onPress={() => this.props.onSelectAddOns(object)}>
                 <FastImage 
                 source={require('../assets/plus-empty-yellow.png')}
                 style={{height:40,width:40}} 
                 resizeMode={FastImage.resizeMode.contain}/>
                 </TouchableOpacity>
             }
         </View>


 
 
         
 
 
         
         </View>
         </Animated.View>
      
       )

  }
}



const styles = StyleSheet.create({
    roomContainer:{
        width:'95%',
        paddingHorizontal:5,
        paddingVertical:20,
        backgroundColor:colors.BLACK,
        borderRadius:20,
        marginVertical:5,
        alignSelf:'center',
        borderWidth:1,
        borderColor:'black'
       
      },
      yellowText:{
        color:colors.COLOR_PRIMARY
      },
      whiteText:{
        color:colors.WHITE
      },
      rowContainer:{
        flexDirection:'row',
       // justifyContent:'space-between',
        alignItems:'center',
       
    },
});