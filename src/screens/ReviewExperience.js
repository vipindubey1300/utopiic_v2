import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ButtonComponent from '../components/ButtonComponent';
import {showMessage,showToastMessage} from '../utils/showMessage';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import DateTimePicker from 'react-native-modal-datetime-picker';

import SwiperFlatList from 'react-native-swiper-flatlist';
import { Rating, AirbnbRating } from 'react-native-elements';
import { Checkbox } from 'react-native-paper';

const axios = require('axios');

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import AddOnsComponent from '../components/AddOnsComponent';
import Message from '../components/Message';

 const d = Dimensions.get("window")



 class ReviewExperience extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
         experience:'',
         hotels:[],
         isBooked:false,
         checked:false

      };

    }

  
   days_between(date1, date2) {

      // The number of milliseconds in one day
      const ONE_DAY = 1000 * 60 * 60 * 24;
  
      // Calculate the difference in milliseconds
      const differenceMs = Math.abs( new Date(date1) -  new Date(date2));
  
      // Convert back to days and return
      return Math.round(differenceMs / ONE_DAY);
  
  }

  componentWillMount(){
   // console.log(JSON.stringify(this.props.user))
   var result = this.props.navigation.getParam('result')
   var experience = result['experience']
   var hotels = result['hotels']



   console.log('experience_id-----',result['experience'])
   console.log('hotels-----',result['hotels'])
   this.setState({
    experience,hotels
   })
  }




  convertDate(date) {
    var yyyy = date.getFullYear().toString();
    var mm = (date.getMonth()+1).toString();
    var dd  = date.getDate().toString();

    var mmChars = mm.split('');
    var ddChars = dd.split('');

    return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
  }

  _terms =()=>{
    this.props.navigation.navigate("Terms")
  }

  _onDone =()=>{

        var result = this.props.navigation.getParam('result')
       var experience = result['experience']  
       
      var formData = new FormData();
      formData.append('package_id',experience.id);
      formData.append('currency',experience.currency);
      formData.append('basic_price',experience.basic_price);
      formData.append('convenience_fees',experience.convenience_fee);
      formData.append('discount',experience.discount);
      formData.append('tax',experience.tax);
      formData.append('date',this.convertDate(new Date()));

      var details= {
        'package_id':experience.id,
        'currency':experience.currency,
        'basic_price':experience.basic_price,
        'convenience_fees':experience.convenience_fee,
        'discount':experience.discount,
        'tax':experience.tax,
        'date':this.convertDate(new Date())

      }

    

      var formBody =[]
      for(var property in details){
          var encodeKey = encodeURIComponent(property)
          var encodedValue = encodeURIComponent(details[property])
          formBody.push(encodeKey + "=" + encodedValue)
      }
      formBody = formBody.join("&")
     
      console.log('formData----',formData)
      this.setState({loading_status:true})
               let url = urls.base_url +'booking/book/?package_id='+  experience.id +
                '&basic_price='+  parseInt(experience.basic_price) +
                '&tax='+  parseInt(experience.tax) +
                '&convenience_fees='+  parseInt(experience.convenience_fee) +
                '&discount='+  parseInt(experience.discount) +
                '&currency='+  experience.currency +
                '&date='+this.convertDate(new Date()).toString()

            //    axios.post(url, details,{headers: {
            //     //'Content-Type':'application/x-www-form-urlencoded;UTF-8',
            //      // 'Accept':'application/x-www-form-urlencoded',
            //      'token': this.props.user.token,
            //      }})
            //   .then(function (response) {
            //     console.log(response);
            //   })
            //   .catch(function (error) {
            //     console.log(error);
            //   });


               console.log('url----',url)
          
                    fetch(url, {
                        method: 'GET', 
                      //  body:formData,
                        headers: {
                       //'Content-Type':'application/x-www-form-urlencoded;UTF-8',
                        // 'Accept':'application/x-www-form-urlencoded',
                        'token': this.props.user.token,
                        }
                    }).then((response) => response.json())
                        .then((responseJson) => {

                          console.log("))))))))))",responseJson)
                        

                          if(responseJson.status){
                          //  this.setState({isBooked:true})
                            // setTimeout(() => {
                            //   this.setState({isBooked: false})
                            //   this.props.navigation.navigate("Home")
                            // }, 4000)

                            showMessage("Booked Successfully !",false)
                            this.props.navigation.navigate("Home")
                            
                          }
                          else{
                            ToastAndroid.show(responseJson.message,ToastAndroid.SHORT)
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
                          ToastAndroid.show(error.message,ToastAndroid.SHORT)
                         console.log(error)
                        });
    
    
    
      }

      _onFinal =()=>{
        if(this.state.checked) this._onDone()
        else showMessage('Accept Terms and Condition to continue')
      }
      _onSkip =()=>{
        var result =   this.props.navigation.getParam('result')

        let obj ={
          'experience':result.experience,
          'from_date':result.from_date,
          'to_date':result.to_date,
          'room':result.room,
          'add_on':[]
        }
        this.props.navigation.navigate("Review",{result:obj})
      }


      _hotelsView = (hotel) =>{
           
        return hotel.map((object) => {
          return (
           <View style={[styles.rowContainer,{marginHorizontal:3,justifyContent:'space-between'}]}>
           <Text style={[styles.yellowText ,{fontSize:12,textTransform:'capitalize'}]}>{object.name}</Text>

             <FastImage 
             source={{uri:object.image_path + object.image}}
             style={{height:55,width:55,margin:3,borderRadius:12,overflow:'hidden',backgroundColor:'grey'}} 
             resizeMode={FastImage.resizeMode.contain}/>
           </View>
            
          );
      });
 
     
  }
   
    render() {
      const {experience,hotels} = this.state

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <ScrollView style={{backgroundColor:colors.LIGHT_BLACK}}>

    


          {/** header */}
          <View style={styles.header}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
          <TouchableOpacity
          onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/round_back_black.png')}
          style={{height:40,width:40}} 
          resizeMode={FastImage.resizeMode.contain}/>
           </TouchableOpacity>
           <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,marginHorizontal:10}}>Review</Text> 

           </View>

          <View style={{margin:12,marginTop:40}}>
            <Text style={{fontWeight:'100',fontSize:14,marginVertical:5,color:'white',width:'80%'}}>The details of your request and the complete costs associated with it are listed below.Please take a look an confirm your request below.</Text> 
               
          </View>
          </View>

            {/** header end */}

            <View style={{height:40}}/>



              {/** experience */}
            <View style={styles.blackContainer}>
               <View style={[styles.rowContainer,{justifyContent:'space-between',alignItems:'flex-start'}]}>
               <View style={{width:'60%'}}>
               <Text style={[styles.whiteText,{marginVertical:3,textTransform:'capitalize'}]}>experience</Text>
               <Text style={[styles.yellowText,{marginVertical:3,textTransform:'capitalize'}]}>{experience.name}</Text>
               <Text style={[styles.yellowText,{fontSize:10,marginVertical:3}]}>{experience.description}</Text>
               <View style={{flexDirection:'row',alignItems:'center'}}>
                <Rating
                type='custom'
                readonly={true}
                ratingColor={colors.COLOR_PRIMARY}
                ratingBackgroundColor='rgba(0,0,0,0.00000000001)'
                ratingCount={experience.rating}
                imageSize={14}
                startingValue={4}
                tintColor= 'rgba(0,0,0,0.9)'
                style={{ paddingVertical: 0 ,backgroundColor:'rgba(0,0,0,0.0000001)'}}
              />
              <Text style={{color:'grey'}}>({experience.rating_count})</Text>
               </View>
                    
               </View>

               <FastImage 
               source={{uri: experience.cover}}
               style={{height:90,width:90,margin:3,borderRadius:15}} 
               resizeMode={FastImage.resizeMode.cover}/>
               </View>



             


              <View style={{marginVertical:13}}>
              <Text style={[styles.whiteText,{marginVertical:3}]}>Price</Text>

              <View style={[styles.rowContainer,{marginVertical:3,justifyContent:'space-between'}]}>
              <Text style={[styles.yellowText,{marginVertical:3,fontSize:15}]}>Base Price </Text>
              <Text style={[styles.yellowText,{marginVertical:3,fontSize:15}]}>{experience.currency} {experience.basic_price} </Text>
              </View>

               </View>

               <View style={{
                 width:'100%',
                 borderRadius:20,
                 borderWidth:0.5,
                 borderColor:colors.COLOR_PRIMARY,
                 justifyContent:'center',
                 alignItems:'center',
                 padding:15
               }}>
               <Text style={[styles.yellowText,{fontSize:15}]}>{experience.currency} {experience.basic_price}</Text>

               </View>

            </View>
             {/** experience end */}



            


              {/** add on */}
            <View style={styles.blackContainer}>
            <Text style={[styles.whiteText,{marginVertical:3}]}>Selected Hotels</Text>

            <View style={{marginVertical:10}}>
            {this._hotelsView(hotels)}
             </View>
           </View>
            {/** add on end */}


            <Text style={[styles.yellowText,{marginVertical:3,fontSize:17,alignSelf:'center'}]}>TOTAL</Text>

            <View style={{
              width:'95%',
              borderRadius:20,
              borderWidth:0.5,
              borderColor:colors.COLOR_PRIMARY,
              justifyContent:'center',
              alignItems:'center',
              padding:15,
              backgroundColor:'black',alignSelf:'center'
            }}>
            <Text style={[styles.yellowText,{fontSize:15}]}>{experience.basic_price} {experience.currency}</Text>

            </View>



            <View style={[styles.rowContainer,{marginTop:27,alignSelf:'center'}]}>
               

              <TouchableOpacity onPress={()=> this._terms()}
                style={[styles.buttonContainer,styles.rowContainer]}>
                   <Checkbox.Android
                    color={colors.COLOR_PRIMARY}
                    uncheckedColor={'white'}
                    onPress={() => this.setState({ checked: !this.state.checked })}
                    status={this.state.checked ? 'checked' : 'unchecked'}
                  />

                <Text style={{color:colors.COLOR_PRIMARY,textAlign:'center'}}>   Terms and Conditions</Text>
                </TouchableOpacity>
           
            </View>

           




                  <View style={{height:60}}/>
           

              </ScrollView>

              <View style={styles.bottomContainer}>
              <ButtonComponent 
              style={{marginTop:-15,width:dimensions.SCREEN_WIDTH * 0.45,alignSelf:'center'}}
              handler={this._onFinal}
              label ={'Confirm Request'}/>
          </View>

          {this.state.isBooked && <Message 
            success={true} message={'Requested Successfully'}/>}
         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(ReviewExperience);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.BLACK,
       borderBottomRightRadius:20,
       height:dimensions.SCREEN_HEIGHT * 0.3
  },
 
  bottomContainer:{
    position:'absolute',
    bottom:0,left:0,right:0,
    height:45,
     backgroundColor:'rgba(30,30,30,0.7)',
   // backgroundColor:'rgba(0,0,0,0.91)',
   borderTopColor:'black',
   borderTopWidth:0.7

  
  },
  blackContainer:{
    width:'95%',
    paddingHorizontal:5,
    paddingVertical:10,
    backgroundColor:colors.BLACK,
    borderRadius:20,
    marginVertical:5,
    alignSelf:'center',
  
   
  },
  yellowText:{
    color:colors.COLOR_PRIMARY
  },
  whiteText:{
    color:colors.WHITE
  },
  rowContainer:{
    flexDirection:'row',
   // justifyContent:'space-between',
    alignItems:'center',
   
},
buttonContainer:{
  borderRadius:20,
  backgroundColor:colors.BLACK,
  padding:10,
  justifyContent:'center',
  alignItems: 'center',
  borderWidth:0.5,borderColor:colors.COLOR_PRIMARY,
  marginBottom:10,
 // width:220,
  alignSelf:'center'
}

}
)
