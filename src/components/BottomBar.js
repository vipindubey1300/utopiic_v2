import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import FastImage from 'react-native-fast-image'

import { withNavigation } from 'react-navigation';

import {colors,urls,dimensions} from '../app_constants';

export default class BottomBar extends React.Component {
    constructor(props) {
        super(props);
        this. state = {
            selectedTab:'home'
        };
    }

    // componentDidUpdate( prevProps,prevState) {
    //   // if (nextProps.selectedTab !== this.props.selectedTab) {
    //   //   this.setState({ text: nextProps.defaultValue.toString() });
    //   // }
    //   console.log('@@@@@@@@@@',prevState)
    // }

    

    getSelectedTab = () => this.state.selectedTab;
    
    _onPressHome =()=>{
          this.setState({ selectedTab:'home'},()=>{
               this.props.handler('home')
          })
    }

    _onPressDestination =()=>{
      this.setState({ selectedTab:'destination'},()=>{
       this.props.handler('destination')
     })
      
    }

    _onPressSearch =()=>{
        this.setState({ selectedTab:'search'},()=>{
          this.props.handler('search')
       })
        
      }

      _onPressMenu =()=>{
        this.setState({ selectedTab:'menu'},()=>{
          this.props.handler('menu')
       })
        
      }
   
  
    render(){
        const {selectedTab } = this.state
        return (
            <View style={styles.container}>


            {
               selectedTab == 'home'
               ?
               <TouchableOpacity style={styles.imageContainer}>
               <FastImage 
               source={require('../assets/home_white.png')}
               style={styles.selectedimage} 
               resizeMode={FastImage.resizeMode.cover}/> 
                </TouchableOpacity>
                :
                <TouchableOpacity style={styles.imageContainer}
                onPress={()=> this._onPressHome() }>
                <FastImage 
                source={require('../assets/home_yellow.png')}
                style={styles.image} 
                resizeMode={FastImage.resizeMode.cover}/>
                </TouchableOpacity>

            }
            {
                selectedTab == 'destination'
                ?
                <TouchableOpacity style={styles.imageContainer}>
                <FastImage 
                source={require('../assets/destination_white.png')}
                style={styles.selectedimage} 
                resizeMode={FastImage.resizeMode.cover}/> 
                 </TouchableOpacity>
                 :
                 <TouchableOpacity style={styles.imageContainer}
                 onPress={()=> this._onPressDestination() }>
                 <FastImage 
                 source={require('../assets/destination_yellow.png')}
                 style={styles.image} 
                 resizeMode={FastImage.resizeMode.cover}/>
                 </TouchableOpacity>
 
             }

             {
                selectedTab == 'search'
                ?
                <TouchableOpacity style={styles.imageContainer}>
                <FastImage 
                source={require('../assets/search_white.png')}
                style={styles.selectedimage} 
                resizeMode={FastImage.resizeMode.contain}/> 
                 </TouchableOpacity>
                 :
                 <TouchableOpacity style={styles.imageContainer}
                 onPress={()=> this._onPressSearch() }>
                 <FastImage 
                 source={require('../assets/search_yellow.png')}
                 style={styles.image} 
                 resizeMode={FastImage.resizeMode.contain}/>
                 </TouchableOpacity>
 
             }

             {
                selectedTab == 'menu'
                ?
                <TouchableOpacity style={styles.imageContainer}>
                <FastImage 
                source={require('../assets/menu-white.png')}
                style={{height:30,width:30}} 
                resizeMode={FastImage.resizeMode.cover}/> 
                 </TouchableOpacity>
                 :
                 <TouchableOpacity style={styles.imageContainer}
                 onPress={()=> this._onPressMenu() }>
                 <FastImage 
                 source={require('../assets/menu-gold.png')}
                 style={{height:25,width:25}} 
                 resizeMode={FastImage.resizeMode.cover}/>
                 </TouchableOpacity>
 
             }


           
               
            </View>
           );
        }
};

BottomBar.defaultProps = {
    handler: () => {},
    style: {},
   
  };



const styles = StyleSheet.create({

  container:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',  
    backgroundColor:'rgba(30,30,30,0.7)',
    width:dimensions.SCREEN_WIDTH,
    height:60,
    paddingHorizontal:10
    // shadowColor: '#000000',
    // shadowOffset: {
    //   width: 0,
    //   height: 3
    // },
    // shadowRadius: 5,
    // shadowOpacity: 1.0,
    // elevation:1,

  },
  image:{
      height:22,
      width:22
  },
  selectedimage:{
    height:26,
    width:26
},
  imageContainer:{
      flex:1,
      alignItems:'center'
  }
  

})
