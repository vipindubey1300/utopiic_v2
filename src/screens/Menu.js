import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ButtonComponent from '../components/ButtonComponent';


import { connect } from 'react-redux';
import {removeUser } from '../actions/actions';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")



 class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
    

      };

    }

    _onLogout =async ()=>{
      try {
       
        this.props.remove({});
        await AsyncStorage.removeItem("user_id");
        await AsyncStorage.removeItem("name");
        await AsyncStorage.removeItem("image");
        await AsyncStorage.removeItem("email");
        await AsyncStorage.removeItem("phone");
        await AsyncStorage.removeItem("user_type");
        await AsyncStorage.removeItem("token");
        
       this.props.navigation.navigate("Passcode")
      
        return true;
      }
      catch(exception) {
        
        return false;
      }
    }


_terms =()=>{
  this.props.navigation.navigate("Terms")
}

_privacy =()=>{
  this.props.navigation.navigate("Privacy")
}

_payment =()=>{
  this.props.navigation.navigate("Payments")
}

_editProfile =()=>{
  this.props.navigation.navigate("EditProfile")
}

_notifications =()=>{
  this.props.navigation.navigate("Notifications")
}



   
    render() {

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <ScrollView style={{backgroundColor:'black'}}>



            {/** header */}
            <View style={styles.header}>
              
              <FastImage 
                  source={require('../assets/logo-info.png')}
                  style={{height:130,width:140,marginLeft:20}} 
                  resizeMode={FastImage.resizeMode.contain}/>
               <Text style={{color:'white',fontSize:14,fontWeight:'600',
               marginHorizontal:15,color:'grey'}}> Member ID : <Text style={{textDecorationLine:'underline',textTransform:'uppercase',fontSize:17,color:'white'}}>{this.props.user.passcode}</Text></Text>
          
            </View>
            {/** header end */}



              {/** menu */}
              <View style={{padding:10}}>
                <Text style={styles.labelText}>My accounts</Text>

                <TouchableOpacity onPress={()=> this._editProfile()}
                style={styles.rowContainer}>
                  <Text style={styles.label}>Personal information</Text>
                  <FastImage 
                  source={require('../assets/personal.png')}
                  style={styles.image}  resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>


                <TouchableOpacity onPress={()=> this._payment()}
                style={styles.rowContainer}>
                  <Text style={styles.label}>Payment History</Text>
                  <FastImage 
                  source={require('../assets/payment.png')}
                  style={styles.image}  resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=> {this._notifications()}}
                style={styles.rowContainer}>
                  <Text style={styles.label}>Notifications</Text>
                  <FastImage 
                  source={require('../assets/notification.png')}
                  style={styles.image}  resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=> {}}
                style={styles.rowContainer}>
                  <Text style={styles.label}>Add Card</Text>
                  <FastImage 
                  source={require('../assets/credit-card.png')}
                  style={styles.image}  resizeMode={FastImage.resizeMode.contain}/>
                </TouchableOpacity>



                <View style={{height:10}}/>

                <Text style={styles.labelText}>Others</Text>

          
                  <TouchableOpacity onPress={()=> this._terms()}
                   style={styles.rowContainer}>
                    <Text style={styles.label}>Terms and Conditions</Text>
                    <FastImage 
                    source={require('../assets/terms.png')}
                    style={styles.image}  resizeMode={FastImage.resizeMode.contain}/>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={()=> this._privacy()}
                   style={styles.rowContainer}>
                    <Text style={styles.label}>Privacy Policy</Text>
                    <FastImage 
                    source={require('../assets/privacy.png')}
                    style={styles.image}  resizeMode={FastImage.resizeMode.contain}/>
                  </TouchableOpacity>


                  <TouchableOpacity onPress={()=> this._onLogout()}
                  style={styles.rowContainer}>
                    <Text style={styles.label}>Logout</Text>
                    <FastImage 
                    source={require('../assets/logout.png')}
                    style={styles.image}  resizeMode={FastImage.resizeMode.contain}/>
                  </TouchableOpacity>


              </View>

              {/** menu end*/}

        


           

           
            
              
              </ScrollView>
         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
    remove : (userinfo) => dispatch(removeUser(userinfo)),   
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Menu);

let styles = StyleSheet.create({
  header:{
       backgroundColor:colors.LIGHT_BLACK,
       padding:20,
       justifyContent:'center',
       alignItems:'center',
       marginBottom:20
  },
  
  rowContainer:{
      flexDirection:'row',
     justifyContent:'space-between',
      alignItems:'center',
      padding:10,
      marginVertical:10,
      borderBottomColor:colors.GREY,
      borderBottomWidth:0.6
     
  },
  labelText:{
    color:'white',
    lineHeight:30,
    fontWeight:'500',
    fontSize:16
  },
  label:{
    color:colors.COLOR_PRIMARY,
    fontSize:16,
    fontWeight:'500'
  }
  ,image:{
    height:25,width:25
  }
}
)
