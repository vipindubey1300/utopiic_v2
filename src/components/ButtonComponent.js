import React from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  ImageBackground,
  Image,
  TouchableOpacity,
  Alert,
  ToastAndroid,
  Dimensions,FlatList,
  TextInput,PermissionsAndroid,
  RefreshControl,Fragment,Platform
} from 'react-native';
import { withNavigation } from 'react-navigation';
import {colors,urls,dimensions} from '../app_constants';


const d = Dimensions.get("window")



 const ButtonComponent = (props) => {

  function _onPress()  {
    props.handler()
  }
  
  return (
    <TouchableOpacity
    onPress={()=> _onPress() }>
            <View style={[styles.container,props.style]}>
                 <Text style={{fontWeight:'300',color:colors.BLACK,fontSize:19}}>{props.label}</Text>
            </View>
     </TouchableOpacity>
  )
};

export default ButtonComponent

const styles = StyleSheet.create({

  container:{
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY,
    paddingHorizontal:10,
    paddingVertical:7,
    borderRadius:30,
    marginTop:10,
    marginBottom:10
    
  }
})
