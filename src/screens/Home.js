import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,
  ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors } from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import {removeUser } from '../actions/actions';

import SwiperFlatList from 'react-native-swiper-flatlist';




import PreferencesComponent from '../components/PreferencesComponent';
import BottomBar from '../components/BottomBar';
import HomeData from './HomeData';
import Destinations from './Destinations';
import {showMessage,showToastMessage} from '../utils/showMessage';
import { connect } from 'react-redux';
import ProgressBar from '../components/ProgressBar';
import Swiper from '@starodubenko/react-native-deck-swiper'

import {  urls } from '../app_constants';
import Menu from './Menu';
import Requests from './Requests';
 const d = Dimensions.get("window")



class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          selectedTab:'home',
       

      };

    }

    _logout =async ()=>{
      try {
       
        this.props.remove({});
        await AsyncStorage.removeItem("user_id");
        await AsyncStorage.removeItem("name");
        await AsyncStorage.removeItem("image");
        await AsyncStorage.removeItem("email");
        await AsyncStorage.removeItem("phone");
        await AsyncStorage.removeItem("user_type");
        await AsyncStorage.removeItem("token");
        this.props.navigation.navigate('Passcode')
        return true;
      }
      catch(exception) {
        
        return false;
      }
    }


    _checkAuthToken =  () =>{
     
      this.setState({loading_status:true})
               let url = urls.base_url +'auth/checkToken'
                    fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      },
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log(responseJson)
                          this.setState({loading_status:false})
                          if(!responseJson.status){
                            //means token expired
                           
                            this._logout()
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
                          console.log(error)
                         
                        });
    
    
    
    }

    componentDidMount(){
        const selectedTab = this.bar.getSelectedTab()
         this.setState({selectedTab})
     }

     componentWillMount(){
        this._checkAuthToken()
     }

     _onTabSelect = (selectedTab) =>{
        // console.log('selectedTab------',selectedTab)//home//information
         this.setState({selectedTab})
     }

     _getView =()=>{
         if(this.state.selectedTab == 'home'){
             return(
                <HomeData  {...this.props}/>
             )
         }
         else if(this.state.selectedTab == 'destination'){
            return(
                <Destinations  {...this.props}/>
            )
        }
        else if(this.state.selectedTab == 'search'){
            return(
                <View>
                    <Text style={{color:'white',alignSelf:'center',marginTop:200}}>Coming Soon</Text>
                </View>
            )
        }
        else if(this.state.selectedTab == 'menu'){
          return(
            <Menu {...this.props}/>
          )
      }
        else{
            return(
                <HomeData/>
            )
        }
     }


    render() {

        return (
         <View style={{backgroundColor:'black',flex:1}}>
           <SafeAreaView/>
          <StatusBar
              backgroundColor="black"
              barStyle="light-content"
           />

             <View style={{flex:1}}>
             {this._getView()}
             
             </View>
        
             <BottomBar   handler={this._onTabSelect}
             ref={ref => this.bar = ref} {...this.props}/>
             
          { this.state.loading_status && <ProgressBar/> }
          <SafeAreaView style={{backgroundColor:'rgba(30,30,30,0.7)'}}/>
            </View>




        )
    }
}


const mapStateToProps = state => {
	return {
	
		user: state.user,
	
	};
};

const mapDispatchToProps = dispatch => {
  return {
   
    remove : (user_info) => dispatch(removeUser(user_info)),
   
    
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Home);

let styles = StyleSheet.create({
  container:{
        flex: 1,


  },
  imageBackground:{

    position: 'absolute',
    flex: 1,
    backgroundColor:'rgba(0,0,0,0.45)',
    width: d.width,

    zIndex:-2,
    height: d.height * 0.97,

  },
  heading:{
    fontSize:18,
    color:'white',
    fontWeight:'bold',
      textAlign:'center',
  },
  title:{
    fontSize:12,
    color:'white',
    textAlign:'center',
    marginBottom:20
  },
    input: {
    width: d.width * 0.64,
		marginHorizontal: 40,
		paddingTop: 0,
		marginTop: 0,
  },
  buttonContainer:{
    marginTop:10,
    padding:10,
    width:null,
    height:null,
    alignSelf:'center',
    flexDirection:'row',
    backgroundColor:'#BE863A',
    alignItems:'center'

}

}
)
