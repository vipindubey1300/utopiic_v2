import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ButtonComponent from '../components/ButtonComponent';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import DateTimePicker from 'react-native-modal-datetime-picker';

import SwiperFlatList from 'react-native-swiper-flatlist';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")



 class SelectDate extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
            fromDate:'',
            toDate:'',
            isDateTimePickerVisibleFrom:false,
            isDateTimePickerVisibleTo:false

      };

    }



    componentWillMount=()=>{
            this.setState({
                fromDate:Date().substring(4,16),
                toDate:Date().substring(4,16)
            })
    }

    convertDate(date) {
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth()+1).toString();
        var dd  = date.getDate().toString();
    
        var mmChars = mm.split('');
        var ddChars = dd.split('');
    
        return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
      }
    
    
      _showDateTimePickerFrom = () => this.setState({ isDateTimePickerVisibleFrom: true });
      _hideDateTimePickerFrom = () => {
        this.setState({ isDateTimePickerVisibleFrom: false });
      }
      handleDatePickedFrom = (date) => {
        console.log('A date has been picked: ',date);
        this.setState({fromDate:date.toString().substring(4,16)})
       // this.setState({date_obj:this.convertDate(date),dob:date.toString().substring(4,15)})
        this._hideDateTimePickerFrom();
      };

      _showDateTimePickerTo = () => this.setState({ isDateTimePickerVisibleTo: true });
      _hideDateTimePickerTo = () => {
        this.setState({ isDateTimePickerVisibleTo: false });
      }
    
      handleDatePickedTo = (date) => {
        console.log('A date has been picked------: ',this.convertDate(date));
        this.setState({toDate:date.toString().substring(4,16)})
       // this.setState({date_obj:this.convertDate(date),dob:date.toString().substring(4,15)})
        this._hideDateTimePickerTo();
      };


      _onDone =()=>{
        var hotel =   this.props.navigation.getParam('result').hotel

        let obj ={
          'hotel':hotel,
          'from_date':this.state.fromDate,
          'to_date':this.state.toDate
        }
        this.props.navigation.navigate("SelectRoom",{result:obj})
      }
   
    render() {

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <ScrollView style={{backgroundColor:colors.LIGHT_BLACK}}>

          <DateTimePicker
                mode="date"
             isVisible={this.state.isDateTimePickerVisibleFrom}
             onConfirm={this.handleDatePickedFrom}
             onCancel={this._hideDateTimePickerFrom}
             //maximumDate={new Date()}
              >
          </DateTimePicker>


          <DateTimePicker
          mode="date"
       isVisible={this.state.isDateTimePickerVisibleTo}
       onConfirm={this.handleDatePickedTo}
       onCancel={this._hideDateTimePickerTo}
       //maximumDate={new Date()}
        >
    </DateTimePicker>


          {/** header */}
          <View style={styles.header}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
          <TouchableOpacity
          onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/round_back_black.png')}
          style={{height:40,width:40}} 
          resizeMode={FastImage.resizeMode.contain}/>
           </TouchableOpacity>
           <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,marginHorizontal:10}}>Pick The Dates </Text> 

           </View>

          <View style={{margin:12,marginTop:40}}>
            <Text style={{fontWeight:'100',fontSize:14,marginVertical:5,color:'white',width:'70%'}}>Select a date when you wish to book the roms and the date up to which you wish to keep it booked.</Text>
           
           
          </View>
          </View>

            {/** header end */}


                 {/** date section */}
                 <View style={{marginVertical:40}}/>


                 <View style={{alignSelf:'center',width:'80%'}}>
                 <Text style={styles.dateHeading}>From</Text>
                 <TouchableOpacity onPress={this._showDateTimePickerFrom} style={styles.dateContainer}>
                    <Text style={{color:'white'}}>{this.state.fromDate}</Text>
                 </TouchableOpacity>

                <View style={{marginVertical:40}}/>

                 <Text style={styles.dateHeading}>To</Text>
                 <TouchableOpacity onPress={this._showDateTimePickerTo} style={styles.dateContainer}>
                 <Text style={{color:'white'}}>{this.state.toDate}</Text>
                 </TouchableOpacity>


                 </View>


              {/** date section end */}


           



    

             




            
              
              </ScrollView>

              <View style={styles.bottomContainer}>
              <ButtonComponent 
              style={{marginTop:-15,width:dimensions.SCREEN_WIDTH * 0.4,alignSelf:'center'}}
              handler={this._onDone}
              label ={'Next'}/>
          </View>


         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(SelectDate);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.BLACK,
       borderBottomRightRadius:20,
       height:190
  },
 
  
  rowContainer:{
      flexDirection:'row',
     // justifyContent:'space-between',
      alignItems:'center',
     
  },
  dateContainer:{
    borderColor:colors.COLOR_PRIMARY,
    borderRadius:16,
    borderWidth:0.3,
    marginRight:10,
    padding:10,
    backgroundColor:'black'
  },
  dateHeading:{
      color:colors.COLOR_PRIMARY,
      marginVertical:7
  },
  bottomContainer:{
    position:'absolute',
    bottom:0,left:0,right:0,
    height:45,
     backgroundColor:'rgba(30,30,30,0.7)',
   // backgroundColor:'rgba(0,0,0,0.91)',
   borderTopColor:'black',
   borderTopWidth:0.7

  
  }

}
)
