import { createAppContainer ,createSwitchNavigator} from 'react-navigation';
    import { createStackNavigator ,Header} from 'react-navigation-stack';
    import { createDrawerNavigator } from 'react-navigation-drawer';
     import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
  import React from 'react';
import { colors,urls} from './src/Constants';
import {StatusBar,TouchableOpacity,Image,Text,Platform,ToastAndroid} from 'react-native';


import Splash from './src/screens/Splash';
import Passcode from './src/screens/PassCode';
import Register from './src/screens/Register';
import Preferences from './src/screens/Preferences';
import Home from './src/screens/Home';
import Menu from './src/screens/Menu';
import Stays from './src/screens/Stays';
import HotelDetails from './src/screens/HotelDetails';
import CountryDetails from './src/screens/CountryDetails';
import CityDetails from './src/screens/CityDetails';
import Experiences from './src/screens/Experiences';
import SelectDate from './src/screens/SelectDate';
import SelectRoom from './src/screens/SelectRoom';
import HomeSupplier from './src/screens/HomeSupplier';
import SupplierDetails from './src/screens/SupplierDetails';
import ExperienceDetails from './src/screens/ExperienceDetails';
import SelectAddOns from './src/screens/SelectAddOns';
import Terms from './src/screens/Terms';
import Privacy from './src/screens/Privacy';
import Review from './src/screens/Review';
import AttractionDetails from './src/screens/AttractionDetails';
import Requests from './src/screens/Requests';
import ReviewExperience from './src/screens/ReviewExperience';
import EditProfile from './src/screens/EditProfile';
import ViewGallery from './src/screens/ViewGallery';
import ViewImage from './src/screens/ViewImage';
import Filter from './src/screens/Filter';
import Payments from './src/screens/Payments';
import ViewWeb from './src/screens/ViewWeb';
import Brands from './src/screens/Brands';
import BrandDetails from './src/screens/BrandDetails';
import Notifications from './src/screens/Notifications';

console.disableYellowBox = true;



//******************************** Menu Stack ********************************************** */

const menuStack = createStackNavigator({
 

  Menu :{ screen: Menu},
  Terms :{ screen: Terms},
  Privacy :{ screen: Privacy},
  Requests :{ screen: Requests},
  Payments :{ screen: Payments},
  EditProfile :{ screen: EditProfile},

 
 
}, {
     headerMode: 'none',
     initialRouteName: 'Menu',
     navigationOptions: {
      gesturesEnabled: true
    }
})


//******************************** Home Stack ********************************************** */

const homeStack = createStackNavigator({
 
  Home :{ screen: Home},
  Menu :{ screen: menuStack},
  Filter :{ screen: Filter},
  Stays :{ screen: Stays},
  Brands :{ screen: Brands},
  BrandDetails :{ screen: BrandDetails},
  HotelDetails :{ screen: HotelDetails},
  CountryDetails :{ screen: CountryDetails},
  CityDetails :{ screen: CityDetails},
  Experiences :{ screen: Experiences},
  ExperienceDetails :{ screen: ExperienceDetails},
  ReviewExperience :{ screen: ReviewExperience},
  AttractionDetails :{ screen: AttractionDetails},
  SelectDate :{ screen: SelectDate},
  SelectRoom :{ screen: SelectRoom},
  ViewGallery :{ screen: ViewGallery},
  ViewImage :{ screen: ViewImage},
  SelectAddOns :{ screen: SelectAddOns},
  Review :{ screen: Review},
  Terms :{ screen: Terms},
  Privacy :{ screen: Privacy},
  ViewWeb :{ screen: ViewWeb},
  Payments :{ screen: Payments},
  EditProfile :{ screen: EditProfile},
  Notifications :{ screen: Notifications},

}, {
     headerMode: 'none',
     initialRouteName: 'Home',
     navigationOptions: {
      gesturesEnabled: true
    }
})


//******************************** Home Supplier Stack ********************************************** */

const homeSupplierStack = createStackNavigator({
 
  HomeSupplier :{ screen: HomeSupplier},
  Menu :{ screen: menuStack},
  Stays :{ screen: Stays},
  HotelDetails :{ screen: HotelDetails},
  SupplierDetails :{ screen: SupplierDetails},
 
}, {
     headerMode: 'none',
     initialRouteName: 'HomeSupplier',
     navigationOptions: {
      gesturesEnabled: true
    }
})


//******************************** Passcode Stack ********************************************** */

const passcodeStack = createStackNavigator({
 
  Passcode :{ screen: Passcode},
  Register :{ screen: Register},
  Preferences :{ screen: Preferences},
  ViewWeb :{ screen: ViewWeb},

  
}, {
     headerMode: 'none',
     initialRouteName: 'Passcode',
     navigationOptions: {
      gesturesEnabled: true
    }
})

//******************************** App Stack ********************************************** */

 const AppStack = createSwitchNavigator({
      Splash : { screen: Splash},
      Passcode : { screen: passcodeStack},
      Home :{ screen: homeStack},
      HomeSupplier :{ screen: homeSupplierStack},
 }, {
         headerMode: 'none',
         initialRouteName: 'Splash',
         navigationOptions: {
          gesturesEnabled: false
        }

    })


  const RootNavigator = createAppContainer(AppStack)
 export default RootNavigator;
