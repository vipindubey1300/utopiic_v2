import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'
import BottomSheetGallery from './BottomSheetGallery';
import { Item } from 'native-base';

export default class RoomComponent extends React.Component {
    constructor(props) {
        super(props);
        this.delayValue = 500;
        this.state = {
          animatedValue: new Animated.Value(0),
          data:[],
          selectedGallery:null
        }
      }

      componentDidMount = () => {
        Animated.spring(this.state.animatedValue, {
          toValue: 1,
          tension: 20,
          useNativeDriver: true
        }).start();
      }


      guestView  = (count) =>{
      console.log(count)
      
        if(count > 0){
          var a =   Array(parseInt(count)).fill().map(Math.random)
          return a.map((object) => {
            return (
              <FastImage 
              source={require('../assets/guest.png')}
              style={{height:15,width:15,margin:3,flexWrap:'nowrap'}} 
              resizeMode={FastImage.resizeMode.contain}/>
            );
        });
        }
        
         
   
        
     }
   
     _facilities = (facilities) =>{
           
          return facilities.map((object) => {
            return (
             <View style={[styles.rowContainer,{marginHorizontal:3}]}>
               <FastImage 
               source={{uri:object.icon}}
               style={{height:15,width:15,margin:3,flexWrap:'nowrap'}} 
               resizeMode={FastImage.resizeMode.contain}/>
               <Text style={[styles.whiteText,{fontSize:10}]}>{object.name}</Text>
             </View>
              
            );
        });
   
       
    }

    _next =() =>{
      let obj = {'gallery':this.props.object.image}
      //this.props.navigation.navigate("ViewGallery",{result:obj})
      this.gallerySheet.open()
    }
   
   
   
	

  render() {
      const {object,onSelectRoom,selected} = this.props


      this.delayValue = this.delayValue + 500;
      const translateX = this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [this.delayValue, 1]
      });

       return (
          <Animated.View  style={{ transform: [{ translateX }]}}>
          <BottomSheetGallery gallery={object.image}
          inputRef={ref => this.gallerySheet = ref} //for close/open sheet
          ref={ref => this.bottomGallerySheet = ref}/>

         <View style={[styles.roomContainer,{
             borderColor:selected ? colors.COLOR_PRIMARY : colors.BLACK
         }]}>
 
         <View  style={[styles.rowContainer,{justifyContent:'space-between',alignItems:'flex-start'}]}>
           <View style={{width:'60%',overflow:'hidden',justifyContent:'space-evenly'}}>
           <Text style={[styles.yellowText,{width:'60%'}]}>{object.name}</Text>
             <View style={styles.rowContainer}>
              <Text style={styles.whiteText}>Guest For : </Text>
              {this.guestView(object.max_members)}
             </View>
         <Text style={[{width:'80%',color:'grey',fontSize:12,lineHeight:20,marginVertical:4}]}>{object.description.substring(0,100)}</Text>
           </View>
 
           <View style={{width:'40%',alignItems:'flex-end',marginRight:10}}>
 
              <TouchableOpacity onPress={()=> this._next()}
              style={{height:90,width:90,margin:3,borderRadius:15}} >
               <FastImage 
               source={{uri: object.image.length > 0  ? object.image[0].images :''}}
               style={{height:90,width:90,margin:3,borderRadius:15}} 
               resizeMode={FastImage.resizeMode.cover}/>
               </TouchableOpacity>

               <View style={{alignSelf:'flex-end',}}>
                 <Text style={[styles.yellowText,{lineHeight:20,}]}>Price for 1 Night</Text>
                 <Text style={[{color:'grey'},{lineHeight:20}]}>{object.currency} {object.price} </Text>
               </View>
         </View>
         </View>
 
 
          <View style={[styles.rowContainer,{flexWrap:'wrap',marginVertical:10}]}>
            {this._facilities(object.facilities)}
          </View>
 
 
          <TouchableOpacity onPress={()=>{
        //    this.setState({
        //      selectedRoom: object
        //    });
            this.props.onSelectRoom(object)
          }}
        
           style={{
            borderRadius:18,
            borderWidth:0.6,
            borderColor:colors.COLOR_PRIMARY,padding:12,
            width:'90%',
            justifyContent:'center',
            alignItems:'center',
            alignSelf:'center',
            marginVertical:12
          }}>
 
          {
            selected
            ? <View style={[styles.rowContainer,{width:'100%',justifyContent:'space-between'}]}>
             <Text style={styles.yellowText}>Room Selected</Text>
             <FastImage 
             source={require('../assets/round_clear_black.png')}
             style={{height:15,width:15,margin:3,flexWrap:'nowrap'}} 
             resizeMode={FastImage.resizeMode.contain}/>
            </View>
            :
            <Text style={[styles.yellowText,{fontSize: 17,color:'grey'}]}>SELECT</Text>
 
 
          }
 
          </TouchableOpacity>
 
         
         </View>
         </Animated.View>
       )

  }
}



const styles = StyleSheet.create({
    roomContainer:{
        width:'95%',
        paddingHorizontal:5,
        paddingVertical:10,
        backgroundColor:colors.BLACK,
        borderRadius:20,
        marginVertical:5,
        alignSelf:'center',
        borderWidth:1,
        borderColor:'black'
       
      },
      yellowText:{
        color:colors.COLOR_PRIMARY
      },
      whiteText:{
        color:colors.WHITE
      },
      rowContainer:{
        flexDirection:'row',
       // justifyContent:'space-between',
        alignItems:'center',
       
    },
});