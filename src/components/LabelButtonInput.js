import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform, TouchableOpacity} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'

export default class LabelButtonInput extends React.Component {
  state = {
    text: this.props.defaultValue ? this.props.defaultValue : '',
    isFocus: false,
    errorState:false,
    errorText:''
  };

	componentWillReceiveProps(nextProps) {
		// You don't have to do this check first, but it can help prevent an unneeded render
		if (nextProps.defaultValue !== this.props.defaultValue) {
			this.setState({ text: nextProps.defaultValue.toString() });
		}
	}

  getInputValue = () => this.state.text;

  render() {
    const { isFocus, text } = this.state;
   

    return (
  
      <View style={[styles.container, this.props.style]}>
      
      
        <TextInput
          style={[styles.inputText]}
          value={this.state.text}
          autoCapitalize={this.props.autoCapitalize}
          numberOfLines={1}
          maxLength ={this.props.maxLength}
          onFocus={() => this.setState({isFocus:true})}
          onBlur={() => this.setState({isFocus:false})}
          ref={this.props.inputRef}
          secureTextEntry={this.props.secureTextEntry}
          blurOnSubmit={this.props.blurOnSubmit}
          keyboardType={this.props.keyboardType}
          returnKeyType={this.props.returnKeyType}
          placeholder={this.props.placeholder}
          textContentType={this.props.textContentType}
          onSubmitEditing={this.props.onSubmitEditing}
          placeholderTextColor={colors.DARK_GREY}
          onChangeText={this.props.onChangeText ? this.props.onChangeText : (text) => this.setState({text})}
          editable={this.props.editable}
          multiline={false}
          numberOfLines={1}
          underlineColorAndroid={'rgba(0,0,0,0)'}
          blurOnSubmit={false}
        />

         <TouchableOpacity  onPress={()=> this.props.onFinish()}
            style={styles.buttonStyle}>
           <Text>{this.props.buttonLabel.toUpperCase()}</Text>
            </TouchableOpacity>
        
      </View>
     
    );

   


  }
}

LabelButtonInput.defaultProps = {
  focus: () => {},
  onFinish: () => {},
  style: {},
  placeholder: 'Enter',
  blurOnSubmit: false,
  returnKeyType: 'next',
  keyboardType: null,
  secureTextEntry: false,
  autoCapitalize: "none",
  textContentType: "none",
  defaultValue: '',
  editable: true,
  maxLength:40,
  buttonLabel:""
  
};

const styles = StyleSheet.create({
  container: {
   flexDirection:'row',
   borderColor:colors.COLOR_PRIMARY,
   borderWidth:0.7,
   borderRadius:30,
   backgroundColor:colors.BLACK,
   margin:0,
   overflow:'hidden',
   paddingLeft:2,
   paddingRight:0,
   marginHorizontal:0,
   marginVertical:10

  },

  inputText: {
    textAlign:'left',
    fontSize: 16,
    flex:7,
    color:colors.WHITE,
    marginHorizontal:3
  },
  buttonStyle:{
      flex:3,
      height:49,
      backgroundColor:colors.COLOR_PRIMARY,
      borderRadius:22,
      justifyContent:'center',
      alignItems:'center'
    },
    buttonImage:{
      height:30,
      width:30
    }
});