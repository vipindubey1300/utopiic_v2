import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView, Linking} from 'react-native';
import { colors ,urls,dimensions,fonts} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Rating, AirbnbRating } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import ChooseDateSheet from '../components/ChooseDateSheet';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';
import LinearGradient from 'react-native-linear-gradient';

 const d = Dimensions.get("window")



 class BrandDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
           brand_info: '',
           moreVisibility:false

      };

    }





    _fetchCoupons =  (brandId) =>{
       
        this.setState({loading_status:true})
              var formData = new FormData()
              //formData.append('supplier_id',brandId)
              formData.append('supplier_id',335)
              console.log(formData)
                 let url = urls.base_url +'hotels/coupon_list'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      },body:formData
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("_fetchCoupons*********",JSON.stringify(responseJson))
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                             
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }



      _fetchSupplierProduct =  (brandId) =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'hotels/supplier_productlist'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      },body:JSON.stringify({'supplier_id':brandId})
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("_fetchSupplierProduct*********",JSON.stringify(responseJson))
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                             
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }


    componentWillMount(){
      var result = this.props.navigation.getParam('result')
        console.log('_Brands detial-----',result['brand'].supplier_id)
       this.setState({brand_info:result['brand']})
       this._fetchCoupons(result['brand'].supplier_id)
       this._fetchSupplierProduct(result['brand'].supplier_id)
  }



  _renderCoupon = ({item, index}) =>{
  return (
     <TouchableOpacity
      style={[styles.rowContainer,{width:dimensions.SCREEN_WIDTH * 0.9,justifyContent:'flex-start',margin:5}]}>
        <View style={{height:15,width:15,borderRadius:8,backgroundColor:colors.COLOR_PRIMARY}}/>
  <Text style={{color:'white',marginHorizontal:10}}>{item.description}</Text>
     </TouchableOpacity>
  )
}

renderPage = ({item, index}) =>{
  return (
    <FastImage 
    source={{uri:urls.base_url_media + item.image}}
    style={{height:'100%',width:'100%',backgroundColor:'grey'}}
    resizeMode={FastImage.resizeMode.cover}/>
  );
}



_getAbout=()=>{
  if(this.state.moreVisibility){
    return(
      <Text  style={{color:colors.WHITE,fontSize:13}} 
      >{this.state.brand_info.description+ '  ' }
      <Text onPress={()=> this.setState({moreVisibility:!this.state.moreVisibility})}
       style={{color:colors.DARK_GREY,textDecorationLine:'underline',fontSize:13}}>done...</Text>
      </Text>
    )
  }
  else{
    return(
      <Text  style={{color:colors.WHITE,fontSize:13}} 
    >{this.state.brand_info.description.substring(0,200) + '  ' }
    <Text onPress={()=> this.setState({moreVisibility:!this.state.moreVisibility})}
    style={{color:colors.COLOR_PRIMARY,textDecorationLine:'underline',fontSize:13}}>more...</Text>
    </Text>
    )
  }
}

   
    render() {

      const {brand_info} = this.state

      return (
        <SafeAreaView style={{flex:1,backgroundColor: 'black' }}>
          <KeyboardAwareScrollView
            bounces={false} 
            showsVerticalScrollIndicator={false}
           contentContainerStyle={styles.scrollContainer}>
  
         <View style={{height:dimensions.SCREEN_HEIGHT * 0.4,width:dimensions.SCREEN_WIDTH}} >
             {
                       brand_info.brand_logo ?
                      <FastImage
                      style={{ width: '100%', height: '100%' }}
                      source={{uri:urls.base_url_media + brand_info.brand_logo}}
                      resizeMode={FastImage.resizeMode.cover}
                      />
                      : null
               }
                       {/** header */}
           
                  <View style={styles.header}>
                    <TouchableOpacity  onPress={()=> {this.props.navigation.goBack()} }>
                      <FastImage 
                      source={require('../assets/back.png')}
                      style={{height:40,width:30,marginLeft:4,marginRight:10}} 
                      resizeMode={FastImage.resizeMode.contain}/>
                    </TouchableOpacity>

                    <Text style={{color:colors.COLOR_PRIMARY,fontSize:20,fontWeight:'700',
                    textTransform:'uppercase'}}>{brand_info.name} </Text> 
  
                  </View>
              

            {/** header end */}
           </View>
           <LinearGradient
           style={{padding:10,marginTop:0}}
           colors={['rgba(0,0,0,0.9)', 'rgba(0,0,0,0.9)', 'rgba(0,0,0,0.9)']} >

          <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',padding:10}}>
                <View>
                  <Text style={{color:colors.COLOR_PRIMARY,fontWeight:'600',fontSize:18}}>{brand_info.name}</Text>
                  <Text style={{color:'white',marginVertical:7,fontSize:13}}>{brand_info.country} </Text>
                    <Text style={{color:'white',marginVertical:7,fontSize:13}}>{brand_info.industry_category} ,{brand_info.industry_name} </Text>
                </View>


                <FastImage 
              source={require('../assets/logo.png')}
              style={{height:30,width:30,marginLeft:15}} 
              resizeMode={FastImage.resizeMode.contain}/>

               {/* <View style={styles.ratingContainer}>
                  <Text style={{color:colors.BLACK}}>{brand_info ? parseFloat(brand_info.marks).toFixed(1) : 0.0}</Text>
              </View> */}
           </View>

           
           </LinearGradient>

          <View style={{padding:10}}>
           <View style={[styles.rowContainer,{marginVertical:15}]}>
              <Text style={styles.aboutText}>About</Text>
              <View/>
             </View>
            {
                brand_info.description && brand_info.description.length > 200 ?
                 this._getAbout()
                :
              <Text style={{color:colors.GREY,fontSize:13}}>{brand_info.description}</Text>
            }
            </View>

           </KeyboardAwareScrollView>
         </SafeAreaView>
       )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(BrandDetails);

let styles = StyleSheet.create({
  header:{
    flexDirection: 'row',
    justifyContent:'flex-start',
    alignItems:'center',
    backgroundColor:'rgba(255,255,255,0.001)',
    position:'absolute',
    top:0,
    left:0,right:0,padding:10
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.LIGHT_BLACK,
    marginTop:15,
   
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  ratingContainer:{
    padding:6,borderRadius:20,
    justifyContent:'center',alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY
  },
  descriptionContainer:{
    padding:6,borderRadius:10,
    backgroundColor:colors.BLACK,
    borderWidth:0.4,
    borderColor:colors.COLOR_PRIMARY,
    width:'95%',alignSelf:'center',
    marginVertical:10
  }, 
  listContainer:{
    width:'45%',
    borderRadius:30,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    height:50,
    marginVertical:5,
    alignSelf:'center',
    overflow:'hidden',flexDirection:'row',alignItems:'center',marginHorizontal:10
  },
  facilitiesImage:{
    height:20,width:20,marginHorizontal:5
  },
  aboutText:{
    fontSize:18,fontWeight:'500',
  marginBottom:0,fontFamily:fonts.heading_font,
  textTransform:'uppercase',color:colors.WHITE,alignSelf:'center'},

  rowContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
},
bottomContainer:{
  position:'absolute',
  bottom:0,left:0,right:0,
  height:45,
  // backgroundColor:'rgba(30,30,30,0.7)'
  backgroundColor:'rgba(0,0,0,0.91)'

},
scrollContainer:{
  paddingBottom:0,backgroundColor:'black',flexGrow:1
},

})
