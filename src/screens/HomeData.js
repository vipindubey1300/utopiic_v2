import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions, fonts} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import Carousel from 'react-native-banner-carousel';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';
import PackageComponent from '../components/PackageComponent';
import BrandComponent from '../components/BrandComponent';


 const d = Dimensions.get("window")



 class HomeData extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          packages:[],
          loading_status:false,
          selectedTab:'',
          baseUrl:'',
          baseUrlBanner:'',
          banner: [],
          brands:[]
         

      };

    }




    _fetchBanners = async () =>{
        console.log("*********",this.props.user.token)
      this.setState({loading_status:true})
               let url = urls.base_url +'general/banner'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                          console.log("BANEERRSSSS*********",JSON.stringify(responseJson))
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data.list
                            //console.log('lists,',list)
                            var baseUrl = responseJson.data.image_path
                           
                           this.setState({banner:list,baseUrlBanner:baseUrl})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }


    _fetchBrands = async () =>{
      console.log("*********",this.props.user.token)
    this.setState({loading_status:true})
             let url = urls.base_url +'hotels/getsuppliers'
                  fetch(url, {
                  method: 'POST',
                  headers: {
                     'token': this.props.user.token,
                  }
  
                  }).then((response) => response.json())
                      .then((responseJson) => {

                        console.log("BANEERRSSSS*********",JSON.stringify(responseJson))
                        this.setState({loading_status:false})

                        if(responseJson.status){
                          var list = responseJson.data.Suppliers
                         
                         
                         this.setState({brands:list.slice(0,16)})
                        }
                        else{
                          
                        }
                        
                       
                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})

                       
                      });
  
  
  
  }



    componentWillMount(){
      console.log(JSON.stringify(this.props.user))
     this._fetchBanners()
     this._fetchBrands()
    }




  
    onBannerClick = (item) =>{
     
      let obj ={'url':item.link}
      this.props.navigation.navigate("ViewWeb",{result:obj})
    
    }

    _renderBanner = ({item, index}) =>{
       return (
          <BannerComponent
           object = {item} 
           index ={index}
           baseUrl ={this.state.baseUrlBanner}
           clickHandler ={this.onBannerClick}/>
       )
     }

     renderPage(item, index) {
      return (
          <TouchableOpacity style={{alignSelf:'center'}}
           onPress={()=> this.onBannerClick(item)}
          key={index}>
              <Image style={styles.bannerImage} 
                source={{ uri: item.image }} />
          </TouchableOpacity>
      );
  }


  onBrandClick = (item) =>{
    console.log('onBrandClick--------',JSON.stringify(item))
    var id = item.id
    let obj ={'brand':item}
    this.props.navigation.navigate("BrandDetails",{result:obj})
  
  }


     _renderBrand = ({item, index}) =>{
        return (
           <BrandComponent
            object = {item} 
            index ={index}
            clickHandler ={this.onBrandClick}/>
        )
      }

      

      

    render() {

        return (
          <ScrollView bounces={false} style={{flex:1,backgroundColor:'black'}}>
          {/** header */}
          <View style={styles.header}>
          <TouchableOpacity style={styles.container}
          onPress={()=> {} }>
          <FastImage 
          source={require('../assets/utopiic-logo-text.png')}
          style={{height:40,width:100,marginLeft:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
             
           </TouchableOpacity>

         
          </View>

            {/** header end */}


            {/** heading */}
                <Text style={{color:'white',fontSize:12,margin:10,fontFamily:'gillsans'}}>Find and book your favourite travel destinations across the globe with Utopiic Travel Concierge.</Text>

             {/** heading end */}



               {/** exclusive */}
               <View style={styles.exclusiveContainer}>
      
                <Carousel
                style={{alignSelf:'center'}}
                    autoplay
                    autoplayTimeout={4000}
                    loop
                    index={0}
                    pageSize={dimensions.SCREEN_WIDTH}
                >
                    {this.state.banner.map((image, index) => this.renderPage(image, index))}
                </Carousel>

               </View>

              {/** exclusive end */}




               {/** brands */}
               <View style={styles.hotelContainer}>
                <View style={styles.rowContainer}>
                    <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,fontSize:16}}>  Brands</Text>
                    <TouchableOpacity style={styles.rowContainer}
                    onPress={()=>{this.props.navigation.navigate("Brands")} }>
                      <Text style={{color:colors.COLOR_PRIMARY}}>View all   </Text>
                    <FastImage 
                    source={require('../assets/arrow_forward_yellow.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.cover}/>
                       
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10,alignSelf:'center'}}
                    numColumns={2}
                    data={this.state.brands}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderBrand(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** brands end */}


              


              
         
         </ScrollView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(HomeData);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
  exclusiveContainer:{
      paddingVertical:0,
      paddingHorizontal:0,
      borderTopRightRadius:16,
      width:dimensions.SCREEN_WIDTH,
      backgroundColor:colors.BLACK,
      marginTop:10,
      paddingBottom:0,
      height:dimensions.SCREEN_HEIGHT * 0.26
  },
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.BLACK,
    marginTop:-15
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  bannerImage:{ width: dimensions.SCREEN_WIDTH * 0.99, 
    height: dimensions.SCREEN_HEIGHT * 0.22,
    alignSelf:'center',borderRadius:0,marginTop:0,backgroundColor:'#808080' }

}
)
