import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Rating, AirbnbRating } from 'react-native-elements';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import Carousel from 'react-native-snap-carousel';

import SwiperFlatList from 'react-native-swiper-flatlist';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")


const  carouselItems = [
  {id:1,name:'Description'},
  {id:2,name:'Map'}

]

 class AttractionDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
          details: '',
          images:[],
           addressRegion:{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
           },
          
          
         

      };

    }





    _fetchDetails =  (attractionId) =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'general/attraction_details/'+ attractionId
                      fetch(url, {
                      method: 'POST',
    
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              this.setState({
                                details:responseJson.data.details,
                                images:responseJson.data.images,
                               
                              addressRegion:{
                                latitude:parseFloat(responseJson.data.details.lat),
                                longitude:parseFloat(responseJson.data.details.lng),
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                              }})

                              this._carousel.snapToNext();
                              //this._carousel.snapToPrev();
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }



    componentWillMount(){
      var result = this.props.navigation.getParam('result')
      console.log('_fetchDetails-----',result['attraction_id'])
       this._fetchDetails(result['attraction_id'])
  }

 

  _renderCarousel = ({item, index}) => {
    if(item.id == 1){
      return(
        <View style={styles.descriptionContainer}>
        <Text style={{color:colors.WHITE,fontSize:19,marginBottom:15}}>Description</Text>

           <Text style={{color:colors.DARK_GREY}}>{this.state.details.description}</Text>
         </View>
         )
    }
    else if(item.id == 2){
      return( 
         <View  pointerEvents={'none'} style = {{height:200, width:dimensions.SCREEN_WIDTH* 0.8,marginBottom:0,
        overflow:'hidden',borderRadius:20,alignSelf:'center'
       }}>
           <MapView
           style = {{height: '100%', width: '100%'}}
             initialRegion={{
           latitude: 28.78825,
           longitude: 77.4324,
           latitudeDelta: 0.0922,
           longitudeDelta: 0.0421,
         }}
         region={this.state.addressRegion}
         showsUserLocation = {true}
         followUserLocation = {true}
         zoomEnabled = {true}
         />
      </View>)
    }
   
   
 
  
  }


   
  renderPage =({item, index}) =>{
    console.log('image--',item)
    return (
      <FastImage 
      source={{uri:item}}
      style={{height:'100%',width:'100%'}}
      resizeMode={FastImage.resizeMode.cover}/>
    );
  }
  

   
    render() {

      const {details,images} = this.state

        return (
      <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
        {/** header */}
        <View style={styles.header}>
        <TouchableOpacity style={{flexDirection:'row',alignItems:'center',padding:10}}
        onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/back.png')}
          style={{height:30,width:30,marginLeft:4,marginRight:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
          <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>{details.name} </Text> 
         </TouchableOpacity>

         <View/>
        </View>

          {/** header end */}
        <KeyboardAwareScrollView contentContainerStyle={{
          justifyContent:'center',
         paddingBottom:50
        }}>

        <View style={{height: dimensions.SCREEN_HEIGHT * 0.33,width:'100%',backgroundColor:'grey'}}>
            
            
        <Carousel
             ref={(c) => { this._carousel = c; }}
             data={images}
             renderItem={this.renderPage}
             autoplay={true}
             //loop
             sliderWidth={dimensions.SCREEN_WIDTH}
             itemWidth={dimensions.SCREEN_WIDTH }
           />
        </View>

        <View style={styles.middleContainer}>
        <TouchableOpacity style={{flex:1,justifyContent:'center',
       borderBottomColor:colors.COLOR_PRIMARY,
        borderBottomWidth:1,backgroundColor:'rgba(0,0,0,0.5)',paddingLeft:20,paddingRight:20}}>
        <Text style={{color:colors.COLOR_PRIMARY}}>{details.name}</Text>
        
        </TouchableOpacity>
        <TouchableOpacity style={{flex:1,justifyContent:'center',backgroundColor:colors.COLOR_PRIMARY,paddingLeft:20,paddingRight:20}}>
            <Text>{details.address}</Text>
        </TouchableOpacity>
        </View>

        <View style={{height:40}}/>

   <View style={{height:dimensions.SCREEN_HEIGHT * 0.3}}>
        <Carousel
        ref={(c) => { this._carousel = c; }}
       data={carouselItems}
        renderItem={this._renderCarousel}
        autoplay={false}
        //loop
        extraData={this.state}
        sliderWidth={dimensions.SCREEN_WIDTH}
        itemWidth={dimensions.SCREEN_WIDTH * 0.8}
      />
      </View>

      <View style={{width: '100%',height:0.6,
      backgroundColor:'grey',marginVertical:10}}/>

      <Text style={{color:'white',fontSize:17,marginHorizontal:10}}>Ticket Price</Text>
      <Text style={{color:colors.COLOR_PRIMARY,fontSize:13,marginHorizontal:10}}>{details.price}</Text>


        </KeyboardAwareScrollView>

   
         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(AttractionDetails);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.LIGHT_BLACK,
    marginTop:15,
   
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  ratingContainer:{
    padding:6,borderRadius:20,
    justifyContent:'center',alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY
  },
  descriptionContainer:{
    padding:10,borderRadius:10,
    backgroundColor:colors.LIGHT_BLACK,
    borderRadius:15,
    width:'95%',alignSelf:'center',
    marginVertical:10,
    height:200,alignSelf:'center',marginHorizontal:10
  }, 
  listContainer:{
    width:'45%',
    borderRadius:30,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    height:50,
    marginVertical:5,
    alignSelf:'center',
    overflow:'hidden',flexDirection:'row',alignItems:'center',marginHorizontal:10
  },
  facilitiesImage:{
    height:20,width:20,marginHorizontal:5
  },
  facilitiesText:{
      color:'white',
      fontWeight:'100',
      fontSize:13,
      marginLeft:10
  },
  rowContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
},
bottomContainer:{
  position:'absolute',
  bottom:0,left:0,right:0,
  height:45,
  // backgroundColor:'rgba(30,30,30,0.7)'
  backgroundColor:'rgba(0,0,0,0.91)'

},
middleContainer:{
  height:90,
  width:dimensions.SCREEN_WIDTH * 0.7,
  marginTop:-45,
  alignSelf:'center',
 overflow:'hidden',
  borderRadius:13,
  borderColor:colors.COLOR_PRIMARY,
  borderWidth:0.8
},

}
)
