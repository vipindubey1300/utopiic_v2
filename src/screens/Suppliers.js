import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")



 class Suppliers extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          suppliers:[],
          loading_status:false,

      };

    }




    _fetchSuppliers = async () =>{
        console.log("00000000000-----------",this.props.user.token)
         this.setState({loading_status:true})
               let url = urls.base_url +'suppliers/native/my_natives'
                    fetch(url, {
                    method: 'POST',
                    headers: { 'token': this.props.user.token, }
                    }).then((response) => response.json())
                        .then((responseJson) => {
                            console.log("*********",responseJson)

                          this.setState({loading_status:false})
                          if(responseJson.status){
                            var list = responseJson.data.list
                            console.log('suppliers,',list)

                           this.setState({suppliers:list})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          ToastAndroid.show(error.message,ToastAndroid.LONG)
                          this.setState({loading_status:false})
                        });
    
    
    
    }

    componentWillMount(){
   
     this._fetchSuppliers()
   
    }


    onSupplierClick = (item) =>{
      console.log('onSupplierClick--------',JSON.stringify(item))
      var id = item.id
      let obj ={'supplier_id':id}
      this.props.navigation.navigate("SupplierDetails",{result:obj})
    
    }



     _renderSupplier = ({item, index}) =>{
        return (
           <TouchableOpacity onPress={()=> this.onSupplierClick(item)}
           style={styles.listContainer}>
           <FastImage 
           source={{uri:item.image}}
           style={styles.supplierImage} 
           resizeMode={FastImage.resizeMode.cover}/>
           <View style={{flex:8,marginLeft:15}}>


            <Text style={styles.supplierText}>{item.first_name}</Text>
            <Text style={{color:'grey'}}>{item.email}</Text>
            </View>
           </TouchableOpacity>
        )
      }

    render() {

        return (
          <ScrollView style={{backgroundColor:'black',flex:1}}>
          {/** header */}
          <View style={styles.header}>
          <View/>

           <TouchableOpacity 
           onPress={()=>{} }>
           <FastImage 
           source={require('../assets/menu-gray.png')}
           style={{height:50,width:50}} 
           resizeMode={FastImage.resizeMode.cover}/>
              
            </TouchableOpacity>
          </View>

            {/** header end */}


            {/** heading */}
                <View style={{margin:10,flexDirection:'row',alignItems:'center',marginTop:-10}}>
                <FastImage 
                source={require('../assets/supplier-yellow.png')}
                style={{height:30,width:30}} 
                resizeMode={FastImage.resizeMode.cover}/>
                <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,margin:10}}>Natives</Text>

                </View>
                <Text style={{color:'white',fontSize:12,margin:10}}>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</Text>

             {/** heading end */}



               {/** exclusive */}
               <View style={styles.countriesContainer}>
              


                <FlatList
                    style={{marginVertical:10}}
                    horizontal={false}
                    data={this.state.suppliers}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderSupplier(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** exclusive end */}




              
              
         
         </ScrollView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Suppliers);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
  countriesContainer:{
      paddingVertical:10,
      paddingHorizontal:2,
      width:dimensions.SCREEN_WIDTH,
      backgroundColor:colors.LIGHT_BLACK,
      marginTop:10
  },
  listContainer:{
  width:'95%',
  borderRadius:30,
  borderWidth:1,
  borderColor:colors.COLOR_PRIMARY,
  height:60,
  marginVertical:5,
  alignSelf:'center',
  overflow:'hidden',
  flexDirection:'row',
  alignItems:'center'
},
supplierImage:{
   height:'100%',
   width:60,
   flex:2,
   borderRadius:30,
},
supplierText:{
    color:'white',
    fontWeight:'bold',
    fontSize:17,
    
}}
)
