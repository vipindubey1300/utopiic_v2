import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'
import { Rating, AirbnbRating } from 'react-native-elements';
import StarRating from 'react-native-star-rating';



export default class StayComponent extends React.Component {
    constructor(props) {
        super(props);
        this.delayValue = 500;
        this.state = {
          animatedValue: new Animated.Value(0),
          data:[]
        }
      }

      componentDidMount = () => {
        Animated.spring(this.state.animatedValue, {
          toValue: 1,
          tension: 20,
          useNativeDriver: true
        }).start();
      }
	

  render() {
      const {object} = this.props
      this.delayValue = this.delayValue + 500;
      const translateX = this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [this.delayValue, 1]
      });
    return (
        <Animated.View  style={{ transform: [{ translateX }]}}>
        <TouchableOpacity style={styles.container}  onPress={()=> this.props.clickHandler(this.props.object)}>
        <View
         style={styles.imageContainer} >
            <FastImage 
            source={{uri:object.image}}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.cover}/>
        </View>

        <View  style={[styles.bottomContainer,{justifyContent:'space-between',flexDirection:'row',alignItems:'center'}]} >
              <View>
              <Text style={{color:'white'}}>{object.name}</Text>
        <Text style={{color:object.city_name.length > 0 ? 'grey' :'red',fontSize:12,marginVertical:3}}>{object.city_name.length > 0 ?object.city_name : 'City not found.'}</Text>
        
              </View>

              <View >
              <Text style={{color:'grey'}}>Price : {object.price}  {object.currency}</Text>
              <View style={{flexDirection:'row',alignItems:'center',alignSelf:'flex-end'}}>
            <StarRating
            disabled={true}
            emptyStar={'ios-star-outline'}
            fullStar={'ios-star'}
            halfStar={'ios-star-half'}
            iconSet={'Ionicons'}
            maxStars={5}
            starSize={17}
            rating={object.star_rating}
            fullStarColor={colors.COLOR_PRIMARY}
          />
    
          <Text style={{color:'grey'}}>({object.star_rating_count == '' ? 0 : object.star_rating_count})</Text>
        </View>
        </View>
             
              </View>


  


       
        </TouchableOpacity>
        </Animated.View>
     
    );

  }
}



const styles = StyleSheet.create({
  container: {
   backgroundColor:colors.LIGHT_BLACK,
   margin:10,
   height:dimensions.SCREEN_HEIGHT * 0.36,
   width:dimensions.SCREEN_WIDTH * 0.93,
   marginVertical:15,
  
   alignSelf:'center'

  },
  imageContainer:{
    margin:0,
    width:'100%',
    flex:8,
    overflow:'hidden',
    borderRadius:10,
    borderWidth:0.6,
    borderColor:colors.BLACK,
  },
  bottomContainer:{
   
    height:80,
    backgroundColor:colors.LIGHT_BLACK,
    padding: 3,
    justifyContent:'center'
  },

 
  imageStyle:{
  
   height:'100%',width:'100%'
  },
  
  icon:{
      position: 'absolute',right: 10,
      top:0,height:25,width:25
  }
});