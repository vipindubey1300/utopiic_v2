import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Rating, AirbnbRating } from 'react-native-elements';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import HTML from 'react-native-render-html';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';
import WebView from 'react-native-webview';

 const d = Dimensions.get("window")



 class Privacy extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
           data:''
          
      };

    }





    _fetchPrivacy =  () =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'cms/privacy'
                      fetch(url, {
                      method: 'POST',
                     
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              this.setState({data:responseJson.data.description})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }


   


    componentWillMount(){
     
       this._fetchPrivacy()
 }



   
    render() {

      const {data} = this.state

        return (
      <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
        {/** header */}
        <View style={styles.header}>
        <TouchableOpacity style={{flexDirection:'row',alignItems:'center',padding:10}}
        onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/back.png')}
          style={{height:30,width:30,marginLeft:4,marginRight:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
          <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>Privacy Policy</Text> 
         </TouchableOpacity>

         <View/>
        </View>

          {/** header end */}
        <KeyboardAwareScrollView contentContainerStyle={{
          justifyContent:'center',
         paddingBottom:20,
         backgroundColor:'white',
         flexGrow:1
        }}>

      

        <WebView  originWhitelist={['*']}
        source={{ html: data }}/>



       
        </KeyboardAwareScrollView>

       
         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Privacy);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.LIGHT_BLACK,
    marginTop:15,
   
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  ratingContainer:{
    padding:6,borderRadius:20,
    justifyContent:'center',alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY
  },
  descriptionContainer:{
    padding:6,borderRadius:10,
    backgroundColor:colors.BLACK,
    borderWidth:0.4,
    borderColor:colors.COLOR_PRIMARY,
    width:'95%',alignSelf:'center',
    marginVertical:10
  }, 
  listContainer:{
    width:'98%',
    borderRadius:10,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    height:60,
    marginVertical:5,
    alignSelf:'center',
    overflow:'hidden',justifyContent:'center'
  },
  countryImage:{
      position:'absolute',
      top:0,
      left:0,
      right:0,
      bottom:0,
  },
  countryText:{
      color:'white',
      fontWeight:'bold',
      fontSize:17,
      marginLeft:20
  },
  rowContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
},
container: {
  backgroundColor:colors.GREY,
  margin:10,
  height:dimensions.SCREEN_HEIGHT * 0.24,
  width:dimensions.SCREEN_WIDTH * 0.6,
  overflow:'hidden',
  borderRadius:10,
  borderWidth:0.6,
  borderColor:colors.BLACK,

 },
 imageContainer:{
   margin:0,
   position:'absolute',
   top:0,
   bottom:0,left:0,right:0,
  
 },
 factContainer:{
   height:80,
   flex:1,
   margin:10,
   backgroundColor:colors.LIGHT_BLACK,
   borderRadius:10,
   flexDirection:'row',
   padding:7,
 }


}
)
