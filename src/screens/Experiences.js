import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView,Animated} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import ExperienceComponent from '../components/ExperienceComponent';
import SearchInput from '../components/SearchInput';
import { Form } from 'native-base';

 const d = Dimensions.get("window")



 class Experiences extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
        
          loading_status:false,
          packages: [],
          packages_category:[]
         
        }
    }

    _fetchPackages =  () =>{
        console.log("*********",this.props.user.token)
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'package/packages'
                      fetch(url, {
                      method: 'POST',
                      body:{'limit':6},
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              var list = responseJson.data.list
                              console.log('package/packages----,',list)
                             
                             
                             this.setState({packages:list})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                           
                          });
      
      
      
      }


      _fetchPackagesCategory =  () =>{
        
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'package/package_types'
                      fetch(url, {
                      method: 'POST',
                     
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              var list = responseJson.data.list
                              
                             this.setState({packages_category:list})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                           
                          });
      
      
      
      }

      _getCategoryPackage =  (id) =>{
        console.log("*********",this.props.user.token)
        this.setState({loading_status:true})

        var formData = new FormData()
        formData.append('category_id',id)
      
                 let url = urls.base_url +'package/package'
                      fetch(url, {
                      method: 'POST',
                      body:formData,
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              var list = responseJson.data.list
                              console.log('package/packages----,',list)
                             
                            // if(list.length == 0)
                             this.setState({packages:list})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                           
                          });
      
      
      
      }




    componentWillMount(){
      var res = this.props.navigation.getParam('experiences')
      if(res) this.setState({packages:res}) //means someone clicked view more pfrom previsousscreen
      else   this._fetchPackages()
 
     
      this._fetchPackagesCategory()
    }


    _searchPackages = async (text) =>{
      console.log("*********",this.props.user.token)
    this.setState({loading_status:true})
    var formdata = new FormData()
    formdata.append('name',text)
             let url = urls.base_url +'package/packages'
                  fetch(url, {
                  method: 'POST',
                 body:formdata,
                 headers:{'token':this.props.user.token}
                  }).then((response) => response.json())
                      .then((responseJson) => {

                       
                        this.setState({loading_status:false})

                        if(responseJson.status){
                          var list = responseJson.data.list
                          console.log('countries,',list)

                         this.setState({packages:list})
                        }
                        else{
                          
                        }
                        
                       
                    }
                      ).catch((error) => {
                        this.setState({loading_status:false})

                       
                      });
  
  
  
  }

    _onSearch =()=>{
      var searchText = this.searchInput.getInputValue()
      console.log(searchText)
      this._searchPackages(searchText)

    }

    _applyFilterExperiences =  (data) =>{
      var formData = new FormData()
      formData.append('priceOrder',data.sortAlphabet)
      formData.append('alphabetOrder',data.sortPrice)
      formData.append('between',data.price)
      this.setState({loading_status:true})
               let url = urls.base_url +'package/packageList'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                    },
                    body:formData
                    }).then((response) => response.json())
                        .then((responseJson) => {
                          console.log("*********",responseJson)
                         
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data.list
                            console.log('hotels,',list)
                            this.setState({hotels:list})
                          }
                          else{
                            showMessage("No Hotesl Found")
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }
   
    onApplyFilter = data => {
      console.log("DATA",JSON.stringify(data))
      //this.setState({filters_data:data})
      this._applyFilterExperiences(data)

    };


    _onFilter =()=>{
      this.props.navigation.navigate("Filter" ,{ onApply: this.onApplyFilter })
    }


     
    onPackageClick = (item) =>{
      console.log('onPackageClick--------',JSON.stringify(item))
      var id = item.id
      let obj ={'package_id':id}
      this.props.navigation.navigate("ExperienceDetails",{result:obj})
    
    }
  

    _renderExperience = ({item, index}) =>{
        
        return (
           <ExperienceComponent
            object = {item} 
            index ={index}
            clickHandler ={this.onPackageClick}/>
        )
      }


      _renderCategory = ({item, index}) =>{
        
        return (
          <TouchableOpacity onPress={()=> this._getCategoryPackage(item.id)}
          style={styles.categoryContainer}>
            <View style={{flexDirection:'row',alignItems:'center'}}>
              <Image source={{uri:item.icon}} style={{height:20,width:20,borderRadius:10,overflow:'hidden',marginRight:5}}/>
            <Text style={{color:'white'}}>{item.name}</Text>
            </View>
          </TouchableOpacity>
        )
      }
    render() {

        return (
     <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <ScrollView style={{backgroundColor:'black'}}>
          {/** header */}
          <View style={styles.header}>
          <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}}
          onPress={()=> {this.props.navigation.goBack()} }>
            <FastImage 
            source={require('../assets/back.png')}
            style={{height:30,width:30,marginLeft:4,marginRight:10}} 
            resizeMode={FastImage.resizeMode.contain}/>
            <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>Experiences </Text> 
           </TouchableOpacity>

           <TouchableOpacity style={styles.container}
           onPress={()=>{this.props.navigation.navigate("Menu")} }>
           <FastImage 
           source={require('../assets/menu-gray.png')}
           style={{height:0,width:0}} 
           resizeMode={FastImage.resizeMode.cover}/>
              
            </TouchableOpacity>
          </View>

            {/** header end */}

            <Text style={{color:'white',width:'60%',marginBottom:30,marginTop:20,marginLeft:5}}>Find your favorite experiences using our filter and sort products.</Text>



            <FlatList
            style={{marginVertical:10}}
            horizontal={true}
            numColumns={1}
            data={this.state.packages_category}
            showsHorizontalScrollIndicator={false}
            renderItem={(item,index) => this._renderCategory(item,index)}
            keyExtractor={item => item.id}
        />


        <SearchInput
        placeholder={'Search Experiences..'}
         onSubmitEditing={()=> this._onSearch()}
        onFinish={this._verifyOtp}
        inputRef={ref => this.search = ref}
        ref={ref => this.searchInput = ref}
        style={{width:'97%',alignSelf:'center',marginVertical:10}}
        returnKeyType="go"
        onSearch={this._onSearch}
        onFilter={this._onFilter}
        haveFilter={true}
       />


               {/** hotels */}
               <View style={styles.hotelContainer}>
              
                <FlatList
                    style={{marginVertical:10}}
                    horizontal={false}
                    numColumns={1}
                    data={this.state.packages}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderExperience(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** hotels end */}

              
         
         </ScrollView>

         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Experiences);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.LIGHT_BLACK,
    marginTop:15,
   
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },


  categoryContainer:{
    width:'auto',
    paddingHorizontal:15,
    paddingVertical:8,
    backgroundColor:colors.LIGHT_BLACK,
    borderRadius:20,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    marginHorizontal:5,
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'row'

  }


}
)
