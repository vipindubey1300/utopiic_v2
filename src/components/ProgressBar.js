import React from 'react';
import {Text, View, Image,Dimensions,
    ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
    import {  colors,urls } from '../Constants';
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator
  } from 'react-native-indicators';



const ProgressBar = () => {
    return(
        <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.3)', justifyContent: 'center' }
          ]}>
          <BarIndicator color={colors.color_primary}/>
          </View>
    )
  };
  export default ProgressBar;