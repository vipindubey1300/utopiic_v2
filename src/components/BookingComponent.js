import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'

export default class BookingComponent extends React.Component {
    constructor(props) {
        super(props);
        this.delayValue = 500;
        this.state = {
          animatedValue: new Animated.Value(0),
          data:[]
        }
      }

      componentDidMount = () => {
        Animated.spring(this.state.animatedValue, {
          toValue: 1,
          tension: 20,
          useNativeDriver: true
        }).start();
      }
	

  render() {
      const {object} = this.props
      this.delayValue = this.delayValue + 500;
      const translateX = this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [this.delayValue, 1]
      });
    return (
        <Animated.View  style={{ transform: [{ translateX }]}}>
        <TouchableOpacity style={styles.container}>
        <View onPress={()=> this.props.clickHandler(this.props.object)}
         style={styles.imageContainer} >
            <FastImage 
            source={{uri:object.image}}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.cover}/>
        </View>

       

        <FastImage 
        source={require('../assets/category1_icon.png')}
        style={styles.icon} 
        resizeMode={FastImage.resizeMode.cover}/>
        </TouchableOpacity>
        </Animated.View>
     
    );

  }
}



const styles = StyleSheet.create({
  container: {
   backgroundColor:colors.WHITE,
   margin:10,
   height:dimensions.SCREEN_HEIGHT * 0.20,
   width:dimensions.SCREEN_WIDTH * 0.43,
   overflow:'hidden',
   borderRadius:10,
   borderWidth:0.6,
   borderColor:colors.BLACK,

  },
  imageContainer:{
    margin:0,
    position:'absolute',
    top:0,
    bottom:0,left:0,right:0,
   
  },

 
  imageStyle:{
  
   height:'100%',width:'100%'
  },
  
  icon:{
      position: 'absolute',right: 10,
      top:0,height:25,width:25
  }
});