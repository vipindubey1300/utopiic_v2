import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import ButtonComponent from '../components/ButtonComponent';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'
import DateTimePicker from 'react-native-modal-datetime-picker';

import SwiperFlatList from 'react-native-swiper-flatlist';
import { Rating, AirbnbRating } from 'react-native-elements';
import RazorpayCheckout from 'react-native-razorpay';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import RequestComponent from '../components/RequestComponent';
import stripe from 'tipsi-stripe'
import { showMessage } from '../utils/showMessage';

 const d = Dimensions.get("window")



 class Payments extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          payments:[],
      };

    }

    _fetchRequests =  () =>{
        console.log("*********",this.props.user.token)
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'booking/paid_history'
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              var list = responseJson.data
                            //  console.log('requests,',list)
                             
                             
                             this.setState({payments:list})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                           
                          });
      
      
      
      }



    componentWillMount(){
        this._fetchRequests()
    }


    convertDate(date) {
        var yyyy = date.getFullYear().toString();
        var mm = (date.getMonth()+1).toString();
        var dd  = date.getDate().toString();
    
        var mmChars = mm.split('');
        var ddChars = dd.split('');
    
        return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
      }
    



    _renderRequest = ({item, index}) =>{
        //admin_status  0 - pending from admin side 1 admin approved the booking
        //status 0 -pending 1 Approved 2 Paid 3 Completed
        //payment_status - pending or paid
        //booking_type_name - this will be the type of booking like package, hotel, transport, native, tour to transfer
        return (
           <RequestComponent
            object = {item} 
            onPay={this._razorPay}
            index ={index}/>
        )
      }


    render() {

        return (
          <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <View style={{backgroundColor:colors.LIGHT_BLACK}}>

    


          {/** header */}
          <View style={styles.header}>
          <View style={{flexDirection:'row',alignItems:'center'}}>
          <TouchableOpacity
          onPress={()=> {this.props.navigation.goBack()} }>

          <FastImage 
          source={require('../assets/round_back_black.png')}
          style={{height:40,width:40}} 
          resizeMode={FastImage.resizeMode.contain}/>
           </TouchableOpacity>
           <Text style={{color:colors.COLOR_PRIMARY,fontSize:18,marginHorizontal:10}}>Payments</Text> 

           </View>

          <View style={{margin:12,marginTop:40}}>
            <Text style={{fontWeight:'100',fontSize:14,marginVertical:5,color:'white',width:'80%'}}>The details of your payment and the complete costs associated with it are listed below.Please take a look an confirm your paymemnts below.</Text> 
               
          </View>
          </View>

            {/** header end */}

            <View style={{height:10}}/>



               {/** hotels */}
             
                <FlatList
                    style={{marginVertical:10}}
                    horizontal={false}
                    numColumns={1}
                    data={this.state.payments}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderRequest(item,index)}
                    keyExtractor={item => item.id}
                />

              {/** hotels end */}

              </View>

            

         </SafeAreaView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Payments);

let styles = StyleSheet.create({
  header:{
    
       backgroundColor:colors.BLACK,
       borderBottomRightRadius:20,
       height:dimensions.SCREEN_HEIGHT * 0.3
  },
 
  bottomContainer:{
    position:'absolute',
    bottom:0,left:0,right:0,
    height:45,
     backgroundColor:'rgba(30,30,30,0.7)',
   // backgroundColor:'rgba(0,0,0,0.91)',
   borderTopColor:'black',
   borderTopWidth:0.7

  
  },
  blackContainer:{
    width:'95%',
    paddingHorizontal:5,
    paddingVertical:10,
    backgroundColor:colors.BLACK,
    borderRadius:20,
    marginVertical:5,
    alignSelf:'center',
  
   
  },
  yellowText:{
    color:colors.COLOR_PRIMARY
  },
  whiteText:{
    color:colors.WHITE
  },
  rowContainer:{
    flexDirection:'row',
   // justifyContent:'space-between',
    alignItems:'center',
   
},
buttonContainer:{
  borderRadius:20,
  backgroundColor:colors.BLACK,
  padding:10,
  justifyContent:'center',
  alignItems: 'center',
  borderWidth:0.5,borderColor:colors.COLOR_PRIMARY,
  marginBottom:10,
  width:150,alignSelf:'center'
}

}
)
