import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Rating, AirbnbRating } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import ChooseDateSheet from '../components/ChooseDateSheet';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")



 class ViewImage extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          image:''
      };

    }


    componentWillMount(){
      var result = this.props.navigation.getParam('result')
      console.log('_fetchDetails-----',result['image'])
       this.setState({image:result['image']})
  }
   
    render() {

      const {image} = this.state

        return (
      <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
    
        {/** header */}
        <View style={styles.header}>
        <TouchableOpacity style={{flexDirection:'row',alignItems:'center',padding:10}}
        onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/back.png')}
          style={{height:30,width:30,marginLeft:4,marginRight:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
          <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>Gallery </Text> 
         </TouchableOpacity>

         <View/>
        </View>

          {/** header end */}
        <KeyboardAwareScrollView contentContainerStyle={{
          justifyContent:'center',
        
        }}>

        <View style={{height:dimensions.SCREEN_HEIGHT * 0.9,width:dimensions.SCREEN_WIDTH}} >
        <FastImage 
        source={{uri:image}}
        style={{height:'100%',width:'100%',backgroundColor:'grey'}}
        resizeMode={FastImage.resizeMode.cover}/>
        </View>

        
        </KeyboardAwareScrollView>
         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(ViewImage);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
 

  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  

}
)
