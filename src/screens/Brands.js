import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView,Animated} from 'react-native';
import { colors ,urls,dimensions} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import StayComponent from '../components/StayComponent';
import SearchInput from '../components/SearchInput';
import { showMessage } from '../utils/showMessage';
import BrandComponent from '../components/BrandComponent';

 const d = Dimensions.get("window")



 class Brands extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
        
          loading_status:false,
          brands: [],
          brands_global:[]
         
        }
    }
    _fetchBrands = async () =>{
        console.log("*********",this.props.user.token)
      this.setState({loading_status:true})
               let url = urls.base_url +'hotels/getsuppliers'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
  
                         console.log("getsuppliers*********",JSON.stringify(responseJson))
                          this.setState({loading_status:false})
  
                          if(responseJson.status){
                            var list = responseJson.data.Suppliers
                            var temp=[]
                            if(list.length>0){
                                list.map(e=>{
                                    if(!temp.includes(e.industry_name)) temp.push(e.industry_name)
                                })
                            }
                          
                            var filtered = temp.filter(function (el) {
                              return el != null;
                            });
                           
                           this.setState({brands:list,brands_category:filtered,brands_global:list})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
                            showMessage('Try Again')
                         
                        });
    
    
    
    }

  
    componentWillMount(){
            this._fetchBrands()
    }


    onBrandClick = (item) =>{
      console.log('onBrandClick--------',JSON.stringify(item))
      var id = item.id
      let obj ={'brand':item}
      this.props.navigation.navigate("BrandDetails",{result:obj})
      
      }
    
    
         _renderBrand = ({item, index}) =>{
            return (
               <BrandComponent
                object = {item} 
                textBackground={colors.LIGHT_BLACK}
                index ={index}
                clickHandler ={this.onBrandClick}/>
            )
          }
    
          
          _renderCategory = ({item, index}) =>{
            console.log('item--',item)
            return (
              <TouchableOpacity onPress={()=> {}}
              style={styles.categoryContainer}>
                <Text style={{color:'white'}}>{item.toString()}</Text>
              </TouchableOpacity>
            )
   }
  _filtercategory =(industry_name)=>{

    var filteredBrands= this.state.brands.filter(function(brand){
      return brand.industry_name == industry_name;
     });
     this.setState({brands:filteredBrands})
  }


  _searchBrands =(name)=>{

    var filteredBrands= this.state.brands_global.filter(function(brand){
      return brand.name.includes(name)
     });
     if(filteredBrands.length == 0) showMessage("No Brands Found")
     this.setState({brands:filteredBrands})
  }

  _onSearch =()=>{
    var searchText = this.searchInput.getInputValue()
    console.log(searchText)
    this._searchBrands(searchText)

  }

    render() {

        return (
     <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
          <View style={{backgroundColor:'black'}}>
          {/** header */}
          <View style={styles.header}>
          <TouchableOpacity style={{flexDirection:'row',alignItems:'center'}}
          onPress={()=> {this.props.navigation.goBack()} }>
            <FastImage 
            source={require('../assets/back.png')}
            style={{height:30,width:30,marginLeft:4,marginRight:10}} 
            resizeMode={FastImage.resizeMode.contain}/>
            <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>Brands </Text> 
           </TouchableOpacity>

           <TouchableOpacity style={styles.container}
           onPress={()=>{this.props.navigation.navigate("Filter" ,{ onApply: this.onApplyFilter })} }>
           <FastImage 
           source={require('../assets/menu-gray.png')}
           style={{height:0,width:0}} 
           resizeMode={FastImage.resizeMode.cover}/>
              
            </TouchableOpacity>
          </View>

            {/** header end */}

            <Text style={{color:'white',width:'60%',marginBottom:30,marginTop:20,marginLeft:5}}>Find your favorite brands to enjoy .</Text>



              {
               this.state.brands_category &&  this.state.brands_category.map(e=>{
                  return(
                    <TouchableOpacity onPress={()=> {this._filtercategory(e)}}
                    style={styles.categoryContainer}>
                      <Text style={{color:'white'}}>{e.toString()}</Text>
                    </TouchableOpacity>
                  )
                })
              }

              <View style={{height:15}}/>

                <SearchInput
                placeholder={'Search Brands..'}
                onSubmitEditing={()=> this._onSearch()}
              // onFinish={this._verifyOtp}
                inputRef={ref => this.search = ref}
                ref={ref => this.searchInput = ref}
                style={{width:'90%',alignSelf:'center',marginVertical:10}}
                returnKeyType="go"
                onSearch={()=>this._onSearch()}
            />



               {/** hotels */}
               <View style={[styles.hotelContainer,{
                 height:this.state.brands.length > 0 ? 'auto' : dimensions.SCREEN_HEIGHT * 0.8
               }]}>
              
                <FlatList
                    style={{marginVertical:10}}
                    numColumns={2}
                    data={this.state.brands}
                    showsVerticaltalScrollIndicator={false}
                    renderItem={(item,index) => this._renderBrand(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** hotels end */}

              
         
         </View>

         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(Brands);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.LIGHT_BLACK,
    marginTop:15,
    //height:dimensions.SCREEN_HEIGHT * 0.8
   
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  categoryContainer:{
    width: 100,
    paddingHorizontal:15,
    paddingVertical:8,
    backgroundColor:colors.LIGHT_BLACK,
    borderRadius:20,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    marginHorizontal:5,
    justifyContent:'center',
    alignItems:'center',
    marginVertical:10
   

  }

}
)
