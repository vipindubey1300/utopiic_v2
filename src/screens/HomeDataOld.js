import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions, fonts} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';


import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';
import PackageComponent from '../components/PackageComponent';


 const d = Dimensions.get("window")



 class HomeData extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          packages:[],
          loading_status:false,
          selectedTab:'',
          baseUrl:'',
          baseUrlBanner:'',
          banner: []
         

      };

    }




    _fetchBanners = async () =>{
        console.log("*********",this.props.user.token)
      this.setState({loading_status:true})
               let url = urls.base_url +'hotels/getsuppliers'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {

                          console.log("BANEERRSSSS*********",JSON.stringify(responseJson))
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data.list
                            //console.log('lists,',list)
                            var baseUrl = responseJson.data.image_path
                           
                           this.setState({banner:list,baseUrlBanner:baseUrl})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }

    _fetchPackages = async () =>{
      console.log(' all_package lists _fetchPackages-----------,')
      this.setState({loading_status:true})
    
               let url = urls.base_url +'package/packages'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                       'Content-Type':'application/x-www-form-urlencoded'
                    },
                    body:JSON.stringify({
                      'limit':10
                    })
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
                           // console.log("REsponse ----",responseJson)
                         
                          this.setState({loading_status:false})

                          if(responseJson.status){
                            var list = responseJson.data.list
                           // console.log(' all_package lists-----------,',list)
                           
                           
                           this.setState({packages:list})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})

                         
                        });
    
    
    
    }

    _fetchHotels =  () =>{
        console.log("*********",this.props.user.token)
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'hotels/list_hotels'
                      fetch(url, {
                        method: 'POST',
                        headers: {
                           'token': this.props.user.token,
                         
                        },
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                        //  console.log("*********",JSON.stringify(responseJson))
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              var list = responseJson.data.list
                              //console.log('hotels,',list)
                             
                             
                             this.setState({hotels:list})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})
  
                           
                          });
      
      
      
      }








    componentWillMount(){
      console.log(JSON.stringify(this.props.user))
     this._fetchBanners()
     this._fetchHotels()
     this._fetchPackages()
    }


    onHotelClick = (item) =>{
        console.log('onHotelClick--------',JSON.stringify(item))
        var id = item.id
        let obj ={'hotel_id':id}
        this.props.navigation.navigate("HotelDetails",{result:obj})
      
      }



    onPackageClick = (item) =>{
      console.log('onPackageClick--------',JSON.stringify(item))
      var id = item.id
      let obj ={'package_id':id}
      this.props.navigation.navigate("ExperienceDetails",{result:obj})
    
    }
    onBannerClick = (item) =>{
     
      let obj ={'url':item.link}
      this.props.navigation.navigate("ViewWeb",{result:obj})
    
    }

    _renderBanner = ({item, index}) =>{
       return (
          <BannerComponent
           object = {item} 
           index ={index}
           
           baseUrl ={this.state.baseUrlBanner}
           clickHandler ={this.onBannerClick}/>
       )
     }

     _renderHotel = ({item, index}) =>{
        return (
           <HotelComponent
            object = {item} 
            index ={index}
            baseUrl ={this.state.baseUrlBanner}
            clickHandler ={this.onHotelClick}/>
        )
      }

      _renderExperiences = ({item, index}) =>{
        return (
           <PackageComponent
            object = {item} 
            textBack={colors.LIGHT_BLACK}
            index ={index}
            clickHandler ={this.onPackageClick}/>
        )
      }

    render() {

        return (
          <ScrollView style={{flex:1}}>
          {/** header */}
          <View style={styles.header}>
          <TouchableOpacity style={styles.container}
          onPress={()=> {} }>
          <FastImage 
          source={require('../assets/utopiic-logo-text.png')}
          style={{height:40,width:100,marginLeft:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
             
           </TouchableOpacity>

           <TouchableOpacity style={styles.container}
          >
           <FastImage 
           source={require('../assets/search_yellow.png')}
           style={{height:20,width:20,margin:15}} 
           resizeMode={FastImage.resizeMode.cover}/>
              
            </TouchableOpacity>
          </View>

            {/** header end */}


            {/** heading */}
                <Text style={{color:'white',fontSize:18,margin:10,fontFamily:'Helvetica'}}>Hi {this.props.user.name},</Text>
                <Text style={{color:'white',fontSize:12,margin:10,fontFamily:'gillsans'}}>Find and book your favourite travel destinations across the globe with Utopiic Travel Concierge.</Text>

             {/** heading end */}



               {/** exclusive */}
               <View style={styles.exclusiveContainer}>
                <View style={styles.rowContainer}>
                    <Text style={{fontSize:16,fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR}}>  Exclusives</Text>
                    <TouchableOpacity style={styles.container}
                    onPress={()=>{} }>
                    <FastImage 
                    source={require('../assets/arrow_forward_yellow.png')}
                    style={{height:0,width:0}} 
                    resizeMode={FastImage.resizeMode.cover}/>
                       
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10}}
                    horizontal={true}
                    data={this.state.banner}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderBanner(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** exclusive end */}




               {/** hotels */}
               <View style={styles.hotelContainer}>
                <View style={styles.rowContainer}>
                    <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,fontSize:16}}>  Stays</Text>
                    <TouchableOpacity style={styles.container}
                    onPress={()=>{this.props.navigation.navigate("Stays")} }>
                    <FastImage 
                    source={require('../assets/arrow_forward_yellow.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.cover}/>
                       
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10}}
                    horizontal={true}
                    data={this.state.hotels}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderHotel(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** hotels end */}


               {/** Experiences */}
               <View style={styles.exclusiveContainer}>
                <View style={styles.rowContainer}>
                    <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,fontSize:16}}>  Experiences</Text>
                    <TouchableOpacity style={styles.container}
                    onPress={()=>{this.props.navigation.navigate("Experiences")} }>
                    <FastImage 
                    source={require('../assets/arrow_forward_yellow.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.cover}/>
                       
                     </TouchableOpacity>
                </View>


                <FlatList
                    style={{marginVertical:10}}
                    horizontal={true}
                    data={this.state.packages}
                    showsHorizontalScrollIndicator={false}
                    renderItem={(item,index) => this._renderExperiences(item,index)}
                    keyExtractor={item => item.id}
                />

               </View>

              {/** Experiences end */}


              
         
         </ScrollView>




        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(HomeData);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
  exclusiveContainer:{
      paddingVertical:10,
      paddingHorizontal:2,
      borderTopRightRadius:16,
      width:dimensions.SCREEN_WIDTH,
      backgroundColor:colors.LIGHT_BLACK,
      marginTop:10,
      paddingBottom:30
  },
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.BLACK,
    marginTop:-15
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  }

}
)
