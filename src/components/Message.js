import React from 'react';
import {Text, View, Image,Dimensions,
    ToastAndroid,StatusBar,StyleSheet, TouchableOpacity, SafeAreaView} from 'react-native';
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator
  } from 'react-native-indicators';

  import * as Animatable from 'react-native-animatable';

  import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image';

const Message = (props) => {
    return(
        <View style={[
            StyleSheet.absoluteFill,
            { backgroundColor: 'rgba(0, 0, 0, 0.7)', justifyContent: 'center',alignItems:'center' }
          ]}>

            {
                props.success
                ?
                <FastImage 
            source={require('../assets/verified_yellow.png')}
            style={{height:75,width:75,}} 
            resizeMode={FastImage.resizeMode.cover}/>
                    :
                    <FastImage 
            source={require('../assets/error.png')}
            style={{height:75,width:75,}} 
            resizeMode={FastImage.resizeMode.cover}/>


            }
        
        <Animatable.Text
        animation="pulse"
        easing="ease-out" 
        iterationCount="infinite" 
        style={{ textAlign: 'center',color: props.success ?colors.COLOR_PRIMARY : 'red',
        fontSize:22,margin:15 ,fontWeight:'bold'}}>{props.message}</Animatable.Text>


          </View>
    )
  };


  Message.defaultProps = {

    success: false,
    message:''
    
  };

  export default Message;