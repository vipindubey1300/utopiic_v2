import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView, Linking} from 'react-native';
import { colors ,urls,dimensions,fonts} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Rating, AirbnbRating } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import ChooseDateSheet from '../components/ChooseDateSheet';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';

 const d = Dimensions.get("window")



 class HotelDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
           hotel_info: '',
           hotel_images:[],
           hotel_facilities:[],
           addressRegion:{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
           }
          
          
         

      };

    }





    _fetchDetails =  (hotelId) =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'hotels/hotel_details/'+ hotelId
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",responseJson)
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              this.setState({hotel_info:responseJson.data.hotel_info,
                                hotel_images:responseJson.data.hotel_images,
                                hotel_facilities:responseJson.data.hotel_facilities,
                              addressRegion:{
                                latitude:parseFloat(responseJson.data.hotel_info.lat),
                                longitude:parseFloat(responseJson.data.hotel_info.lng),
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                              }})
                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }


    // componentWillMount(){
    //  // console.log(JSON.stringify(this.props.user))
   
    //  this._fetchHotels()
    //  // this._fetchPackages()
    // }


    componentWillMount(){
      var result = this.props.navigation.getParam('result')
      console.log('_fetchDetails-----',result['hotel_id'])
       this._fetchDetails(result['hotel_id'])
  }


  openMap= () => {
   console.log('open directions')
   let f = Platform.select({
        ios: () => {
            Linking.openURL('http://maps.apple.com/maps?daddr='+this.state.hotel_info.lat+','+this.state.hotel_info.lng+'&dir_action=navigate');
        },
        android: () => {
            console.log('ANDROID')
            Linking.openURL('http://maps.google.com/maps?daddr='+this.state.hotel_info.lat+','+this.state.hotel_info.lng+'&dir_action=navigate').catch(err => console.error('An error occurred', err));;
        }
    });

    f();
}

 _renderFacilities = ({item, index}) =>{
  return (
     <TouchableOpacity
      style={styles.listContainer}>
     <FastImage 
     source={{uri:item.image}}
     style={styles.facilitiesImage} 
     resizeMode={FastImage.resizeMode.contain}/>

      <Text style={styles.facilitiesText}>{item.name}</Text>
     </TouchableOpacity>
  )
}

renderPage = ({item, index}) =>{
  return (
    <FastImage 
    source={{uri:item}}
    style={{height:'100%',width:'100%',backgroundColor:'grey'}}
    resizeMode={FastImage.resizeMode.cover}/>
  );
}
_success =(fromDate,toDate)=>{
  console.log(fromDate,toDate)
  this.dateSheet.close()
  var hotel = this.state.hotel_info
  let obj ={
    'hotel':hotel,
    'from_date':fromDate,
    'to_date':toDate,
    'is_addons':hotel.is_addons // 1 means yes
  }
  this.props.navigation.navigate("SelectRoom",{result:obj})
  
}
_onDone =()=>{
  this.dateSheet.open()
}

   
    render() {

      const {hotel_info,hotel_images,hotel_facilities} = this.state

        return (
      <SafeAreaView style={{flex:1,backgroundColor:'black'}}>
      <ChooseDateSheet
      inputRef={ref => this.dateSheet = ref} //for close/open sheet
      ref={ref => this.chooseDateSheet = ref}
      success ={this._success}
      />
        {/** header */}
        <View style={styles.header}>
        <TouchableOpacity style={{flexDirection:'row',alignItems:'center',padding:10}}
        onPress={()=> {this.props.navigation.goBack()} }>
          <FastImage 
          source={require('../assets/back.png')}
          style={{height:30,width:30,marginLeft:4,marginRight:10}} 
          resizeMode={FastImage.resizeMode.contain}/>
          <Text style={{color:colors.COLOR_PRIMARY,fontSize:18}}>{hotel_info.name} </Text> 
         </TouchableOpacity>

         <View/>
        </View>

          {/** header end */}
        <KeyboardAwareScrollView contentContainerStyle={{
          justifyContent:'center',
         paddingBottom:50
        }}>

        <View style={{height:dimensions.SCREEN_HEIGHT * 0.3,width:dimensions.SCREEN_WIDTH}} >
       

          <Carousel
          ref={(c) => { this._carousel = c; }}
          data={this.state.hotel_images}
          renderItem={this.renderPage}
          //autoplay={true}
         // loop
          sliderWidth={dimensions.SCREEN_WIDTH}
          itemWidth={dimensions.SCREEN_WIDTH }
        />
        </View>

        

           <View style={{borderRadius:15,padding:5,backgroundColor:colors.LIGHT_BLACK,marginTop:-10,paddingBottom:15}}>
            <View style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',padding:10}}>
                <View>
                  <Text style={{color:colors.COLOR_PRIMARY}}>{hotel_info.name}</Text>
                    <Rating
                    type='custom'
                    readonly={true}
                    ratingColor={colors.COLOR_PRIMARY}
                    ratingBackgroundColor='rgba(30,30,30,1)'
                    ratingCount={5}
                    imageSize={14}
                    startingValue={4}
                    tintColor='rgba(30,30,30,1)'
                    style={{ paddingVertical: 6,alignSelf:'flex-start'}}
                  />
                  <Text style={{color:colors.WHITE}}>Luxury</Text>

                </View>


                <View style={styles.ratingContainer}>
                  <Text style={{color:colors.BLACK}}>{hotel_info ? parseFloat(hotel_info.star_rating).toFixed(1) : 0.0}</Text>
                </View>
            </View>

            <View style={styles.descriptionContainer}>
            <Text style={{color:colors.GREY}}>{hotel_info.description}</Text>
             </View>

             <Text style={{color:'white',marginVertical:7}}>{hotel_info.city} ,{hotel_info.country}</Text>
             <View  pointerEvents={'none'} style = {{height:200, width:dimensions.SCREEN_WIDTH*0.95,marginBottom:20,
             overflow:'hidden',zIndex: -1,borderRadius:20,alignSelf:'center'
            }}>
                <MapView
                style = {{flex: 1, height: '100%', width: '100%'}}
                  initialRegion={{
                latitude: 28.78825,
                longitude: 77.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
              }}
              region={this.state.addressRegion}
              showsUserLocation = {true}
              followUserLocation = {true}
              zoomEnabled = {true}
              >
                 <MapView.Marker coordinate = {{latitude:this.state.addressRegion.latitude,
                 longitude:this.state.addressRegion.longitude}}
                  pinColor = {"red"} // any color
                  title={"title"}
                  description={"description"}/>

              </MapView>
           </View>

           <TouchableOpacity onPress={()=> this.openMap()}
            style={{
            flexDirection:'row',
            alignItems:'center',
            marginTop:7,
            marginBottom:7
          }}>
          <Image
          source={require('../assets/pin.png')}
          resizeMode='stretch'
          style={{width:25, height: 25,margin:5}}/>
          <Text style={{ fontSize:12,color:'white',width:'70%'}}>{hotel_info.address}</Text>
          </TouchableOpacity>

           <View style={{height:2,width:'100%',backgroundColor:'black',marginVertical:10}}/>
    
           <Text style={{fontFamily:fonts.heading_font,textTransform:'uppercase',color:colors.HEADING_COLOR,fontSize:18,fontWeight:'200',marginVertical:10}}>  Facilities</Text>

           <FlatList
           contentContainerStyle={{alignSelf:'center'}}
           style={{marginVertical:10}}
           horizontal={false}
           numColumns={2}
           data={this.state.hotel_facilities}
           showsHorizontalScrollIndicator={false}
           renderItem={(item,index) => this._renderFacilities(item,index)}
           keyExtractor={item => item.id}
          />

          <View style={{height:2,width:'100%',backgroundColor:'black',marginVertical:10}}/>


          <View style={styles.rowContainer}>
          <Text style={{color:colors.WHITE,fontSize:16,fontWeight:'200'}}>  Property Policies</Text>
          <TouchableOpacity style={styles.container}
          onPress={()=>{this.props.navigation.navigate("Privacy")} }>
          <FastImage 
          source={require('../assets/arrow_forward_yellow.png')}
          style={{height:25,width:25}} 
          resizeMode={FastImage.resizeMode.cover}/>
             
           </TouchableOpacity>
        </View>


           </View>


         
        
        </KeyboardAwareScrollView>

        <View style={styles.bottomContainer}>
            <ButtonComponent 
            style={{marginTop:-15,width:dimensions.SCREEN_WIDTH * 0.4,alignSelf:'center'}}
            handler={this._onDone}
            label ={'Select Rooms'}/>
        </View>
         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(HotelDetails);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.LIGHT_BLACK,
    marginTop:15,
   
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  ratingContainer:{
    padding:6,borderRadius:20,
    justifyContent:'center',alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY
  },
  descriptionContainer:{
    padding:6,borderRadius:10,
    backgroundColor:colors.BLACK,
    borderWidth:0.4,
    borderColor:colors.COLOR_PRIMARY,
    width:'95%',alignSelf:'center',
    marginVertical:10
  }, 
  listContainer:{
    width:'45%',
    borderRadius:30,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    height:50,
    marginVertical:5,
    alignSelf:'center',
    overflow:'hidden',flexDirection:'row',alignItems:'center',marginHorizontal:10
  },
  facilitiesImage:{
    height:20,width:20,marginHorizontal:5
  },
  facilitiesText:{
      color:'white',
      fontWeight:'100',
      fontSize:13,
      marginLeft:10
  },
  rowContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
},
bottomContainer:{
  position:'absolute',
  bottom:0,left:0,right:0,
  height:45,
  // backgroundColor:'rgba(30,30,30,0.7)'
  backgroundColor:'rgba(0,0,0,0.91)'

}

}
)
