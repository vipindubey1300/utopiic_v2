import React from 'react';

import store from './src/store/store';
import { Provider } from 'react-redux';
import RootNavigator from './RootNavigator';


export default class App extends React.Component {

	constructor(props) {
		super(props);
	}



	render() {
		return (
			<Provider store={store}>
				<RootNavigator/>
			</Provider>
		);
	}
}
