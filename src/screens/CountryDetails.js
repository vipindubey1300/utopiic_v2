import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,ActivityIndicator,TextInput,FlatList,SafeAreaView} from 'react-native';
import { colors ,urls,dimensions, fonts} from '../app_constants';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import { Rating, AirbnbRating } from 'react-native-elements';
import Carousel from 'react-native-snap-carousel';

import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
// { Indicator, Pages } from 'react-native-pages';
import FastImage from 'react-native-fast-image'

import SwiperFlatList from 'react-native-swiper-flatlist';
import ParallaxScroll from '@monterosa/react-native-parallax-scroll';
import PackageComponent from '../components/PackageComponent';

import MapView , {PROVIDER_GOOGLE} from 'react-native-maps';

import ButtonComponent from '../components/ButtonComponent';

import { connect } from 'react-redux';

import BannerComponent from '../components/BannerComponent';
import HotelComponent from '../components/HotelComponent';
import LinearGradient from 'react-native-linear-gradient';
import BrandComponent from '../components/BrandComponent';

 const d = Dimensions.get("window")



 class CountryDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state ={
          loading_status:false,
           data:'',
           images:[],
           temperature:'0.00',
           brands:[],
           moreVisibility:false
          
         

      };

    }



     getWeather(country) {
      //const openweather_api = "https://api.openweathermap.org/data/2.5/weather?appid=f6c9f143d6581381662978157abcb2aa&lat=" + lat +"&lon=" + long;
      const openweather_api = "http://api.openweathermap.org/data/2.5/weather?q="+country+"&appid=f6c9f143d6581381662978157abcb2aa&units=imperial"
      fetch(openweather_api)
      .then(res => res.json())
      .then((data) => {
        console.log('data', JSON.stringify(data))
        if(data.cod == 200){
          if(data.main){
            var temperature = data.main.temp // in farenheight
            this.setState({temperature:temperature})
            
            // var message = fTemp+'\xB0F is ' + fToCel + '\xB0C.';
            //   console.log(message)
            
          }
        }
       
      })
    
  }

    _fetchCountry =  (countryId) =>{
       
        this.setState({loading_status:true})
      
                 let url = urls.base_url +'general/country_details/'+ countryId
                      fetch(url, {
                      method: 'POST',
                      headers: {
                         'token': this.props.user.token,
                      }
      
                      }).then((response) => response.json())
                          .then((responseJson) => {
                            console.log("*********",JSON.stringify(responseJson))
                           
                            this.setState({loading_status:false})
  
                            if(responseJson.status){
                              this.setState({data:responseJson.data,
                                images:responseJson.data.images})
                                this.getWeather(responseJson.data.name)

                            }
                            else{
                              
                            }
                            
                           
                        }
                          ).catch((error) => {
                            this.setState({loading_status:false})                           
                          });
      
      
      
      }

      _fetchBrands = async () =>{
        console.log("*********",this.props.user.token)
      this.setState({loading_status:true})
               let url = urls.base_url +'hotels/getsuppliers'
                    fetch(url, {
                    method: 'POST',
                    headers: {
                       'token': this.props.user.token,
                    }
    
                    }).then((response) => response.json())
                        .then((responseJson) => {
  
                          console.log("BANEERRSSSS*********",JSON.stringify(responseJson))
                          this.setState({loading_status:false})
  
                          if(responseJson.status){
                            var list = responseJson.data.Suppliers
                           
                           
                           this.setState({brands:list.slice(0,16)})
                          }
                          else{
                            
                          }
                          
                         
                      }
                        ).catch((error) => {
                          this.setState({loading_status:false})
  
                         
                        });
    
    
    
    }

   


    componentWillMount(){
      var result = this.props.navigation.getParam('result')
      console.log('country_id-----',result['country_id'])
       this._fetchCountry(result['country_id'])
       this._fetchBrands()
 }






_attractionClick (item){
  console.log('.......',item)
  var aid = item.id
  let obj ={'attraction_id':aid}
  this.props.navigation.navigate("AttractionDetails",{result:obj})
}



_renderAttractions= ({item, index}) => {
console.log(index % 2 == 0 ? 'true':'false')
  if(index % 2 == 0){
    return (
      <TouchableOpacity  onPress={()=>this._attractionClick(item)}
      style={{flexDirection:'row',marginVertical:8,height:170}}>
         <FastImage source={{uri:item.images}}
         resizeMode={FastImage.resizeMode.cover}
         style={styles.attractionsImage}/>


          <View style={{flex:7,justifyContent:'space-evenly',marginHorizontal:10}}>
          <Text style={{color:'white',marginBottom:10}}>{item.name}</Text>
          <Text style={{color:'grey',fontSize:11}}>{item.description.substring(0,300)} ...</Text>
          <View style={styles.readContainer}>
            <Text>Read More </Text>
          </View>
         </View>
      </TouchableOpacity>
  );
  }
  else{
    return(
      <TouchableOpacity  onPress={()=>this._attractionClick(item)}
      style={{flexDirection:'row',marginVertical:8,height:170}}>
        

          <View style={{flex:7,justifyContent:'space-evenly',marginHorizontal:10}}>
          <Text style={{color:'white',marginBottom:10}}>{item.name}</Text>
          <Text style={{color:'grey',fontSize:11}}>{item.description.substring(0,300)} ...</Text>
          <View style={styles.readContainer}>
            <Text>Read More </Text>
          </View>
         </View>

         <FastImage source={{uri:item.images}}
         resizeMode={FastImage.resizeMode.cover}
         style={styles.attractionsImage}/>

      </TouchableOpacity>
    )
  }
 
}
renderPage = ({item, index}) =>{
  return (
    <FastImage 
    source={{uri:item}}
    style={{height:'100%',width:'100%',backgroundColor:'grey'}}
    resizeMode={FastImage.resizeMode.cover}/>
  );
}


onBrandClick = (item) =>{
  console.log('onBrandClick--------',JSON.stringify(item))
  var id = item.id
  let obj ={'brand':item}
  this.props.navigation.navigate("BrandDetails",{result:obj})

}


   _renderBrand = ({item, index}) =>{
      return (
         <BrandComponent
          object = {item} 
          index ={index}
          clickHandler ={this.onBrandClick}/>
      )
    }

    _getAbout=()=>{
      if(this.state.moreVisibility){
        return(
          <Text  style={{color:colors.WHITE}} 
          >{this.state.data.facts.about+ '  ' }
          <Text onPress={()=> this.setState({moreVisibility:!this.state.moreVisibility})}
           style={{color:colors.DARK_GREY,textDecorationLine:'underline'}}>done...</Text>
          </Text>
        )
      }
      else{
        return(
          <Text  style={{color:colors.WHITE}} 
        >{this.state.data.facts.about.substring(0,200) + '  ' }
        <Text onPress={()=> this.setState({moreVisibility:!this.state.moreVisibility})}
        style={{color:colors.COLOR_PRIMARY,textDecorationLine:'underline'}}>more...</Text>
        </Text>
        )
      }
    }



   
    render() {

      const {data,moreVisibility} = this.state
      console.log(":::",data.main_attractions)

        return (
      <SafeAreaView style={{flex:1,backgroundColor:this.state.loading_status ? 'black' :'rgba(0,0,0,0.6)'}}>
       
        <KeyboardAwareScrollView
        bounces={false} 
        showsVerticalScrollIndicator={false}
         contentContainerStyle={{
          justifyContent:'center',
         paddingBottom:0
        }}>



           <View style={{height:dimensions.SCREEN_HEIGHT * 0.4,width:dimensions.SCREEN_WIDTH}} >
         
                  {
                    this.state.images && this.state.images.length >0?
                    <FastImage
                    style={{ width: '100%', height: '100%' }}
                    source={{
                        uri: this.state.images[0],
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                    />
                    :null

                  }
                     {/** header */}
         
                <View style={styles.header}>
                <TouchableOpacity style={{alignItems:'center',padding:10}}
                onPress={()=> {this.props.navigation.goBack()} }>
                 
                  <Text style={{color:colors.WHITE,fontSize:24,fontWeight:'900',
                textTransform:'uppercase'}}>{data.name} </Text> 
                 
                </TouchableOpacity>

                <View/>
                </View>
            

          {/** header end */}
         </View>
         <LinearGradient
         style={{padding:10,marginTop:-50}}
         colors={['rgba(0,0,0,0.4)', 'rgba(0,0,0,0.7)', 'rgba(0,0,0,0.9)']} >
         <Text style={{fontSize:20,fontWeight:'500',
         marginBottom:15,fontFamily:fonts.heading_font,
         textTransform:'uppercase',color:colors.WHITE,alignSelf:'center'}}>About</Text>
         {
           data.facts && data.facts.about.length > 200 ?
            this._getAbout()
           :
            <Text 
                style={{color:colors.WHITE}}>{data.facts ? data.facts.about : ''}
            </Text>


         }

         </LinearGradient>

         <View style={{backgroundColor:'black'}}>
           
         <Text style={styles.heading}>Facts</Text>

          <View style={{flexDirection:'row',justifyContent:'flex-start',alignItems:'flex-start'}}>
            <View style={styles.factContainer}>
              <FastImage 
              source={require('../assets/currency_yellow.png')}
              style={{height:30,width:30,marginVertical:10}} 
              resizeMode={FastImage.resizeMode.cover}/>
              <Text style={styles.factData}>{(data.facts && data.facts.currency)
              ? data.facts.currency : ''}</Text>
            </View>

            <View style={styles.factContainer}>
              <FastImage 
              source={require('../assets/weather_yellow.png')}
              style={{height:30,width:30,marginVertical:10}} 
              resizeMode={FastImage.resizeMode.cover}/>
              <Text style={styles.factData}>{this.state.temperature} C</Text>
            </View>


            <View style={styles.factContainer}>
              <FastImage 
              source={require('../assets/plug_yellow.png')}
              style={{height:30,width:30,marginVertical:10}} 
              resizeMode={FastImage.resizeMode.cover}/>
              <Text style={styles.factData}>{(data.facts && data.facts.electic_plug) ? data.facts.electic_plug : ''}</Text>
            </View>

            <View style={styles.factContainer}>
              <FastImage 
              source={require('../assets/language.png')}
              style={{height:30,width:30,marginVertical:10}} 
              resizeMode={FastImage.resizeMode.cover}/>
          <Text style={styles.factData}>{(data.facts && data.facts.languages) ? data.facts.languages : ''}</Text>
            </View>



            


          </View>




               
        
       
         <Text style={styles.heading}>Attractions</Text>

          <FlatList
           
           style={{marginVertical:10}}
           horizontal={false}
           data={data.main_attractions}
           showsVerticalScrollIndicator={false}
           renderItem={(item,index) => this._renderAttractions(item,index)}
           keyExtractor={item => item.id}
          /> 

           {/** brands */}
      <View style={styles.hotelContainer}>
           <View style={styles.rowContainer}>
             <View/>
               <Text style={styles.heading}>  BRANDS</Text>
               <TouchableOpacity style={{marginHorizontal:15}}
                    onPress={()=>{this.props.navigation.navigate("Brands")} }>
                    <FastImage 
                    source={require('../assets/arrow_forward_yellow.png')}
                    style={{height:25,width:25}} 
                    resizeMode={FastImage.resizeMode.cover}/>
                       
                     </TouchableOpacity>
           </View>
           <FlatList
               style={{marginVertical:10}}
               horizontal={true}
               data={this.state.brands}
               showsHorizontalScrollIndicator={false}
               renderItem={(item,index) => this._renderBrand(item,index)}
               keyExtractor={item => item.id}
           />
          </View>
         {/** brands end */}
         


          </View>
        </KeyboardAwareScrollView>

        

       
         </SafeAreaView>


        )
    }
}

const mapStateToProps = state => {
	return {

		user: state.user,
	
	};
};



export default connect(mapStateToProps, null)(CountryDetails);

let styles = StyleSheet.create({
  header:{
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'rgba(255,255,255,0.1)',
        position:'absolute',
        top:0,
        left:0,right:0,justifyContent:'center'
  },
 
  hotelContainer:{
    paddingVertical:10,
    paddingHorizontal:2,
    borderTopRightRadius:16,
    width:dimensions.SCREEN_WIDTH,
    backgroundColor:colors.BLACK,
    marginTop:15,
   
},
  rowContainer:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  ratingContainer:{
    padding:6,borderRadius:20,
    justifyContent:'center',alignItems:'center',
    backgroundColor:colors.COLOR_PRIMARY
  },
  descriptionContainer:{
    padding:6,borderRadius:10,
    backgroundColor:colors.BLACK,
    borderWidth:0.4,
    borderColor:colors.COLOR_PRIMARY,
    width:'95%',alignSelf:'center',
    marginVertical:10
  }, 
  listContainer:{
    width:'98%',
    borderRadius:10,
    borderWidth:1,
    borderColor:colors.COLOR_PRIMARY,
    height:70,
    marginVertical:5,
    alignSelf:'center',
    overflow:'hidden',justifyContent:'center'
  },
  countryImage:{
      position:'absolute',
      top:0,
      left:0,
      right:0,
      bottom:0,
  },
  countryText:{
      color:'white',
      fontWeight:'bold',
      fontSize:17,
      marginLeft:20
  },
  rowContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center'
},
container: {
  backgroundColor:colors.LIGHT_BLACK,
  margin:10,
  height:dimensions.SCREEN_HEIGHT * 0.24,
  width:dimensions.SCREEN_WIDTH * 0.6,
  overflow:'hidden',
  borderRadius:10,
  borderWidth:0.6,
  borderColor:colors.BLACK,

 },
 imageContainer:{
   margin:0,
   position:'absolute',
   top:0,
   bottom:0,left:0,right:0,
  
 },
 factContainer:{
  
   flex:1,
   margin:10,
   padding:7,
   justifyContent:'center',
   alignItems:'center'
 },
 factData:{color:colors.COLOR_PRIMARY,marginLeft:10,fontSize:10,textAlign:'center'},
 attractionsContainer:{
  width:'100%',
  height:190,
  borderRadius:10,
  backgroundColor:'grey',overflow:'hidden'
},
attractionsImage:{
  flex:4,
  height:'100%',
  marginHorizontal:10
},
readContainer:{
  backgroundColor:colors.COLOR_PRIMARY,
  paddingHorizontal:10,
  paddingVertical:6,
  alignItems:'center',
  justifyContent:'center',
  width:100,
  marginTop:10
},
heading:{fontFamily:fonts.heading_font,
  textTransform:'uppercase',
  color:colors.HEADING_COLOR,fontSize:18,fontWeight:'500',marginVertical:10,
  alignSelf:'center'}


}
)
