import React, {Component} from 'react';
import {Text, View, Image,Dimensions,
  Alert,ToastAndroid,StatusBar,BackHandler,
  ScrollView,StyleSheet,Platform,
  ImageBackground,TouchableOpacity,
  ActivityIndicator,TextInput, SafeAreaView,Keyboard,Animated} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
import Video from 'react-native-video';
import { FlatList } from 'react-native-gesture-handler';
import { Button } from 'native-base';
import FastImage from 'react-native-fast-image'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


import { colors ,urls,fonts,dimensions} from '../app_constants';
import {showMessage,showToastMessage} from '../utils/showMessage';
import ProgressBar from '../components/ProgressBar';
import ButtonTextInputComponent from '../components/ButtonTextInputComponent';


import { connect } from 'react-redux';
import { addUser } from '../actions/actions';
import ButtonComponent from '../components/ButtonComponent';

class Passcode extends React.Component {

	constructor(props) {
    super(props);
    //https://medium.com/@sgroff04/get-started-with-react-native-animations-23ffa1850f9
    //https://medium.com/react-native-training/react-native-animations-using-the-animated-api-ebe8e0669fae
    this.moveAnimation = new Animated.ValueXY({x:150,y:450})
    this.state={
      loading_status:false,
      deviceToken:'324323243cfffgfdgfd',
      tempMargin:0,
      animationDone : false

    }
  }

  _moveView =()=>{
    //spring,timing,decay
    Animated.timing(
      this.moveAnimation,{
        toValue:{x:80,y:10},
        //friction: 5
      }
    ).start(()=> this.setState({animationDone:true}))

   
  }

  componentDidMount(){
    this._moveView()
  }

       
componentWillMount () {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow =() => {
    this.setState({tempMargin:dimensions.SCREEN_HEIGHT * 0.23})
  }

  _keyboardDidHide =() =>{
    this.setState({tempMargin:0})
  }

  loginHandler =() =>{
   this.props.navigation.navigate("Home")
  }


  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  _isValid(){
      console.log("0000000")
   const passcode = this.passcodeInput.getInputValue();
   if(passcode.trim().length == 0){
      showMessage('Enter PassCode')
      return false
    }
    else{
      return true;
    }
  }

  _onPasscode = () =>{
   
    if(this._isValid()){
     
        this.setState({loading_status:true})
        var formData = new FormData();
        const passcode = this.passcodeInput.getInputValue();
      
       formData.append('passcode', passcode);
       formData.append('device_id', this.state.deviceToken);
       Platform.OS =='android'
       ?  formData.append('type',1)
       : formData.append('type',2)

       Keyboard.dismiss()
    
     let url = urls.base_url +'auth/passcode'
             
     fetch(url, {
               method: 'POST',
               body: formData
              }).then((response) => response.json())
                   .then(async (responseJson) => {
                       this.setState({loading_status:false})
    
                    console.log("FFF",JSON.stringify(responseJson))
                    
                    if (responseJson.status){

          
    
                      var  user_id = responseJson.data.customer_id.toString()
                      var name = responseJson.data.name
                      var email = responseJson.data.email
                      var image = responseJson.data.customer_image
                      var token = responseJson.data.token
                      var phone = responseJson.data.phone
                      var user_type = responseJson.data.user_type

                      //user type 
                      //2 - customer
                      //6 - supplier
    
                      
                      AsyncStorage.multiSet([
                        ["user_id", user_id.toString()],
                        ["email", email.toString()],
                        ["name", name.toString()],
                        ["image", image.toString()],
                        ["token", token.toString()],
                        ["phone", phone.toString()],
                        ["user_type", user_type],
                        ["passcode", passcode],
                        ]);
  
                       await this.props.add({ 
                           user_id: user_id, 
                          name : name,
                          image : image,
                          email:  email ,
                          phone : phone,
                          token:  token ,
                          user_type:  user_type ,
                          passcode:passcode
                        })

                        //showMessage('Correct Passcode')

                        if(user_type == '6'){
                          //supplier
                          this.props.navigation.navigate('HomeSupplier');

                          return
                        }
                        else {
                          this.props.navigation.navigate('Register');

                        }
                        
                       // this.setState({passcode:''})
                      
                       }else{
                        showMessage(responseJson.message)
                        
                        
                      }
                   }).catch((error) => {
                             this.setState({loading_status:false})
                          
                             showMessage(error.message)
                   });
     }
   
  }
  
  
    _onLogin = () =>{
      if(this._isValid()){
      Keyboard.dismiss()
      const email = this.emailInput.getInputValue();
      const password = this.passwordInput.getInputValue();
  
    
        this.setState({loading_status:true})
        var formData = new FormData();
      
       formData.append('email',email);
       formData.append('password', password);
       formData.append('device_token', 'jaBBD87dg7D');
       Platform.OS =='android'
       ?  formData.append('device_type',1)
       : formData.append('device_type',2)
    
    
         console.log("FFF",JSON.stringify(formData))
    
    
               let url = urls.BASE_URL +'api_login'
              console.log("FFF",JSON.stringify(url))
               fetch(url, {
               method: 'POST',
               headers: {
                 'Accept': 'application/json',
                 'Content-Type': 'multipart/form-data',
               },
               body: formData
              }).then((response) => response.json())
                   .then(async (responseJson) => {
                       this.setState({loading_status:false})
    
                       console.log("FFF",JSON.stringify(responseJson))

    
                    if (!responseJson.error){
    
                      var id = responseJson.result.id
                      var name = responseJson.result.name
                      var email = responseJson.result.email
                      var phone = responseJson.result.mobile
                      var image = responseJson.result.user_image
                      var status = responseJson.result.otp_verified   
                     
     


                      showMessage(responseJson.message,false)

                      if(status == 1){
                         //verified user
                         AsyncStorage.multiSet([
                          ["id", id.toString()],
                          ["email", email.toString()],
                          ["name", name.toString()],
                          ["phone", phone.toString()],
                          ["image", image.toString()],
                         
                          ]);
  
                          console.log("Saving----",id)
    
                         await  this.props.add({ 
                            id: id, 
                            phone : phone,
                            image : image,
                            email:  email ,
                            name:name,
                            
                          })
                          this.props.navigation.navigate("Home")
                      }
                      else if(status == 0){
                        let tempObject = {
                          'image':image,
                          'id' : id,
                          'email':email,
                          'phone':phone,
                          'name':name,
                        
                          'path':'login'//path will decide route from otp screen
                         }
                        this.props.navigation.navigate("Otp",{result:tempObject})

                      }
  
                 
                              
                       }else{
  
                           showMessage(responseJson.message)
                        
                         }
                   }).catch((error) => {
                     console.log(error.message)
                             this.setState({loading_status:false})
                             showMessage('Try Again')
    
                   });
      }
    
     }

     _joinNow = () =>{
       let obj ={url:'https://www.utopiicofficial.com'}
       this.props.navigation.navigate('ViewWeb',{result:obj})
     }

	render() {
    return (
      <>
      <StatusBar barStyle="light-content"  backgroundColor={colors.STATUS_BAR_COLOR} />
        <SafeAreaView style={styles.container}>
                {/* position absolute make the image to go in safe are so wrapping in view solve this issue */}
                {/* image background */}
                <View>
                  <Video source={require('../assets/video.mp4')} 
                    repeat={true}
                    muted={true}
                    resizeMode='cover'
                    style={styles.imageContainer}/>
                 </View>
                {/* image background  end*/}

                <View style={{flex:1,justifyContent:'space-between'}}>
                <FastImage 
                 source={require('../assets/logo.png')}
                 style={styles.headerLogo} 
                 resizeMode={FastImage.resizeMode.contain}/>


                <Animated.View 
                style={[{position:'absolute',top:Platform.OS == 'ios' ? 30: 10,alignSelf:'center'},this.moveAnimation.getLayout()]}>
                {/* <Text style={[styles.headingText,{textAlign:'center'}]}>Passcode</Text> */}
                  <FastImage 
                    source={require('../assets/logo-info.png')}
                    style={styles.centerLogo} 
                    resizeMode={FastImage.resizeMode.contain}/>
               </Animated.View>

               



                      
                      <ButtonTextInputComponent
                        placeholder={'Passcode'}
                        onSubmitEditing={()=> this._onPasscode()}
                        onFinish={this._onPasscode}
                        inputRef={ref => this.passcode = ref}
                        ref={ref => this.passcodeInput = ref}
                        secureTextEntry={true}
                        maxLength={10}
                        autoCapitalize="characters"
                        style={{width:'90%',marginTop:-30,alignSelf:'center'}}
                        returnKeyType="go"
                    />

              
                <ButtonComponent 
                  style={{width:dimensions.SCREEN_WIDTH * 0.4,alignSelf:'center',margin:30}}
                    handler={this._joinNow}
                  label ={'JOIN NOW'}/>
                 

                  </View>

                  { this.state.loading_status && <ProgressBar/> }
             
        </SafeAreaView>
      </>
    );
	}
}


const mapDispatchToProps = dispatch => {
  return {
      add: (userinfo) => dispatch(addUser(userinfo)),
  }
}
export default connect(null, mapDispatchToProps)(Passcode);

let styles = StyleSheet.create({
  container:{
  //  height:'100%',
    flex:1,
    backgroundColor:colors.COLOR_PRIMARY,
    justifyContent:'space-between'
  },
  imageContainer:{
      //  position:'absolute',
      //  top:0,left:0,right:0,bottom:0


      //height and width will not go in safe area means notch me nhi 
      //  height:'100%',
      // width:'100%'

      position: 'absolute',
      flex: 1,
      backgroundColor:'rgba(0,0,0,0.45)',
      width:dimensions.SCREEN_WIDTH,
      height: dimensions.SCREEN_HEIGHT
  },

  scrollContainer:{
    padding:10,
    alignItems:'center'
  },
  headerLogo:{
      height: 60, 
      width:60, 
      marginLeft:10,
      marginTop:10

  },
  headingText:{
    color:colors.WHITE,
    fontSize:22,
    marginVertical:25,
    fontWeight:'bold'
  },
  centerLogo:{
    height: 180, 
    width:150, 
    marginTop:-50
  }
})

