import React from 'react';
import { StyleSheet, View, TextInput, Text,Image, Platform,TouchableOpacity,Animated} from 'react-native';

import {colors,urls,dimensions} from '../app_constants';
import FastImage from 'react-native-fast-image'

export default class BannerComponent extends React.Component {
    constructor(props) {
        super(props);
        this.delayValue = 500;
        this.state = {
          animatedValue: new Animated.Value(0),
          data:[]
        }
      }

      componentDidMount = () => {
        Animated.spring(this.state.animatedValue, {
          toValue: 1,
          tension: 20,
          useNativeDriver: true
        }).start();
      }
	

  render() {
      const {object} = this.props
      this.delayValue = this.delayValue + 500;
      const translateX = this.state.animatedValue.interpolate({
        inputRange: [0, 1],
        outputRange: [this.delayValue, 1]
      });
    return (
        <Animated.View  style={{ transform: [{ translateX }]}}>
        <TouchableOpacity onPress={()=> this.props.clickHandler(this.props.object)}
         style={styles.container}>
        <View 
         style={styles.imageContainer} >
            <FastImage 
            source={{uri:object.image}}
            style={styles.imageStyle} 
            resizeMode={FastImage.resizeMode.cover}/>
        </View>

        <View style={{padding:3,backgroundColor:colors.LIGHT_BLACK,
          justifyContent:'center',flex:2.5}}>
        <Text style={{color:'white'}}>{object.title}</Text>
        <Text style={{textAlign:'left',color:'white',fontSize:10}}>{object.description.substring(0,30)}</Text>
        </View>

        {/* <FastImage 
        source={require('../assets/category1_icon.png')}
        style={styles.icon} 
        resizeMode={FastImage.resizeMode.cover}/> */}
        </TouchableOpacity>
        </Animated.View>
     
    );

  }
}



const styles = StyleSheet.create({
  container: {
   backgroundColor:colors.BLACK,
   margin:10,
   height:dimensions.SCREEN_HEIGHT * 0.22,
   width:dimensions.SCREEN_WIDTH * 0.37,
   overflow:'hidden',
  

  },
  imageContainer:{
    margin:0,
    flex:8,
  },

 
  imageStyle:{
  
   height:'100%',width:'100%',
   borderRadius:10,
   borderWidth:0,
   borderColor:colors.GREY,overflow:'hidden'
  },
  
  icon:{
      position: 'absolute',right: 10,
      top:0,height:25,width:25
  }
});